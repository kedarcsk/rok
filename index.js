/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import { Navigation } from 'react-native-navigation'
import App from './App'
import Screens from './src/Utils/Screens'
// import Provider from './src/Utils/StoreProvider'
import { InitializeApp } from './src/Utils/Navigation'
import { Provider } from 'mobx-react/native';
import store from './src/Stores/stores'
const Screen_Array = Array.from(Screens.entries())
Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App, Provider, store );

Navigation.setDefaultOptions({
  animations: {
    push: {
      enabled: 'true',
      content: {
        x: {
          from: 2000,
          to: 0,
          duration: 200
        }
      }
    },
    pop: {
      enabled: 'true',
      content: {
        x: {
          from: 0,
          to: 2000,
          duration: 200
        }
      }
    }
  }
})
registerComponents = () => {
  Screen_Array.forEach( ([screenName, ScreenModule]) => {
    Navigation.registerComponentWithRedux(screenName, ScreenModule,  Provider , {...store})
  });
}
registerComponents();
InitializeApp();




