import * as Layouts from './NavigationLayouts'
import { Navigation } from 'react-native-navigation'
import * as utils from '../Utils/utilFunctions'

export const InitializeApp = () =>{
  Navigation.events().registerAppLaunchedListener(async() => {
    let empData = await utils.getFromaAsyncStorage('@empData')
    empData = empData ? JSON.parse(empData) : empData
    if(empData && empData.pwdFlag == 0) {
      Navigation.setRoot(Layouts.LoggedInLayout(empData))
    }
    else {
      Navigation.setRoot(Layouts.InitialStack);
    }
  });
}

export const push = (id:String, screenName:String, props?:Object) => {
  Navigation.push(id,{
    component:{
      name: screenName,
      passProps: props? props: 'Passed Props',
      options: {
        topBar: {
          visible: false,
          drawBehind: true,
          animate:false,
        }
      }
    }
  })
}

export const pushLayout = (id:String, Layout:Object) => {
  Navigation.push(id, Layout)
}

export const pop = (id:String) => {
  Navigation.pop(id)
}

export const LogOut = async() =>{
  await utils.clearStorage()
  Navigation.setRoot(Layouts.InitialStack);  
}

export const onHomePress = async() => {
  Navigation.popTo('HOMESCREEN')
}

export const onPressHeaderRightImage = (id:String) => {
  Navigation.mergeOptions(id, {
    sideMenu: {
      right: {
        visible: true
      }
    }
  });
}