import * as ScreenIDs from './Screen_IDs';
import Login from '../Screens/login';
import ChangePassword from '../Screens/ChangePassword'
import ForgotPassword from '../Screens/ForgotPassword'
import HomeScreen from '../Screens/HomeScreen'
import SideMenu from '../Screens/SideMenu';
import MyTime from '../Screens/MyTime'
import AddTime from '../Screens/AddTime'
import MyHoliday from '../Screens/MyHoliday'
import MyPTO from '../Screens/MyPTO'
import FindAColleagueDetails from '../Screens/FindAColleague/FindAColleugueDetails';
import FindColleague from '../Screens/FindAColleague/FindColleague';
import ClaimExpense from '../Screens/Expense/ClaimExpense';
import AddExpense from '../Screens/Expense/AddExpense';
import ExpenseDetails from '../Screens/Expense/ExpenseDetails';
import EditExpense from '../Screens/Expense/EditExpense';
import MyExpenseReport from '../Screens/Expense/MyExpenseReport';
import RaiseTicket from '../Screens/HelpDesk/RaiseTicket';
import MyTickets from '../Screens/HelpDesk/MyTickets';
import YourTickets from '../Screens/HelpDesk/YourTickets';
import DisplayModal from '../Screens/HelpDesk/DisplayModal';
import TicketViewDetails from '../Screens/HelpDesk/TicketViewDetails'

const Screens : Map<string,any>= new Map();
Screens.set(ScreenIDs.LOGIN, () => Login)
Screens.set(ScreenIDs.CHANGE_PASSWORD, () => ChangePassword)
Screens.set(ScreenIDs.FORGOT_PASSWORD, () => ForgotPassword)
Screens.set(ScreenIDs.HOME_SCREEN, () => HomeScreen)
Screens.set(ScreenIDs.SIDE_MENU, () => SideMenu)
Screens.set(ScreenIDs.MY_TIME, () => MyTime)
Screens.set(ScreenIDs.ADD_TIME, () => AddTime) 
Screens.set(ScreenIDs.FIND_COLLEAGUE_DETAILS, () => FindAColleagueDetails) 
Screens.set(ScreenIDs.FIND_COLLEAGUE, () => FindColleague) 
Screens.set(ScreenIDs.CLAIM_EXPENSE, () => ClaimExpense) 
Screens.set(ScreenIDs.ADD_EXPENSE, () => AddExpense) 
Screens.set(ScreenIDs.EXPENSE_DETAILS, () => ExpenseDetails) 
Screens.set(ScreenIDs.EDIT_EXPENSE, () => EditExpense) 
Screens.set(ScreenIDs.MY_EXPENSE_REPORT, () => MyExpenseReport) 
Screens.set(ScreenIDs.NO_EXPENSE, () => NoExpense) 
Screens.set(ScreenIDs.CREATE_EXPENSE, () => CreateExpense) 
Screens.set(ScreenIDs.MY_HOLIDAY, () => MyHoliday)
Screens.set(ScreenIDs.MY_PTO, () => MyPTO)
Screens.set(ScreenIDs.RAISE_TICKET, () => RaiseTicket)
Screens.set(ScreenIDs.YOUR_TICKETS, () => YourTickets)
Screens.set(ScreenIDs.MY_TICKETS, () => MyTickets)
Screens.set(ScreenIDs.DISPLAY_MODAL, () => DisplayModal)
Screens.set(ScreenIDs.TICKET_VIEW_DETAILS, () => TicketViewDetails)
export default Screens;