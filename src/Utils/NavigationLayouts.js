import * as ScreenIDs from './Screen_IDs' 
export const InitialStack ={
  root:{
    stack : {
      children: [
        {
          component:{
            id:'LOGIN',
            name: ScreenIDs.LOGIN,
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              }
            }
          }
        }    
      ],
      options: {}
    },
  }
}

export const HomeLayout = {
  sideMenu: {
    center: {
      stack : {
        id:'LoggedIn_Stack',
        children: [
          {
            component:{
              id:'HOMESCREEN',
              name: ScreenIDs.HOME_SCREEN,
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                }
              }
            }
          }    
        ],
        options: {}
      },
    },
    right: {
      component: {
        name: ScreenIDs.SIDE_MENU,
        passProps: {
          text: 'This is a right side menu screen'
        },
        options:{
          visible:false,
        }
      }
    }
  }
}

export const LoggedInLayout = (passprops) => {
  return {
    root:{
      //Side Menu
      sideMenu: {
        center: {
          stack : {
            id:'LoggedIn_Stack',
            children: [
              {
                component:{
                  id:'HOMESCREEN',
                  name: ScreenIDs.HOME_SCREEN,
                  passProps: {
                    empData:passprops
                  },
                  options: {
                    topBar: {
                      visible: false,
                      drawBehind: true,
                      animate: false
                    }
                  }
                }
              },      
            ],
            options: {}
          },
        },
        right: {
          component: {
            name: ScreenIDs.SIDE_MENU,
            passProps: {
              text: 'This is a right side menu screen'
            },
            options:{
              visible:false,
            }
          }
        }
      }
    }
  }
}
 