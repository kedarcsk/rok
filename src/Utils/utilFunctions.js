import moment from 'moment';
import {  Alert, AsyncStorage } from 'react-native';
import constant from '../Assests/constants/constant'
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { Platform } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob'
import {NativeModules} from 'react-native';
import Toast from 'my-file-opener'

// iPhone/Android

getLocalPath = (url) => {
  const filename = url.split('/').pop();
  // feel free to change main path according to your requirements
  return `${RNFS.DocumentDirectoryPath}/${filename}`;
}

export const showFile = (url:String) => {
  const localFile = getLocalPath(url);
   
  const options = {
    fromUrl: url,
    toFile: localFile
  };
  RNFS.downloadFile(options).promise
    .then(() => FileViewer.open(localFile))
    .then(() => {
      // success
      console.log("file opened")
    })
    .catch(error => {
      // error
      console.log(error)
    });
}

export const chooseFile = (afterSelection:Function) => {
  Alert.alert(
    'Konnect',
    'Choose File',
    [
      { text: 'Open Files', onPress: () => openDocumentPicker(afterSelection) },
      { text: 'Take Pic', onPress: () =>  takePic(afterSelection)}
    ],
    { cancelable: false }
  )
}

export const takePic = (afterSelection:Function) => {
  var options = {
    title: 'Select Photo',
    quality: 0.5,
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  };
  ImagePicker.launchCamera(options, response => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } 
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } 
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } 
    else {
      afterSelection(response)
    }
  });
}

export const openDocumentPicker = (afterSelection:Function) => {
  DocumentPicker.show({
    filetype: [DocumentPickerUtil.allFiles()],
  },(error,res) => {
    // Android
    if(res) {
      afterSelection(res)
      console.log(
        res.uri+'#'+
        res.type+'#'+ // mime type
        res.fileName+'#'+
        res.fileSize
      ); 
    }
    else if(error) {
      console.log(error)
    }
  });
}

export const renderIf = (condition, content) => {
  if (condition) {
    return content;
  } 
  else {
    return null;
  }
}

export const getToken = (serverID:String) => {
  let date = moment().format('DDMM')
  console.log(date)
  return `${serverID}_${date}#${constant.DEVICE_ID}`
  // if(isLogin){
  //   return `${serverID}_${date}#${constant.DEVICE_ID}`
  // }
  // else{
  //   return `$`
  // }
}

export const showAlertWithOkButton = (message:String, okCallBack:Function) => {
  Alert.alert(
    'Konnect',
    message,
    [
      { text: 'OK', onPress: okCallBack },
      { text: 'Cancel', onPress: () => { } }
    ],
    { cancelable: false }
  )
}

export const showAlert = async(message:String, okCallBack?:Function, cancelable?:Boolean) => {
  const onPress = okCallBack ? okCallBack : () => { }
  let Message =  [
    { text: 'OK', onPress: onPress }
  ]
  if(cancelable !== undefined) {
    Message.push({ text: 'Cancel', onPress: ()=>{}})
  }
  Alert.alert(
    'Konnect',
    message,
    Message,
    { cancelable: cancelable !== undefined ? cancelable : false }
  )
}

export const setInAsyncStorage = async(key, value) => {
  try{
    await AsyncStorage.setItem(key, value)
  }
  catch(error) {
    //TODO error handling if any error occurs
  }
} 

export const getFromaAsyncStorage = async(key) => {
  try{
    return await AsyncStorage.getItem(key)
  }
  catch(error) {
    //TODO error handling if any error occurs
  }
}

export const clearStorage = async(fn?:Function) => {
  try{
    await AsyncStorage.clear()
  }
  catch(error) {
    //TODO error handling if any error occurs
  }
}

export const getMonday = (d) => {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

export const checkForMonday = (dateString) => {
  let date = new Date(dateString)
  //console.log(date.getDay())
  if(date.getDay() === 1) {
    return true
  }
  return false
}

export const correctDateFormat = (date:String) => {
  // let tempdtString = date.split('-')
  // return `${tempdtString[1]}-${tempdtString[0]}-${tempdtString[2]}`
  let tempDate = moment(date).format('DD-MM-YYYY')
  let tempdtString = date.split('-')
  if(tempDate === "Invalid date"){
    return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).format('DD-MM-YYYY')
  }
  return date

}

export const formatDate = (date:String) => {
  try{
    let temp = moment(date).format('DD-MM-YYYY')
    if(temp === "Invalid date"){
      let tempdtString = date.split('-')
      return `${tempdtString[1]}-${tempdtString[0]}-${tempdtString[2]}`
    }
    return temp
  }
  catch(error) {
    return date
  }
 
}

export const formatWeekDate = (date:String) => {
  let tempdtString = date.split('-')
  return `${tempdtString[0]}/${tempdtString[1]}`
}

export const getAlphaDay = (date:String) => {
  let tempdtString = date.split('-')
  let dt = new Date(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`)
  return constant.DAYS_ALPHA.get(dt.getDay())
}

export const formatAPI_DATE = (date:String) => {
  let tempdtString = date.split('-')
  return `${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`
}

export const formatEntryDate = (date:String, day:Number) => {
  let tempDate = moment(date).format('DD-MM-YYYY')
  let tempdtString = date.split('-')
  if(tempDate === "Invalid date"){
    return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).add(day, 'days').format('DD-MM-YYYY')
  }
  return moment(`${tempdtString[1]}-${tempdtString[0]}-${tempdtString[2]}`).add(day, 'days').format('DD-MM-YYYY')
}

export const showToast = (message:String) => {
  console.log(Toast)
  console.log(NativeModules)
  Toast.show(message, Toast.SHORT)
}

export const alpha_numeric_date = (date:String) => {
  let tempDate = moment(date).format('DD-MM-YYYY')
  let tempdtString = date.split('-')
  if(tempDate === "Invalid date"){
    return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).format('LL')
  }
  return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).format('LL')
}
export const weeklySearchDate = (date:String) => {
  let tempDate = moment(date).format('DD-MM-YYYY')
  let tempdtString = date.split('-')
  if(tempDate === "Invalid date"){
    return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).format('MM/DD/YYYY')
  }
  return moment(`${tempdtString[2]}-${tempdtString[1]}-${tempdtString[0]}`).format('MM/DD/YYYY')
}

export const holidayDate = (date:String) => {
  return moment(date).format('DD MMM')
}

