import { Linking, Platform } from 'react-native';
import constant from '../Assests/constants/constant';
import Communications from 'react-native-communications';

export const OpenSocial = async (socialPlatform, uri) => {
  try {
    let url;
    switch (socialPlatform) {
      case constant.SOCIAL_PLATFORM.FACEBOOK:
        if (Platform.OS === 'ios') {
          url = uri;
        } 
        else {
          url = `fb://facewebmodal/f?href=${uri}`;
        }
        break;
      case constant.SOCIAL_PLATFORM.SKYPE:
        url = `skype:/${uri}/?chat`;
        break;
      case constant.SOCIAL_PLATFORM.LINKED_IN:
        url = uri;
        break;
      case constant.SOCIAL_PLATFORM.TWITTER:
        url = uri;
    }
    console.log(url);
    await Linking.openURL(url);
  } 
  catch (error) {
    console.log(error.code);
    const appName = 'skype-for-iphone';
    const playStoreId = 'com.skype.raider';
    const appStoreId = '304878510';
    if (error.code === 'EUNSPECIFIED') {
      if (socialPlatform === constant.SOCIAL_PLATFORM.SKYPE) {
        if (Platform.OS === 'ios') {
          const locale =
            typeof appStoreLocale === 'undefined' ? 'us' : appStoreLocale;

          await Linking.openURL(
            'https://itunes.apple.com/in/app/skype-for-iphone/id304878510?mt=8'
          );
        } 
        else {
          await Linking.openURL(
            `https://play.google.com/store/apps/details?id=${playStoreId}`
          );
        }
      } 
      else {
        await Linking.openURL(uri);
      }
    }
  }
};

export const makeCall = async phoneNo => {
  try {
    Communications.phonecall(phoneNo, true);
  }
  catch (error) {
    console.log(error);
  }
};

export const sendText = async (phoneNo, textBody) => {
  try {
    Communications.text(phoneNo, textBody);
  }
  catch (error) {
    console.log(error);
  }
};

export const sendMail = async (email, textBody) => {
  try {
    Communications.email([email],null,null,'','')
  }
  catch (error) {
    console.log(error);
  }
};
