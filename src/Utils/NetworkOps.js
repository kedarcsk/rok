errorHandler = (ErrorCode) => {
  console.log(ErrorCode)
  //TODO handling error according to errorcodes
}

hitAPI = async(url:String, header:Object, afterResponseFn?:Function) => {
  try{
    let response = await fetch(url, header)
    response = await response.json()
    console.log('Network Response>>>>>'+JSON.stringify(response))
    if(response.status){
      afterResponseFn && afterResponseFn()  
      return response
    }
    else{
      console.log(response.message)
      return
      //TODO check for error and handle it
    }
  }
  catch(error){
    //alert(error)
    console.log('url'+url)
    errorHandler(error)
  }
  
}

export const fetchPost = async(url:String, Params:Object, afterResponseFn?:Function) => {
  let header = {
    method:'POST',
    body:JSON.stringify(Params),
    headers:{
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
    }
  }
  return await hitAPI(url, header, afterResponseFn)
}

export const uploadForm = async(url:String, Params:Object) => {
  let header = {
    method: 'post',
    body:Params,
    headers: {
      'Content-Type': 'multipart/form-data',
    }
  }
  return await hitAPI(url, header)
}