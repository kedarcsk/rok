import React, { Component } from 'react';
import {  View , StyleSheet , Image, Text, TouchableOpacity, ScrollView, SafeAreaView} from 'react-native';
import AlertPopup from '../Components/AlertPopup'
import CustomTextInput from '../Components/CustomTextInput';
import CustomButton from '../Components/CustomButton';
import constant from '../Assests/constants/constant'
import { inject, observer } from 'mobx-react/native'
import * as Navigation from '../Utils/Navigation'
import * as ScreenIDs from '../Utils/Screen_IDs'
import { showAlert , showAlertWithOkButton, renderIf } from '../Utils/utilFunctions'
import { HomeLayout } from '../Utils/NavigationLayouts'
import { observable } from 'mobx';
import CustomActivityIndicator from '../Components/CustomActivityIndicator'

@inject('store')
@observer
export default class Login extends Component {

  @observable empid : String = ''
  @observable passwd : String = ''
  @observable modalVisible : Boolean = false
  @observable otp: String = ''
  @observable isLoading: Boolean = false

  componentWillMount(){
    
  }

  onLoginPress = async() => {
    const {store} = this.props
    if(this.validationCheck()){
      this.isLoading = true
      let response = await store.login.loginApi(this.empid, this.passwd )
      if(response){
        
        this.otp = response.data.otp
        this.modalVisible = true
        
        
    }
    this.isLoading = false
    // alert(this.otp)
    }
    
    //console.log(response)
  }

  onApiResponse = () => {
    
  }

  validateUserId = (text) => {
    let reg = new RegExp('^[0-9]+$')
    if(!reg.test(text) && text != '' ) {
      showAlert(constant.TEXTS.numeric)
      this.empid = text.substring(0, (text.length - 1))
    }
    else{
      this.empid = text
    }
  }

  validationCheck = () => {
    if( this.empid == '')
    {
      showAlert(constant.TEXTS.empId)
      return false
    }
    else if ( this.passwd == '' )
    {
      showAlert(constant.TEXTS.passwd)
      return false
    }
    return true
  }

  okPress = async() => {
    this.modalVisible = !this.modalVisible
    const {store} = this.props
    this.isLoading = true
    let response = await store.login.otpVaild(this.empid, this.passwd, this.otp )
    if(response.data){
      if(response.data.pwdFlag == 0)
      {
      Navigation.pushLayout(this.props.componentId, HomeLayout)
      }
      else
      Navigation.push( this.props.componentId, ScreenIDs.CHANGE_PASSWORD)
    }
    this.isLoading = false
  }

  cancelPress = () => {
    this.modalVisible = !this.modalVisible
  }

  renderTextComponents = () => {
    return(
      <View style = {styles.textComponents}>
        <CustomTextInput 
          maxLength={8}
          value={this.empid}
          image = {constant.IMAGES.userPic} 
          onChangeText={(text) => this.validateUserId(text) }
          placeholder = "Employee Id" />
        <CustomTextInput 
         image = {constant.IMAGES.passIcon}
         type = {true}
         onChangeText={(text) => { this.passwd = text }}
         placeholder = "Password" />
      </View>
    )
  }
  renderButtonComponent = () => {
    return(
      <View style={styles.buttonComponents}>         
        <CustomButton 
          text = "Login"
          onPress = {this.onLoginPress}  
        />
      </View>
    )
  }

  render() {
    return(
      <ScrollView scrollEnabled={false} contentContainerStyle = {{flex:1}}>
       {renderIf( this.isLoading, <CustomActivityIndicator />)}
       {/* <CustomActivityIndicator/>  */}
        <SafeAreaView style = {styles.container}  behavior="padding" enabled={true}>
       
        
          <View style={{alignItems:'center'}}>   
         
            <Image  
              resizeMode = {'contain'} 
              style = {styles.image} 
              source = {constant.IMAGES.kelltonLogo} /></View>
          <Text 
            style = {[styles.text, { fontSize:22, marginTop:15 }]} >
            {constant.TEXTS.infinitePoss}
          </Text>
          <Text
            style = {[styles.text, { fontSize:30, marginTop:40}]} >
            {constant.TEXTS.wlcm}
          </Text>
          {this.renderTextComponents()}
          {this.renderButtonComponent()}
          <TouchableOpacity
             onPress={ () => {Navigation.push( this.props.componentId, ScreenIDs.FORGOT_PASSWORD ) }}
          >
            <Text 
              style = {[styles.text, { fontSize:18, marginTop:18}]} >
            Forgot Password</Text>
          </TouchableOpacity>
        </SafeAreaView>
        <AlertPopup
          otp={this.otp}
          Alert_Visibility={this.modalVisible}
          okPress={this.okPress}
          cancelPress={this.cancelPress}
        />
      </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:constant.COLORS.WHITE,
    justifyContent:'center',
  },
  image:{
    width:240,
    height:140
  },
  text:{
    color:constant.COLORS.THEME_BLUE,
    textAlign:'center',
  },
  textComponents:{
    marginTop:50,
    marginHorizontal:40
  },
  buttonComponents:{
    marginHorizontal:40, 
    marginTop:20,
    alignItems:'center'
  }
  
})