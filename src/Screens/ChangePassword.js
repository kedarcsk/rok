import React, { Component } from 'react';
import { Text, View , StyleSheet ,StatusBar, SafeAreaView, ScrollView } from 'react-native';
import CustomTextInput from '../Components/CustomTextInput';
import CustomButton from '../Components/CustomButton';
import Constants from '../Assests/constants/constant'
import CustomHeader from '../Components/CustomHeader';
import { inject, observer } from 'mobx-react/native'
import { observable } from 'mobx';
import { renderIf, getFromaAsyncStorage } from '../Utils/utilFunctions';
import { Navigation } from "react-native-navigation"
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
import * as navigation from '../Utils/Navigation'

@inject('store')
@observer
export default class Login extends Component {
    @observable isLoading: Boolean = false
    state = {
        currentPwd: '',
        newPwd: '',
        rePwd: '',
        userData: {},
        token:''
    }

    componentWillMount = async() => {
        let userData = await getFromaAsyncStorage('@empData')
        let parsedUserData = JSON.parse(userData)
        let token = await getFromaAsyncStorage('@token')
        let parsedToken = JSON.parse(token)
        this.setState({userData:parsedUserData , token:parsedToken})
    }

    handleCurrentPassword = (text) => {
        this.setState({ currentPwd: text })
        console.log(this.state.currentPwd);
    }

    handleNewPassword = (text) => {
        this.setState({ newPwd: text })
        console.log(this.state.newPwd);
    }

    handleReTypePassword = (text) => {
        this.setState({ rePwd: text })
    }

    changePasswordClicked = async () => {
      const { currentPwd, newPwd, userData, token } = this.state;
      // const { token } = this.props.store.login
        if (this.validatePassword()) {
            this.isLoading = true
            let response = await this.props.store.login.callWebServiceToChangePassword(token, userData.corpid, currentPwd, newPwd);
           console.log(response)
            if(response){
              alert("Password Changed")
              navigation.LogOut()
            }
            this.isLoading = false
        }
    }

    validatePassword = () => {
      const { currentPwd, newPwd, rePwd, userData } = this.state;
      if (userData.pwdFlag != 1) {
          if (currentPwd.length) {
              if (newPwd.length) {
                  if (newPwd.length >= 8) {
                      if (this.isValidPassword(newPwd)) {
                          if (rePwd.length) {
                              if (newPwd == rePwd) {
                                  return true;
                              } else {
                                  alert('New Password and Re-type password are not matched.');
                                  return false;
                              }
                          } else {
                              alert('Please Enter Re-type Password.');
                              return false;
                          }
                      } else {
                          alert(' The password Should contain Capital Alphabet, Small Alphabet, Number, Special character.');
                          return false;
                      }
                  } else {
                      alert('The password must be atleast 8 characters long.');
                      return false;
                  }
              } else {
                  alert('Please enter New Password.');
                  return false;
              }
          } else {
              alert('Please enter Current Password.');
              return false;
          }
      } else {

          if (newPwd.length) {
              if (newPwd.length == 8) {
                  if (this.isValidPassword(newPwd)) {
                      if (rePwd.length) {
                          if (newPwd == rePwd) {
                              return true;
                          } else {
                              alert('New Password and Re-type password are not matched.');
                              return false;
                          }
                      } else {
                          alert('Please Enter Re-type Password.');
                          return false;
                      }
                  } else {
                      alert(' The password Should contain Capital Alphabet, Small Alphabet, Number, Special character.');
                      return false;
                  }
              } else {
                  alert('The password must be atleast 8 characters long.');
                  return false;
              }
          } else {
              alert('Please enter New Password.');
              return false;
          }
      }
  };

  isValidPassword = (pwd) => {
      var reg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      return reg.test(pwd);
  };

  onPressHeaderLeftImage = () => {
    Navigation.popTo('HOMESCREEN');
  }

  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }

  renderTextComponents = () => {
    return(
      <View style={Styles.TextInputContainer}>
        {renderIf(this.state.userData.pwdFlag!=1 ,
          <CustomTextInput 
          placeholder='Current Password'
          type = {true}
          onChangeText={this.handleCurrentPassword}
        />
        )}
        <CustomTextInput
          placeholder='New Password' 
          type = {true}
          onChangeText={this.handleNewPassword}
        />
        <CustomTextInput
          placeholder='Re-type Password' 
          type = {true}
          onChangeText={this.handleReTypePassword}
        />
      </View>
    )
  }

  renderButtonComponent = () => {
    return(
      <View style={Styles.ButtonContainer}>
        <CustomButton text='Change'
          onPress={this.changePasswordClicked}
        />
      </View>
    )
  }

  render() {
    return(
      <ScrollView scrollEnabled={false} contentContainerStyle = {Styles.ScrollContainer}>
        <SafeAreaView style={{flex:1}}>
            <StatusBar
              backgroundColor= {Constants.COLORS.THEME_BLUE}
            />
            <CustomHeader 
              title = "Konnect"
              headerLeftImage = {Constants.IMAGES.backIcon}
              onPressHeaderLeftImage = {this.onPressHeaderLeftImage}
            />
            {renderIf( this.isLoading, <CustomActivityIndicator />)}
            <View style={{ justifyContent:'center', flex:1}} behavior="padding" enabled={true}>

              <View style={Styles.container}>
                <View style= {{alignItems:'center',justifyContent:'center'}}>
                  <Text style={Styles.text}>Change Password</Text>
                </View>
                {this.renderTextComponents()}
                {this.renderButtonComponent()}
              </View>
            </View>
            </SafeAreaView>
        </ScrollView>
    )
  }
}

const Styles = StyleSheet.create({
  container:{ 
    borderWidth:1,
    borderColor: Constants.COLORS.THEME_BLUE,
    borderRadius:10,
    marginHorizontal:20,
    height:500,
    justifyContent:'space-evenly'
  },
  text:{
    fontSize:30,
    color: Constants.COLORS.THEME_BLUE,
  },
  ButtonContainer:{
    alignItems:'center',
    marginHorizontal:40
  },
  TextInputContainer:{
    marginHorizontal:40
  },
  ScrollContainer:{
    flex:1,
    backgroundColor:Constants.COLORS.WHITE
  }
})