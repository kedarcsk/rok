import React, {Component} from 'react'
import {View, Text, SafeAreaView, TouchableOpacity,TouchableWithoutFeedback, StyleSheet, Picker, Modal, Dimensions, Image, FlatList, Platform} from 'react-native'
import constant from '../Assests/constants/constant'
import Calendar from '../Components/Calendar'
// import { Calendar } from 'react-native-calendars'
import PickerComponent from '../Components/PickerComponent'
import CustomHeader from '../Components/CustomHeader'
import TimeCard from '../Components/TimeCard'
import Button from '../Components/CustomButton'
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
import * as Screen_IDs from '../Utils/Screen_IDs'
import { Navigation } from 'react-native-navigation'
import * as navigation from '../Utils/Navigation'
import * as utils from '../Utils/utilFunctions'
import {observer, inject} from 'mobx-react'
import {observable} from 'mobx'
import moment from 'moment';
import picker from 'react-native-picker';
 let dataA = [58,59,60,61];
// for(var i=0;i<100;i++){
//     data.push(i);
// }

const {height, width} = Dimensions.get('window')
const item = {
  hours:'8',
  timeDt:'2018-03-01',
  projNm:'kelltond',
  summary:'asdbvcfd',
  timeCode:'Beachd',
  timeCodeId:'2',
  status:'Approved',
  statusId:'2',

}
const data = [item, item, item, item]

@inject('store')
@observer
export default class MyTime extends Component{

  @observable tabSwitch:Boolean = false
  @observable selectedValue:String = ''
  @observable selectedTab:Number = 1
  @observable selectedPicker:Number = 1
  @observable selectedMonth:String = 'January'
  @observable selectedYear:String = '2019'
  @observable selectedDate:String = '01-01-0001'
  @observable modalVisible:Boolean = false
  @observable monthlyTimeData:Array = []
  @observable weeklyTimeData:Array = []
  @observable isLoading:Boolean = false
  pickeritems:Map = new Map()

  constructor(props) {
    super(props)
    console.log(picker)
    this.pickeritems.set("0", ['2017', '2018', '2019', '2020', '2021', '2022', '2023'])
    this.pickeritems.set("1", ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'])
    let currentDate = new Date()
    this.selectedMonth = constant.MONTHS_ALPHA.get(currentDate.getMonth())
    this.selectedYear = currentDate.getFullYear()
    let currentMonday = utils.getMonday(currentDate)
    this.selectedDate = moment(currentMonday).format('DD-MM-YYYY')
  }

  async componentWillMount() {
    this.isLoading = true
    await this.onMonthlySearch()
    await this.onWeeklySearch()
    this.isLoading = false
  }

  getHours = (timeData)=> {
    return timeData.reduce( (acc, obj)=>{
      if(obj.timeCodeId === 3) {
        return {
          billable:acc.billable + parseInt(obj.hours),
          non_billable:acc.non_billable,
          total:acc.total + parseInt(obj.hours)
        }
      }
      else {
        return {
          billable:acc.billable,
          non_billable:acc.non_billable + parseInt(obj.hours),
          total:acc.total + parseInt(obj.hours)
        }
      }
    }, {billable:0, non_billable:0, total:0})
  }

  onTabSwitch = (selectedTab) => {
    if(!(this.selectedTab === selectedTab)) {
      this.tabSwitch = !this.tabSwitch
    }
    this.selectedTab = selectedTab
  }

  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }

  onDayPress = (date) => {
    this.selectedDate = moment(date.dateString).format('DD-MM-YYYY');
    //this.selectedDate = utils.correctDateFormat(date.dateString)
  }

  onDonePress = async() => {
    this.modalVisible = false
    if(this.selectedTab === 0){
      this.isLoading = true
      await this.onWeeklySearch()
      this.isLoading = false
      //API hit for getting weekly Time
    }
  }

  openPicker = (selectedPicker) => {
    this.selectedPicker = selectedPicker
    const selectedValue = selectedPicker ? this.selectedMonth : this.selectedYear
    picker.init({
      pickerToolBarBg:[3, 105, 162, 1],
      pickerTitleText:'',
      pickerBg:[255, 255, 255, 1],
      pickerConfirmBtnText: '',
      pickerCancelBtnText: 'Close',
      pickerConfirmBtnColor:[255, 255, 255, 1],
      pickerCancelBtnColor:[255, 255, 255, 1],
      pickerFontSize:20,
      pickerFontFamily: 'aller',
      pickerToolBarFontSize:18,
      pickerData: this.pickeritems.get(selectedPicker.toString()),
      selectedValue: [selectedValue.toString()],
      onPickerConfirm: async(data) => {
        console.log("working")
      },
      onPickerCancel: data => {
        picker.hide()
        // console.log(data);
      },
      onPickerSelect: data => {
        if(selectedPicker) {
          this.selectedMonth = data[0] 
        }
        else {
          this.selectedYear = data[0]
        }
      }
    })
    picker.show()
    //this.modalVisible = true
  }

  onPickerValueChange = (itemValue, itemIndex) => {
    if(this.selectedPicker !== 1 ) {
      this.selectedYear = itemValue
    }
    else{
      this.selectedMonth = itemValue
    }
  }

  onMonthlySearch = async() => {
    let startDate = `01-${constant.MONTHS_NUMERIC.get(this.selectedMonth).number}-${this.selectedYear}`
    let endDate = `${constant.MONTHS_NUMERIC.get(this.selectedMonth).endDate}-${constant.MONTHS_NUMERIC.get(this.selectedMonth).number}-${this.selectedYear}`
    const store = this.props.store.time
    this.isLoading = true
    let monthlyData = await store.getMonthlyTimeData(startDate, endDate)
    if(monthlyData){
      this.monthlyTimeData = monthlyData.data
    }
    this.isLoading = false
    //alert(startDate+'<<'+endDate)
    
  }

  onWeeklySearch = async() => {
    const store = this.props.store.time
    let weeklyData = await store.getWeeklyTimeData(utils.weeklySearchDate(this.selectedDate) )
    if(weeklyData) {
    this.weeklyTimeData = weeklyData.data
    }
  }

  openCalendar = () => {
    this.modalVisible = true
  }

  renderDayComponent = (date, state) => {
    let textStyle = {}
    let status = utils.checkForMonday(date.dateString)
    let currentDate = moment(date.dateString).format('DD-MM-YYYY')
    let currentMonday = moment(utils.getMonday(new Date())).format('DD-MM-YYYY')
    const style = currentMonday == currentDate ? styles.selectedDate : {height:20, width:20}
    if(currentMonday == currentDate) {
      textStyle = {textAlign:'center', color:constant.COLORS.WHITE}
    }
    else {
      textStyle = {textAlign: 'center', color:  !status || state === 'disabled' ? 'gray' : 'black'}
    }
    return (
      <View 
        style={style}
        >
        <TouchableOpacity
          onPress={() => this.onDayPress(date)}
          disabled={!status || state === 'disabled'}
        >
          <Text style={textStyle}>
            {date.day}
          </Text>
        </TouchableOpacity>
      </View>);
  }

  renderCalendar = () => {
    return(
      <View style={styles.pickerContainer}>
        <Calendar
        selectedDate={this.selectedDate}
        onDonePress={this.onDonePress}
        onDayPress={(date) => {this.onDayPress(date)}}
      />
      </View>
    )
    // return(
    //   <View
    //       style={styles.pickerContainer}
    //     >
    //     {this.renderPickerHeader()}
    //     <View
    //       style={styles.picker}
    //     >
    //       <Calendar
    //         dayComponent={({date, state}) => { return this.renderDayComponent(date, state)}}
    //       />
    //     </View>
    //   </View>
    // )
  }

  renderPickerOptions = ()=> {
    const options = this.pickeritems.get(this.selectedPicker.toString())
    return options.map((item) => {return <Picker.Item label={item} value={item}/>})
  }

  renderPickerHeader = () => {
    return(
      <View
            style={styles.pickerHeader}
          >
            <TouchableOpacity
              onPress={this.onDonePress}
              style={styles.headerItem}
            >
              <Text
                style={styles.headerText}
              >
                Done
              </Text>
            </TouchableOpacity>
            <View
              style={styles.headerItem}
            />
      </View>
    )
  }

  renderPicker = () => {
    const selectedValue = this.selectedPicker !== 1 ? this.selectedYear.toString() : this.selectedMonth
    return(
      <View
          style={styles.pickerContainer}
        >
        {this.renderPickerHeader()}
        <Picker
            mode={'dialog'}
            selectedValue={selectedValue}
            onValueChange={this.onPickerValueChange}
            style={styles.picker}
            itemStyle={styles.pickerItem}
          >
          {this.renderPickerOptions()}
        </Picker>
      </View>
    )
  }

  renderModal = () => {
    const selectedTab = this.selectedTab
    return(
      
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.modalVisible}
          >
            { selectedTab === 1 ? this.renderPicker() : this.renderCalendar()}
        </Modal>
    )
  }

  renderCard = ({item}) => {
    return(
        <TimeCard
          item={item}
        />
    )
  }
  
  renderWeeklyHeader = () => {
    return(
      <View
        style={styles.topTabs}
      >
        <TouchableOpacity
          onPress={this.openCalendar}
          style={[styles.monthyHeader, {width: ( (4/7) * width )}]}
        >
          <Text
            style={styles.monthlyHeaderText}
          >
            {this.selectedDate}
          </Text>
          <View style={{flex:1, alignItems:'flex-end'}}>
            <Image
              style={{height:25, width:25}}
              source={constant.IMAGES.CALENDAR}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  renderWeeklyTime = () => {
    const isEmpty = this.weeklyTimeData !== [] ? true : false
    if(this.weeklyTimeData.length === 0) {
      return(
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
          <Image
            source={constant.IMAGES.ERROR_EMOJI}
          />
          <View style={{width:( (2/5) * width )}}>
            <Button
              text={'Add Time'}
              onPress={()=>{navigation.push('LoggedIn_Stack', Screen_IDs.ADD_TIME, {selectedDate: this.selectedDate})}}
            />
          </View>
        </View>
      )
    }
    else {
      return(
        <View style={{flex:1}}>
          <FlatList
            style={{ marginTop:10, marginBottom:20}}
            extraData={isEmpty}
            data={this.weeklyTimeData}
            renderItem={this.renderCard}
          />
        </View>
      )
    }
  }


  renderWeekly = () => {
    return(
      <View style={{flex:1}}>
        {this.renderWeeklyHeader()}
        {this.renderWeeklyTime()}
        {this.renderBottomTabs(this.weeklyTimeData)}
      </View>
    )
  }

  renderMonthlyHeader = () => {
    return(
      <View>
        <View
          style={styles.topTabs}
        >
          <View style={{flex:3}}>
            <PickerComponent
              disabled={true}
              onPress={()=>{this.openPicker(1)}}
              textValue={this.selectedMonth}
            />
          </View>
          <View style={{flex:2, marginLeft:20}}>
            <PickerComponent
              disabled={true}
              onPress={()=>{this.openPicker(0)}}
              textValue={this.selectedYear}
            />
          </View>
          <TouchableOpacity
            onPress={this.onMonthlySearch}
            style={[styles.monthyHeader, {flex:1, marginLeft:20, backgroundColor:'white'}]}
          >
            <View style={styles.searchIconContainer}>
              <Image
                style={styles.searchIcon}
                source={constant.IMAGES.SEARCH_ICON}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderMonthlyTime = () => {
    const isEmpty = this.monthlyTimeData !== [] ? true : false
    if(this.monthlyTimeData.length === 0) {
      return(
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
          <Image
            source={constant.IMAGES.ERROR_EMOJI}
          />
        </View>
      )
    }
    else {
      return(
        <View style={{flex:1}}>
           <FlatList
            style={{marginTop:10, marginBottom:0}}
            extraData={isEmpty}
            data={this.monthlyTimeData}
            renderItem={this.renderCard}
          />
        </View>
      )
    }
  }

  renderMonthly = () => {
    return(
      <View style={{flex:1}}>
        {this.renderMonthlyHeader()}
        {this.renderMonthlyTime()}
        {this.renderBottomTabs(this.monthlyTimeData)}
      </View>
    )
  }

  renderTopTabs = ()=> {
    const tabSwitch = this.tabSwitch
    return(
      <View
        style={styles.topTabs}
      >
        <TouchableOpacity
          onPress={() => this.onTabSwitch(0)}
          style={[styles.tab, styles.leftTab, {backgroundColor: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE}]}
        >
          <Text
            style={[styles.text, {color: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE}]}
          >
            Weekly
          </Text>
        </TouchableOpacity>
          
        <TouchableOpacity
          onPress={() => this.onTabSwitch(1)}
          style={[styles.tab, styles.rightTab, {backgroundColor: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE}]}
        >
          <Text
            style={[styles.text, {color: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE}]}
          >
            Monthly
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderBottomTabs = (timeData)=> {
    const {billable, non_billable, total} = this.getHours(timeData)
    console.log('here')
    return(
      <View style={styles.bottomTabs}>
        {/* <Text>
          Bottom Tabs
        </Text> */}
        <View style={styles.bottomElement}>
          <Text style={styles.bottomHourText}>
            {billable.toFixed(1)}
          </Text>
          <Text  style={styles.bottomTabText}>
            BILLABLE
          </Text>
        </View>
        <View style={[styles.bottomElement, {backgroundColor: constant.COLORS.NON_BILLABLE}]}>
          <Text style={styles.bottomHourText}>
            {non_billable.toFixed(1)}
          </Text>
          <Text  style={styles.bottomTabText}>
            NON-BILLABLE
          </Text>
        </View>
        <View style={[styles.bottomElement, {backgroundColor: constant.COLORS.TOTAL_HOURS}]}>
          <Text style={styles.bottomHourText}>
            {total.toFixed(1)}
          </Text>
          <Text  style={styles.bottomTabText}>
            TOTAL HOURS
          </Text>
        </View>
      </View>
    )
  }
  
  render() {
    const selectedTab = this.selectedTab
    return(
      <SafeAreaView
        style={{flex:1, backgroundColor:constant.COLORS.WHITE}}
      >
        {this.isLoading ? <CustomActivityIndicator/> : null}
        <CustomHeader
          title = "Konnect"
          headerRightImage = {constant.IMAGES.sidebarIcon} 
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
          onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
        />
        {this.renderTopTabs()}
        { selectedTab === 1 ? this.renderMonthly() : this.renderWeekly()}
        {this.renderModal()}
      </SafeAreaView>
    )
  }
}

export const styles = StyleSheet.create({
  topTabs:{
    flexDirection:'row',
    marginTop:20,
    marginHorizontal:15
  },
  tab:{
    flex:1,
    height:40,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:1,
    borderColor:constant.COLORS.THEME_BLUE
  },
  leftTab:{
    borderRightWidth:0,
    borderTopLeftRadius:7,
    borderBottomLeftRadius: 7
  },
  rightTab:{
    borderLeftWidth:0,
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7
  },
  text:{
    alignItems:'center',
    fontSize:14,
  },
  pickerHeader:{
    flex:0.15,
    flexDirection:'row',
    backgroundColor:constant.COLORS.THEME_BLUE
  },
  picker:{
    flex:0.85,
    backgroundColor:constant.COLORS.WHITE
  },
  pickerContainer:{
    height: ( (2/5) * height ) + 60,
    marginTop:( (3/5) * height) - 60,
  },
  headerItem:{
    flex:1,
    paddingHorizontal:15,
    alignItems:'flex-start',
    justifyContent:'center',
  },
  headerText:{
    color:constant.COLORS.WHITE,
    fontSize:18
  },
  monthyHeader:{
    //justifyContent:'center',
    flexDirection:'row',
    height:35,
    borderBottomWidth:1,
    borderColor:constant.COLORS.THEME_BLUE
  },
  monthlyHeaderText:{
    color:constant.COLORS.THEME_BLUE,
    fontSize:16,
    fontWeight:'500'
  },
  icon:{
    height:20,
    width:20
  },
  searchIcon:{
    height:30,
    width:30
  },
  searchIconContainer:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:constant.COLORS.THEME_BLUE
  },
  selectedDate:{
    height: 20,
    width:20,
    borderRadius:10,
    backgroundColor:constant.COLORS.THEME_BLUE
  },
  bottomTabs:{
    height: ( (1/8) * height),
    backgroundColor:constant.COLORS.WHITE,
    paddingVertical:8,
    paddingHorizontal: 5,
    flexDirection:'row'
  },
  bottomElement:{
    marginHorizontal:4,
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:constant.COLORS.THEME_BLUE
  },
  bottomHourText:{
    color:constant.COLORS.WHITE,
    fontSize:22
  },
  bottomTabText:{
    color:constant.COLORS.WHITE,
    fontSize:14
  }
})