import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { getFromaAsyncStorage } from '../../Utils/utilFunctions';

@inject('store')
@observer
export default class ExpenseDetails extends Component {
  @observable showModal: Boolean = false;
  @observable pickerData: Object = {};
  @observable token: String = ''
  @observable projectData: Object = []

  async componentWillMount(){
    await this.getToken()
    this.getExpenseDetail()
  }
  getToken = async () => {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }
  getExpenseDetail = async () => {
    response = await this.props.store.expense.getExpenseDetails(this.token, this.props.store.expense.reportId)
    this.projectData = response.data
  }

  onPressHeaderLeftImage = () => {
    Navigation.popTo('HOMESCREEN');
  };
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  };
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title='Konnect'
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    );
  };
  renderTopView = () => {
    return (
      <View style={styles.TopView}>
        <TouchableOpacity onPress = {()=>{ Navigation.pop(this.props.componentId);}}>
          <Image source={constant.IMAGES.BLUE_BACK_ICON} style={styles.Icons} />
        </TouchableOpacity>
        <Text style={styles.TopViewText}>Expense Details</Text>
        <View />
      </View>
    );
  };
  renderDetailsView = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 30
          }}
        >
          <View>
            <Text style={styles.DetailsText}>
              Project Name: <Text>{this.projectData.projNm}</Text>
            </Text>
            <Text style={styles.DetailsText}>
              Total Amount Claimed: <Text>{this.projectData.curr} {this.projectData.amount}</Text>
            </Text>
            <Text style={styles.DetailsText}>
              Status: <Text>{this.projectData.statusTitle}</Text>
            </Text>
          </View>
          <View style={{ justifyContent: 'center' }}>    
            <TouchableOpacity onPress = {()=>{alert('Winner winner chicken dinner!!')}}>
             <Image source={constant.IMAGES.NEXT_ICON} style={styles.Icons} />
           </TouchableOpacity>
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <TouchableOpacity
            onPress={() => {
              navigation.push('LoggedIn_Stack', Screen_IDs.CLAIM_EXPENSE)
            }}
            style={{ width: '60%', marginRight: 10 }}
          >
            <Image
              source={constant.IMAGES.CLAIM_EXPENSE}
              style={{ width: '100%', height:60 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              alert('No Expenses have been claimed for this report.');
            }}
            style={{ width: '40%'}}
          >
            <Image
              source={constant.IMAGES.SUBMIT_EXPENSE}
              style={{ width: '100%', height:60 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  };
  renderNoExpenseView = () => {
    return (
      <View style={{ marginTop: 20 }}>
        <Text style={styles.NoExpenseText}>No Expense Claimed !!</Text>
        <Text style={styles.NoExpenseText}>
          Click on Claim Expense to Add Expenses
        </Text>
      </View>
    )
  };
  renderExpenseCard = () => {
    return (
      <View style={styles.ExpenseCard}>
        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
          <Text style={[styles.ExpenseCardText, { color: '#000' }]}>
            05 Apr, 2019
          </Text>
          <Text
            style={[
              styles.ExpenseCardText,
              { color: constant.COLORS.GREEN, fontWeight: 'bold' }
            ]}
          >
            INR 2, 000.00
          </Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Text style={styles.ExpenseCardText}>Expense Type:</Text>
            <Text style={styles.ExpenseCardText}>Merchant Type:</Text>
            <Text style={styles.ExpenseCardText}>Location:</Text>
            <Text style={styles.ExpenseCardText}>Description:</Text>
          </View>
          <View style={{ marginLeft: 20 }}>
            <Text
              style={[
                styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
            >
              Internet
            </Text>
            <Text
              style={[
                styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
            >
              Dominoz
            </Text>
            <Text
              style={[
                styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
            >
              Gurgaon
            </Text>
            <Text
              style={[
                styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
            >
              Party
            </Text>
          </View>
        </View>
        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
          <Text
            style={[
              styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
          >
            Billable
          </Text>
          <Text
            style={[
              styles.ExpenseCardText,{ color: '#000', fontWeight: 'bold' }]}
          >
            Reimbursable
          </Text>
        </View>
        <View style={styles.ExpenseCardBottomView}>
          <TouchableOpacity onPress = {()=>{ navigation.push('LoggedIn_Stack', Screen_IDs.EDIT_EXPENSE)}}>
            <Image source={constant.IMAGES.EDIT_ICON} style={styles.Icons} />
          </TouchableOpacity>
          <TouchableOpacity onPress = {()=>{alert('Phatt se headshot!!')}}>
            <Image source={constant.IMAGES.COPY_ICON} style={styles.Icons} />
          </TouchableOpacity>
          <TouchableOpacity onPress = {()=>{alert('Phatt se headshot!!')}}>
          <Image source={constant.IMAGES.ATTACHMENT_ICON} style={styles.Icons} />
          </TouchableOpacity>
          <TouchableOpacity onPress = {()=>{alert('Phatt se headshot!!')}}>
          <Image source={constant.IMAGES.DELETE} style={styles.Icons} />
          </TouchableOpacity>
        </View>
      </View>
    )
  };
  renderExpenses = () => {
    return (
      <View style={{ marginVertical: 20 }}>
        <Text
          style={[styles.TopViewText, { color: '#000', textAlign: 'left' }]}
        >
          Expense History
        </Text>
        {this.renderExpenseCard()}
      </View>
    )
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderCustomHeader()}
        <View style={{ flex: 1, paddingHorizontal: 10 }}>
          {this.renderTopView()}
          {this.renderDetailsView()}
          {this.renderNoExpenseView()}
          {/* {this.renderExpenses()} */}
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  TopView: {
    flexDirection: 'row',
    marginTop: 30,
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  TopViewText: {
    textAlign: 'center',
    fontSize: 26,
    fontWeight: 'bold',
    color: constant.COLORS.THEME_BLUE
  },
  DetailsText: {
    fontSize: 20,
    // fontWeight: 'bold',
    marginBottom: 5,
    color: '#000'
  },
  NoExpenseText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000'
  },
  ExpenseCard: {
    borderColor: constant.COLORS.THEME_BLUE,
    borderWidth: 1,
    paddingHorizontal: 5,
    marginTop: 20
  },
  ExpenseCardText: {
    fontSize: 20
  },
  ExpenseCardBottomView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderTopWidth: 1,
    height: 60,
    borderTopColor: 'gray'
  },
  Icons: {
    height: 25,
    width: 25
  }
});
