import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, Switch, Modal, Dimensions } from 'react-native';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import CustomTextInput from '../../Components/CustomTextInput';
import { Calendar } from 'react-native-calendars';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import Picker from 'react-native-picker';
import {openDocumentPicker, getFromaAsyncStorage, showAlert} from '../../Utils/utilFunctions'
import { stat } from 'react-native-fs';

const { height, width } = Dimensions.get('window');
@inject('store')
@observer
export default class ClaimExpense extends Component {
  @observable token: String = ''
  @observable dateOfExpense:String = 'Date of Expense'
  @observable datePlaceHolderColor: String = '#A9A9A9';
  @observable expensePlaceHolderColor:String = '#A9A9A9';
  @observable showModal: Boolean = false;
  @observable selectedDate: String = ''
  @observable expenseType:String = 'Expense Type'
  @observable expenseTypeData: Array = []
  @observable reimbursableSwitch: Boolean = false
  @observable billableSwitch: Boolean =false
  @observable attachment: Object = {}
  @observable billAmount: String = ''
  @observable merchantName: String = ''
  @observable location: String = ''
  @observable expenseDescription: String = ''
  @observable billable: String = '0'
  @observable reimbursable: String = '0'
  @observable expenseId: String = ''

  async componentWillMount(){
    await this.getToken()
    response = await this.props.store.expense.getClaimedExpenseType(this.token)
    this.expenseTypeResponse = response.data
    response.data.forEach(element => {
      this.expenseTypeData.push(element.typeTitle)
    });
  }
  getToken = async () => {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }

  onPressHeaderLeftImage = () => {
    Navigation.pop('HOMESCREEN');
  };
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  };
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title='Konnect'
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    );
  };
  resetScreen = () => {
    this.dateOfExpense = 'Date of Expense'
    this.expenseType = 'Expense Type'
    this.billAmount = ''
    this.merchantName = ''
    this.location = ''
    this.expenseDescription = ''
    this.billableSwitch = false
    this.reimbursableSwitch = false
    this.attachment = {}
    this.datePlaceHolderColor = '#A9A9A9';
    this.expensePlaceHolderColor = '#A9A9A9';
  }
  getExpenseTypeId = () => {
    const result = this.expenseTypeResponse.filter(element => element.typeTitle == this.expenseType);
    id = result[0].typeId
    return id;
  }
  claimExpense = async () => {
    expenseTypeId = this.getExpenseTypeId()
    if (this.billableSwitch == true) {
      this.billable = '1'
    }
    else
      this.billable = '0'
    if (this.reimbursableSwitch == true) {
      this.reimbursable = '1'
    }
    else
      this.reimbursable = '0'

    responseClaim = await this.props.store.expense.claimExpense(this.token, this.dateOfExpense, expenseTypeId, this.billable, this.expenseDescription, this.location, this.merchantName, this.reimbursable, this.billAmount)
    if(responseClaim){
      this.expenseId = responseClaim.data.expenseId
      if(this.attachment!=''){
        responseUpload = await this.props.store.expense.uploadExpenseAttachment(this.attachment, this.expenseId, this.props.reportId, this.token)
      }
      this.resetScreen()
      alert('Expense Claimed')
    }
    }
  validateClaimExpense = () => {
    if(this.dateOfExpense == 'Date of Expense'){
      alert('Please Enter Expense Date')
    }
    else{
      if(this.expenseType == 'Expense Type'){
        alert('Please Select Expense Type')
      }else{
        if(this.billAmount == ''){
          alert('Please Select Bill Amount')
        }else{
          if(this.merchantName == ''){
            alert('Please Select Merchant Name')
          }else{
            if(this.location == ''){
              alert('Please Select Location')
            }else{
              if(this.expenseDescription == ''){
                  alert('Please Enter Expense Description')
              }else{
                this.claimExpense()
              }
            }
          }
        }
      }
    }
  }
  renderTopView = () => {
    return (
      <View style={styles.TopView}>
        <TouchableOpacity onPress = {()=>{ Navigation.pop(this.props.componentId)}}>
          <Image
            source={constant.IMAGES.BLUE_BACK_ICON}
            style={styles.Icon}
          />
        </TouchableOpacity>
        <View style={{ justifyContent: 'center' }}>
          <Text style={styles.TopViewText}>Claim Expense</Text>
        </View>
        <TouchableOpacity
          style={styles.TopViewButtonContainer}
          onPress={()=>this.validateClaimExpense()}
        >
          <Text style={{ fontSize: 20, color: '#fff' }}>Add</Text>
        </TouchableOpacity>
      </View>
    );
  };
  renderDateView = () => {
    return (
      <View style={styles.DateViewContainer}>
        <View>
          <Text style={[styles.DateText, { fontWeight: 'bold' }]}>From:</Text>
          <Text style={[styles.DateText, { fontWeight: 'bold' }]}>To:</Text>
        </View>
        <View style={{ marginLeft: 5 }}>
          <Text style={[styles.DateText, { color: '#000' }]}>{this.props.store.expense.stDt}</Text>
          <Text style={[styles.DateText, { color: '#000' }]}>{this.props.store.expense.endDt}</Text>
        </View>
      </View>
    );
  };
  onSelectDate = async () => {
    this.dateOfExpense = this.selectedDate;
    this.datePlaceHolderColor =  constant.COLORS.THEME_BLUE;
    this.showModal = false;
  };
  renderCalender = () => {
    return (
      <Modal
        animationType='slide'
        transparent={true}
        visible={this.showModal}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.ModalView}>
          <View style={styles.ModalTopContainer}>
            <TouchableOpacity onPress={() => (this.showModal = false)}>
              <Text style={styles.ModalTopContainerText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onSelectDate()}>
              <Text style={styles.ModalTopContainerText}>Done</Text>
            </TouchableOpacity>
          </View>
          <Calendar
            theme={{
              textSectionTitleColor: 'green',
              selectedDayBackgroundColor: '#00adf5',
              selectedDayTextColor: constant.COLORS.THEME_BLUE,
              todayTextColor: '#00adf5',
              dayTextColor: '#2d4150'
            }}
            markedDates={{
              [this.selectedDate]: { selected: true, selectedColor: '#00adf5' }
            }}
            onDayPress={day => {
              console.log('selected day', day);
              this.selectedDate = day.dateString;
            }}
          />
        </View>
      </Modal>
    );
  };
  showPicker = () => {
    {
      Picker.init({
        pickerData: this.expenseTypeData,
        pickerConfirmBtnColor: [255, 255, 255, 1],
        pickerCancelBtnColor: [255, 255, 255, 1],
        pickerToolBarBg: [4, 121, 187, 1],
        pickerBg: [255, 255, 255, 1],
        pickerTitleText: '',
        pickerConfirmBtnText: 'Done',
        pickerCancelBtnText: 'Cancel',
        pickerToolBarFontSize: 20,
        pickerFontSize: 20,
        onPickerConfirm: data => {
          console.log(data);
            this.expensePlaceHolderColor =  constant.COLORS.THEME_BLUE;
            this.expenseType = data;
        },
        onPickerCancel: data => {
          console.log(data);
        },
        onPickerSelect: data => {
          console.log(data);
        }
      });
    }
    {Picker.show()}
  }
  renderInputRow = (text, image, onPress = () => {}, color) => {
    return (
      <TouchableOpacity style={styles.InputRowContainer} onPress={onPress}>
        <Text style={{ fontSize: 18, color: color }}>{text}</Text>
        <Image style={styles.Icon} source={image} resizeMode={'contain'} />
      </TouchableOpacity>
    );
  };
  showDateCalendar = () => {
    this.showModal = true;
  }

  renderInputView = () => {
    return (
      <View style={{ marginHorizontal: 50, marginTop:20 }}>
        {this.renderInputRow(this.dateOfExpense, constant.IMAGES.CALENDAR, this.showDateCalendar, this.datePlaceHolderColor)}
          {this.renderInputRow(this.expenseType, constant.IMAGES.DROPDOWN, () => {this.showPicker()}, this.expensePlaceHolderColor)}

        <CustomTextInput
          placeholder='Bill Amount'
          onChangeText={(text) => {
            let reg = new RegExp('^[0-9]+$')
            if (!reg.test(text) && text != '') {
              showAlert('Please Enter Valid Amount')
              this.billAmount = text.substring(0, (text.length - 1))
            } else{
               this.billAmount = text
             }
          }
          }
          value = {this.billAmount}
        />
        <CustomTextInput
          placeholder='Merchant Name'
          onChangeText={(text)=>this.merchantName = text}
          value = {this.merchantName}
        />
        <CustomTextInput
          placeholder='Location'
          onChangeText={(text)=>this.location = text}
          value = {this.location}
        />
        <CustomTextInput
          placeholder='Expense Description'
          onChangeText={(text)=>this.expenseDescription = text}
          value = {this.expenseDescription}
        />
      </View>
    );
  };
  uploadAttachments = async () => {
    openDocumentPicker(async (file) => { this.attachment = file })
  }
  renderBottomView = () => {
    return (
      <View>
        <View style={styles.BottomViewSubContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.BottomViewText}>Billable</Text>
	 					<Switch onValueChange = {()=>this.billableSwitch=!this.billableSwitch} value = {this.billableSwitch}/>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.BottomViewText}>Reimbursable</Text>
	 					<Switch onValueChange = {()=>this.reimbursableSwitch = !this.reimbursableSwitch} value = {this.reimbursableSwitch}/>
          </View>
        </View>
        <View style={{ justifyContent: 'center', marginTop: 10, flexDirection:'row', alignItems:'center' }}>
		 			<TouchableOpacity onPress = {()=>{this.uploadAttachments()}}>
              <Image source = {constant.IMAGES.ATTACHMENT_ICON} style={[styles.Icon,{marginRight:5}]}/>
           </TouchableOpacity>
          <Text style={styles.BottomViewText}>File Upload</Text>
        </View>
      </View> 
    );
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor:constant.COLORS.WHITE}}>
        {this.renderCustomHeader()}
        {this.renderTopView()}
        {this.renderDateView()}
        {this.renderInputView()}
        {this.renderBottomView()}
        {this.renderCalender()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  TopView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    marginRight: 10,
    marginLeft: 20
  },
  TopViewText: {
    textAlign: 'center',
    fontSize: 26,
    fontWeight: 'bold',
    color: constant.COLORS.THEME_BLUE
  },
  TopViewButtonContainer: {
    backgroundColor: constant.COLORS.THEME_BLUE,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: 100
  },
  DateViewContainer: {
    marginHorizontal: 50,
    marginTop: 20,
    flexDirection: 'row'
  },
  DateText: {
    color: 'green',
    fontSize: 20
  },
  BottomViewSubContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 50,
    marginVertical: 10
  },
  BottomViewText: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  InputRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: constant.COLORS.THEME_BLUE
  },
  ModalView: {
    width: width,
    height: height / 2,
    position: 'absolute',
    bottom: 0
  },
  ModalTopContainer: {
    backgroundColor: constant.COLORS.THEME_BLUE,
    height: 44,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  ModalTopContainerText: {
    textAlign: 'center',
    fontSize: 24,
    color: constant.COLORS.WHITE,
    marginHorizontal: 20
  },
  Icon: {
    height: 35,
    width: 35
  }
});
