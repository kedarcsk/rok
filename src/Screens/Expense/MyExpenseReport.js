import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList
} from 'react-native';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { renderIf, getFromaAsyncStorage } from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';

const { height, width } = Dimensions.get('window');

@inject('store')
@observer
export default class MyExpenseReport extends Component {
  @observable token: String = ''
  @observable expenseData: Array = []

  async componentWillMount(){
    await this.getToken()
    this.getMyExpenseReportList()
  }
  getToken = async () => {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }
  getMyExpenseReportList = async () => {
    response = await this.props.store.expense.getExpenseReportList(this.token)
    this.expenseData = response.data
  }
  onPressHeaderLeftImage = () => {
    Navigation.popTo('HOMESCREEN');
  };
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  };
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title='Konnect'
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    );
  };
  onPressCopy = async (reportId) => {
    response = await this.props.store.expense.copyExpenseReport(this.token, reportId)
    if(response){
      this.getMyExpenseReportList()
    }
  }
  onPressDelete = async (reportId) => {
    response = await this.props.store.expense.deleteExpenseReport(this.token, reportId)
    if(response){
      this.getMyExpenseReportList()
    }
  }
  renderBottomCardView = (item) => {
    return(
      <View style={styles.ExpenseCardBottomView}>
        <TouchableOpacity onPress={() => {this.onPressCopy(item.reportId)}}>
          <Image source={constant.IMAGES.COPY_ICON} style={styles.Icons} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          this.saveData(item)
          navigation.push(this.props.componentId, Screen_IDs.EXPENSE_DETAILS)}}>
          <Image source={constant.IMAGES.EYE_ICON} style={styles.Icons} />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>{ navigation.push('LoggedIn_Stack', Screen_IDs.EDIT_EXPENSE, {reportId:item.reportId, projId:item.projId})}}>
          <Image source={constant.IMAGES.EDIT_ICON} style={styles.Icons} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {this.onPressDelete(item.reportId)}}>
          <Image source={constant.IMAGES.DELETE} style={styles.Icons} />
        </TouchableOpacity>
      </View>
    )
  }
  saveData = (item) => {
    this.props.store.expense.reportId = item.reportId
    this.props.store.expense.stDt = item.stDt
    this.props.store.expense.endDt = item.endDt
    this.props.store.expense.currId = item.currId
    this.props.store.expense.projId = item.projId
    this.props.store.expense.expenseData = item
  }
  renderExpenseReportCard = () => {
    return (
      <FlatList
        data={this.expenseData}
        renderItem={({ item, index }) => (
          <View style={styles.Card}>
            <View style={{ flex: 0.95 }}>
              <View>
                <Text style={[styles.HeaderText, { color: constant.COLORS.THEME_BLUE }]}>{item.reportTitle}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.NormalText}>From: {item.stDt}</Text>
                  <Text style={styles.NormalText}>To: {item.endDt}</Text>
                </View>
                <View style={{ marginTop: 15, marginBottom: 15 }}>
                  <Text style={styles.NormalText}>{item.statusTitle}</Text>
                  <Text style={[styles.NormalText,{color:'green'}]}>{item.curr} {item.amount}</Text>
                </View>
              </View>
              {this.renderBottomCardView(item)}
            </View>
            <View style={{ flex: 0.05, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() =>{
                this.saveData(item)
                navigation.push(this.props.componentId, Screen_IDs.EXPENSE_DETAILS)}}>
                <Image source={constant.IMAGES.NEXT_ICON} style={styles.Icons} />
              </TouchableOpacity>
            </View>
          </View>
        )
        }
      />
    )
  }
  expenseReportContainer = () => {
    return (
      <View style = {{flex:1}}>
        <Text style={[styles.HeaderText, { marginTop: 25, marginBottom: 15, color: '#000' }]}>MY Expense Report</Text>
        {this.renderExpenseReportCard()}
      </View>
    )
  }
  renderButtonComponent = () => {
    return(
      <View style={styles.ButtonContainer}>
        <CustomButton text='Add Expenses'
          onPress={()=>navigation.push(this.props.componentId, Screen_IDs.ADD_EXPENSE)}
        />
      </View>
    )
  }
  renderNoExpenseContainer = () => {
    return ( 
      <View style={styles.NoExpenseContainer}>
        <View style={{alignItems:'center'}}>
          <Image source={constant.IMAGES.SMILEY_ICON} style={{ height: 150, width: 150 }} resizeMode = 'contain' />
          <Text style={styles.NoExpenseText}>No Expense Reports Yet</Text>
          <Text style={{ fontSize: 18, fontWeight:'bold'}}>Add Expense Now</Text>
        </View>
        {this.renderButtonComponent()}
      </View> 
    )
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this.renderCustomHeader()}
        <Image
          source={constant.IMAGES.EXPENSE_BG}
          style={styles.ExpenseBG}
        />
        {this.expenseData.length!=0? this.expenseReportContainer(): this.renderNoExpenseContainer()}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  ExpenseBG:{
      width: width, 
      position: 'absolute', 
      // top: 44
  },
  HeaderText: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  NormalText: {
    fontSize: 25,
    textAlign: 'left',
    fontSize: 20,
    marginLeft: 10,
    color: '#2D2D2E'
  },
  Card: {
    backgroundColor: constant.COLORS.WHITE,
    flexDirection: 'row',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#fff',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 6,
    marginHorizontal: 5,
    marginVertical: 5
  },
  Icons: {
    height: 25,
    width: 25
  },
  ExpenseCardBottomView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderTopWidth: 1,
    height: 60,
    borderTopColor: 'gray'
  },
  NoExpenseContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ExpenseBG:{
    width: width, 
    position: 'absolute', 
    top: 44
  },
  ButtonContainer:{
    marginVertical:20,
    marginHorizontal:50
  },
  NoExpenseText:{ 
    fontWeight: 'bold',
    color: 'orange',
    fontSize:24, 
    marginVertical:10
  }
});
