import React, { Component } from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import CustomButton from '../../Components/CustomButton';
import * as navigation from '../../Utils/Navigation';
import * as Screen_IDs from '../../Utils/Screen_IDs';
import CustomTextInput from '../../Components/CustomTextInput';
import { Calendar } from 'react-native-calendars';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import { getFromaAsyncStorage } from '../../Utils/utilFunctions';

const { height, width } = Dimensions.get('window');
@inject('store')
@observer
export default class EditExpense extends Component {
  @observable token: String = ''
  @observable showModal: Boolean = false;
  @observable selectedDate: String = '';
  @observable startDate: String = 'Start Date';
  @observable endDate: String = 'End Date';
  @observable title: String = ''
  @observable project: String = 'Project';
  @observable currency: String = 'Currency';
  @observable startDatePlaceHolderColor: String = '#A9A9A9';
  @observable endDatePlaceHolderColor: String = '#A9A9A9';
  @observable projectPlaceHolderColor: String = '#A9A9A9';
  @observable currencyPlaceHolderColor: String = '#A9A9A9';
  @observable isStartDatePickerOpened: Boolean = false;
  @observable isEndDatePickerOpened: Boolean = false;
  @observable projectList: Array = []
  @observable currencyList: Array = []
  @observable projectListObject: Array = []
  @observable currencyListObject: Array = []
  @observable startDateTimeStamp: String = ''
  @observable endDateTimeStamp: String = ''

  async componentWillMount(){
    await this.getToken()
  }
  getToken = async () => {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }

  onPressHeaderLeftImage = () => {
    Navigation.popTo('HOMESCREEN');
  };
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  };
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title='Konnect'
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    );
  };
  onSelectDate = async () => {
    if (this.isStartDatePickerOpened == true && this.isEndDatePickerOpened == false) {
        this.startDate = this.selectedDate;
        this.startDatePlaceHolderColor =  constant.COLORS.THEME_BLUE;
        this.showModal = false;
    }
    if (this.isStartDatePickerOpened == false && this.isEndDatePickerOpened == true) {
      if(this.startDateTimeStamp < this.endDateTimeStamp){
        this.endDate = this.selectedDate;
        this.endDatePlaceHolderColor =  constant.COLORS.THEME_BLUE;
        this.showModal = false;
      }
      else{
        alert('End Date should be greater than Start Date')
      }
    }
  };
  renderCalender = () => {
    return (
      <Modal
        animationType='slide'
        transparent={true}
        visible={this.showModal}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.ModalView}>
          <View style={styles.ModalTopContainer}>
            <TouchableOpacity onPress={() => (this.showModal = false)}>
              <Text style={styles.ModalTopContainerText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onSelectDate()}>
              <Text style={styles.ModalTopContainerText}>Done</Text>
            </TouchableOpacity>
          </View>
          <Calendar
            theme={{
              textSectionTitleColor: 'green',
              selectedDayBackgroundColor: '#00adf5',
              selectedDayTextColor: constant.COLORS.THEME_BLUE,
              todayTextColor: '#00adf5',
              dayTextColor: '#2d4150'
            }}
            markedDates={{
              [this.selectedDate]: { selected: true, selectedColor: '#00adf5' }
            }}
            onDayPress={day => {
              console.log('selected day', day);
              if (this.isStartDatePickerOpened == true && this.isEndDatePickerOpened == false) {
                this.startDateTimeStamp = day.timestamp
              }
              if (this.isStartDatePickerOpened == false && this.isEndDatePickerOpened == true) {
                this.endDateTimeStamp = day.timestamp
              }
              this.selectedDate = day.dateString;
            }}
          />
        </View>
      </Modal>
    );
  };
  saveEditExpense = async () => {
    response = await this.props.store.expense.editExpenseReport(this.token, this.props.reportId, this.title, this.startDate, this.endDate, this.props.projId)
    if(response){
      navigation.push(this.props.componentId, Screen_IDs.MY_EXPENSE_REPORT)
    }
  }
  renderInputRow = (text, image, onPress = () => {}, color) => {
    return (
      <TouchableOpacity style={styles.InputRowContainer} onPress={onPress}>
        <Text style={{ fontSize: 18, color: color }}>{text}</Text>
        <Image style={styles.Icon} source={image} resizeMode={'contain'} />
      </TouchableOpacity>
    );
  };
  showStartDateCalendar = () => {
    this.showModal = true;
    this.isStartDatePickerOpened = true;
    this.isEndDatePickerOpened = false;
  }
  showEndDateCalendar = () => {
    if(this.startDate=='Start Date'){
      alert('Please Select Start Date')
    }
    else{
      this.showModal = true;
      this.isStartDatePickerOpened = false;
      this.isEndDatePickerOpened = true;
    }
  }
  renderInputContainer = () => {
    return (
      <View style={styles.InputContainer}>
        {this.renderInputRow(
          this.startDate,
          constant.IMAGES.CALENDAR,
          this.showStartDateCalendar,
          this.startDatePlaceHolderColor
        )}
        {this.renderInputRow(this.endDate,constant.IMAGES.CALENDAR,
          this.showEndDateCalendar,
          this.endDatePlaceHolderColor
        )}  
        <CustomTextInput
          placeholder='Expense Title'
          onChangeText={(text)=>{this.title = text}}
        />
        <CustomButton
          text='Save'
          onPress={() =>this.saveEditExpense() }
        />
      </View>
    );
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: constant.COLORS.WHITE }}>
        {this.renderCustomHeader()}
        <Text style={styles.HeaderText}>Edit Expense Report</Text>
        {this.renderInputContainer()}
        {this.renderCalender()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  HeaderText: {
    fontSize: 30,
    color: constant.COLORS.THEME_BLUE,
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 20
  },
  InputContainer: {
    borderWidth: 2,
    borderColor: constant.COLORS.THEME_BLUE,
    borderRadius: 10,
    marginHorizontal: 20,
    marginTop: 20,
    padding: 20
  },
  InputRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: constant.COLORS.THEME_BLUE
  },
  ModalView: {
    width: width,
    height: height / 2,
    position: 'absolute',
    bottom: 0
  },
  ModalTopContainer: {
    backgroundColor: constant.COLORS.THEME_BLUE,
    height: 44,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  ModalTopContainerText: {
    textAlign: 'center',
    fontSize: 24,
    color: constant.COLORS.WHITE,
    marginHorizontal: 20
  },
  Icon: {
    height: 35,
    width: 35
  }
});
