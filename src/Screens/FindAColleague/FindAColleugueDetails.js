import React, { Component } from 'react';
import { Text, View, StyleSheet, StatusBar, Image, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import Constants from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import constant from '../../Assests/constants/constant';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import { OpenSocial, makeCall, sendText, sendMail } from '../../Utils/SocialInfoUtil';
import { getFromaAsyncStorage } from '../../Utils/utilFunctions';

@inject('store')
@observer
export default class FindAColleagueDetails extends Component {
  @observable selectedTab: Number = 0
  @observable tabSwitch: Boolean = true
  @observable userDetails: Object = {}
  @observable token: String = ''

  constructor(props) {
    super(props);
  }
  async componentWillMount() {
    await this.getToken()
    this.getUserDetails()
  }
  getToken = async () => {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }
  getUserDetails = async () => {
    const id = this.props.empId
    let response = await this.props.store.fac.getSearchResultForEmployee(id, this.token);
    if (response.data.length != 0) {
      this.userDetails = response.data
    }
  }
  onPressHeaderLeftImage = () => {
    Navigation.pop('HOMESCREEN');
  }
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title="Konnect"
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    )
  }
  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  }
  renderTopView = () => {
    return (
      <View style={Styles.topViewContainer}>
        <TouchableOpacity onPress={() => this.onPressBack()} style={{ justifyContent: 'center' }}>
          <Image source={constant.IMAGES.BLUE_BACK_ICON} style={Styles.TopViewImage}></Image>
        </TouchableOpacity>
        <View style={{ justifyContent: 'center' }}>
          <Text style={{ fontSize: 24, color: constant.COLORS.THEME_BLUE, fontWeight: 'bold' }}>Employee Details</Text>
        </View>
        <View>
        </View>
      </View>
    )
  }
  verifyProfileImage = (image) => {
    imgSource = { uri: image }
    imgPlaceHolder = constant.IMAGES.PROFILE_PLACEHOLDER
    imageVerifier = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i
    source = imageVerifier.test(image) ? imgSource : imgPlaceHolder
    return source;
  }
  renderProfileView = () => {
    const { profImg, empNm, desig, corpId } = this.userDetails
    return (
      <View style={{ alignItems: 'center' }}>
        <View style={Styles.profileImageContainer}>
          <Image source={this.verifyProfileImage(profImg)}
            style={Styles.profileImage}></Image>
        </View>
        <View>
          <Text style={[Styles.profileViewText, { fontWeight: 'bold' }]}>{empNm}</Text>
          <Text style={Styles.profileViewText}>{desig}</Text>
          <Text style={Styles.profileViewText}>{corpId}</Text>
        </View>
      </View>
    )
  }
  onPressSocialIcon = (socialPlatform) => {
    const { mobNo, skype, emailId } = this.userDetails
    switch (socialPlatform) {
      case 'call':
        makeCall(mobNo)
        break;
      case 'skype':
        OpenSocial(constant.SOCIAL_PLATFORM.SKYPE, skype)
        break;
      case 'chat':
        sendText(mobNo)
        break;
      case 'email':
        sendMail(emailId)
    }
  }
  renderSocialSubView = (text, source, socialPlatform) => {
    return (
      <TouchableOpacity style={Styles.socialIconContainer} onPress={() => this.onPressSocialIcon(socialPlatform)}>
        <Image source={source} style={{ width: 40, height: 40 }}></Image>
        <Text>{text}</Text>
      </TouchableOpacity>
    )
  }
  renderSocialOptions = () => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginVertical: 10 }}>
        {this.renderSocialSubView('Call', constant.IMAGES.CALL_ICON, 'call')}
        {this.renderSocialSubView('Chat', constant.IMAGES.CHAT_ICON, 'chat')}
        {this.renderSocialSubView('Email', constant.IMAGES.EMAIL_ICON, 'email')}
        {this.renderSocialSubView('Skype', constant.IMAGES.SKYPE_ICON, 'skype')}
      </View>
    )
  }
  onTabSwitch = (selectedTab) => {
    if (!(this.selectedTab === selectedTab)) {
      this.tabSwitch = !this.tabSwitch
    }
    this.selectedTab = selectedTab
  }
  renderTabs = () => {
    const tabSwitch = this.tabSwitch
    return (
      <View
        style={Styles.topTabs}
      >
        <TouchableOpacity
          onPress={() => this.onTabSwitch(0)}
          style={[Styles.tab, Styles.leftTab, { backgroundColor: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE }]}
        >
          <Text
            style={[Styles.text, { color: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE }]}
          >
            Contact Details
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.onTabSwitch(1)}
          style={[Styles.tab, Styles.rightTab, { backgroundColor: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE }]}
        >
          <Text
            style={[Styles.text, { color: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE }]}
          >
            Official Details
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
  renderUserDetailsRow = (title, detail) => {
    return (
      <View style={Styles.userDetailsRowContainer}>
        <Text style={Styles.userDetailTitle}>{title}</Text>
        <Text style={Styles.userDetailText}>{detail}</Text>
      </View>
    )
  }
  renderUserDetails = () => {
    const { repMngr, desig, com, divi, loc, dept, bu, emailId, mobNo, skype, seat, lanno, ext } = this.userDetails
    const selectedTab = this.selectedTab
    if (selectedTab == 1) {
      return (
        <ScrollView style={Styles.userDetailsContainer}>
          {this.renderUserDetailsRow('Reporting Manager', repMngr)}
          {this.renderUserDetailsRow('Designation', desig)}
          {this.renderUserDetailsRow('Company', com)}
          {this.renderUserDetailsRow('Division', divi)}
          {this.renderUserDetailsRow('Location', loc)}
          {this.renderUserDetailsRow('Department', dept)}
          {this.renderUserDetailsRow('Business Unit', bu)}
          <View style={{ height: 20 }}></View>
        </ScrollView>
      )
    }
    else {
      return (
        <ScrollView style={Styles.userDetailsContainer}>
          {this.renderUserDetailsRow('Email', emailId)}
          {this.renderUserDetailsRow('Mobile', mobNo)}
          {this.renderUserDetailsRow('Skype', skype)}
          {this.renderUserDetailsRow('Seating Location', seat)}
          {this.renderUserDetailsRow('LandLine no', lanno)}
          {this.renderUserDetailsRow('Extension', ext)}
          <View style={{ height: 20 }}></View>
        </ScrollView>
      )
    }
  }
  render() {
    return (
      <View style={Styles.container}>
        <StatusBar
          backgroundColor={Constants.COLORS.THEME_BLUE}
        />
        {this.renderCustomHeader()}
        {this.renderTopView()}
        {this.renderProfileView()}
        {this.renderSocialOptions()}
        {this.renderTabs()}
        {this.renderUserDetails()}
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.COLORS.WHITE
  },
  TopViewImage: {
    width: 25,
    height: 25
  },
  topTabs: {
    flexDirection: 'row',
    marginTop: 20,
    marginHorizontal: 15
  },
  tab: {
    flex: 1,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: constant.COLORS.THEME_BLUE
  },
  leftTab: {
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7
  },
  rightTab: {
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7
  },
  topViewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 20
  },
  profileImageContainer: {
    alignItems: 'center',
    marginVertical: 10,
    justifyContent: 'center'
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderColor: constant.COLORS.THEME_BLUE,
    borderWidth: 1
  },
  profileViewText: {
    fontSize: 14,
    textAlign: 'center'
  },
  userDetailsContainer: {
    flex: 1,
    marginHorizontal: 15,
    padding: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 6 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    elevation: 2,
    borderColor: 'grey',
  },
  userDetailsRowContainer: {
    borderBottomWidth: 1,
    borderBottomColor: 'grey'
  },
  userDetailTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingVertical: 10
  },
  userDetailText: {
    fontSize: 14,
    paddingBottom: 5
  },
  socialIconContainer: {
    alignItems: 'center',
    marginVertical: 10,
    justifyContent: 'center'
  }
})