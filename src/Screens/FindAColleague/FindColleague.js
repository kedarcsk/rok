import React, { Component } from 'react';
import {  View , StyleSheet , Image, TextInput, TouchableOpacity, StatusBar, PermissionsAndroid, FlatList, Text, Keyboard } from 'react-native';
import constant from '../../Assests/constants/constant'
import CustomHeader from '../../Components/CustomHeader';
import { renderIf, getFromaAsyncStorage } from '../../Utils/utilFunctions';
import {observer, inject} from 'mobx-react'
import {observable} from 'mobx'
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import {OpenSocial, makeCall, sendText, sendMail} from '../../Utils/SocialInfoUtil'
import CustomActivityIndicator from '../../Components/CustomActivityIndicator'
import { Navigation } from 'react-native-navigation';
import Voice from 'react-native-voice';

@inject('store')
@observer
export default class FindColleague extends Component {
  @observable showUserList:Boolean = true
  @observable showUserCards:Boolean = false
  @observable searchedText:String = ''
  @observable searchedTextResponse:Object = {}
  @observable searchedDetailsResponse:Object = {}
  @observable isLoading: Boolean = false
  @observable token:String = ''
  @observable isListening:Boolean = false
  @observable isSpeaking:Boolean = false
  //............................................................................................//
  //Voice recognition listeners
  constructor(props) {
    super(props);
    Voice.onSpeechStart = this.onSpeechStart;
    Voice.onSpeechRecognized = this.onSpeechRecognized
    Voice.onSpeechResults = this.onSpeechResults
    Voice.onSpeechEnd = this.onSpeechEnd
  }
  componentWillUnmount = async () => {
    await Voice.destroy()
    Voice.removeAllListeners
  }
  onSpeechStart = (e) => {
  };

  onSpeechEnd = (e) => {
    this.isListening = false
  }
  onSpeechRecognized = (e) => {

  };
  onSpeechResults = (e) => {
    this.isListening = false
    this.searchedText = e.value[0]
    this.searchUserList(this.searchedText)
  }
  startRecognition = async (e) => {
    this.isListening = true
    try {
      await Voice.start('en-US');
    } catch (e) {
      console.error(e);

    }
  }
  //............................................................................................//
  async componentWillMount() {
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }
  onPressHeaderLeftImage = () => {
    Navigation.pop('HOMESCREEN');
  }
  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }
  renderCustomHeader = () => {
    return (
      <CustomHeader
        title="Konnect"
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={this.onPressHeaderLeftImage}
        onPressHeaderRightImage={this.onPressHeaderRightImage}
      />
    )
  }
  searchButtonPressed = async () => {
    Keyboard.dismiss()
    this.showUserList = false
    this.showUserCards = true
    this.isLoading = true
    let response = await this.props.store.fac.getUserDetails(this.searchedText, this.token);
    this.searchedDetailsResponse = response
    this.isLoading = false
  }
  checkMicPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the mic');
        this.startRecognition()
      } else {
        console.log('Mic permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }
  renderSearchBar = () => {
    return(
      <View style={styles.headerContainer}>
        <TextInput style={styles.findColleagueInput}
          placeholder="Find a Colleague"
          placeholderTextColor = {constant.COLORS.THEME_BLUE}
          onChangeText = {this.searchUserList}
          value = {this.searchedText}
        />
        <View style={{flexDirection:'row'}}>
          <TouchableOpacity onPress={this.checkMicPermission} style={styles.micButtonView}>
            <Image source= {this.isListening ? constant.IMAGES.VOICE_ICON_ACTIVE: constant.IMAGES.VOICE_ICON_INACTIVE}
             style={styles.micIcon} />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.searchButtonPressed} style={styles.searchButtonView}>
            <Image source={constant.IMAGES.searchIcon} style={styles.searchBarIcon} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  onPressSearchedName = async (text) => {
    this.showUserList = false
    this.showUserCards = true
    this.isLoading = true
    let response = await this.props.store.fac.getUserDetails(text, this.token);
    this.searchedDetailsResponse = response
    this.isLoading = false
  }
  searchUserList = async (text) => {
    this.showUserList = true
    this.showUserCards = false
    this.searchedText = text
    this.isLoading = true
    if (this.searchedText != '' ) {
      let response = await this.props.store.fac.getUserList(text, this.token);
      if(this.searchedText != ''){
        this.searchedTextResponse = response
      }
    }
    else {
      this.searchedTextResponse = ''
      this.searchedDetailsResponse = ''
    }
    this.isLoading = false
  }
  renderUsersList = () => {
    return (
      <View>
        <FlatList
          data={this.searchedTextResponse.data}
          renderItem={({ item, index }) => (
            <TouchableOpacity style={styles.userListContainer} onPress={() => this.onPressSearchedName(item.empNm)}>
              <Text style={styles.userListText}>{item.empNm}</Text>
            </TouchableOpacity>
          )
          }
        />
      </View>
    )
  }
  onPressSocialIcon = (socialPlatform, url) => {
    console.log(socialPlatform,url)
    switch (socialPlatform) {
      case 'call':
        makeCall(url)
        break;
      case 'skype':
        OpenSocial(constant.SOCIAL_PLATFORM.SKYPE, url)
        break;
      case 'chat':
        sendText(url)
        break;
      case 'email':
        sendMail(url)
    }
  }
  renderSocialIcons = (icon, socialPlatform, url) => {
    return (
      <TouchableOpacity onPress={() => this.onPressSocialIcon(socialPlatform, url)}>
        <Image source={icon}
          style={styles.userCardSocialIcons}
        />
      </TouchableOpacity>
    )
  }
  onPressNextButton = (id) => {
    navigation.push('LoggedIn_Stack', Screen_IDs.FIND_COLLEAGUE_DETAILS, { empId: id })
  }
  verifyProfileImage = (image) => {
    imgSource = { uri: image }
    imgPlaceHolder = constant.IMAGES.PROFILE_PLACEHOLDER
    imageVerifier = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i
    source = imageVerifier.test(image) ? imgSource : imgPlaceHolder
    return source;
  }
  renderUserCards = () => {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={this.searchedDetailsResponse.data}
          renderItem={({ item, index }) => (
            <TouchableOpacity
               onPress={() => this.onPressNextButton(item.empId)}
               style={[styles.userCards, { backgroundColor: index % 2 ? '#DEE1E3' : '#E8F5FF' }]}>
              <View style={{ justifyContent: 'center' }}>
                <Image source={this.verifyProfileImage(item.profImg)}
                  style={styles.userCardProfileImage}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[styles.userCardText, { fontWeight: 'bold', marginTop: 10 }]}>{item.empNm}</Text>
                <Text style={styles.userCardText}>{item.emailId}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={styles.userCardText}>{item.corpId}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    {this.renderSocialIcons(constant.IMAGES.CALL_ICON, 'call', item.mobNo)}
                    {this.renderSocialIcons(constant.IMAGES.CHAT_ICON, 'chat', item.mobNo)}
                    {this.renderSocialIcons(constant.IMAGES.EMAIL_ICON, 'email', item.emailId)}
                    {this.renderSocialIcons(constant.IMAGES.SKYPE_ICON, 'skype', item.skype)}
                  </View>
                </View>
                <Text style={[styles.userCardText, { marginBottom: 10 }]}>{item.desig}</Text>
              </View>
              <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => this.onPressNextButton(item.empId)}>
                <Image source={constant.IMAGES.NEXT_ICON}
                  style={styles.userCardForwardImage}
                />
              </TouchableOpacity>
            </TouchableOpacity>
          )
          }
        />
      </View>
    )
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor={constant.COLORS.THEME_BLUE}
        />
        {renderIf(this.isLoading, <CustomActivityIndicator />)}
        {this.renderCustomHeader()}
        {this.renderSearchBar()}
        {renderIf(this.showUserList, this.renderUsersList())}
        {renderIf(this.showUserCards, this.renderUserCards())}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  userListContainer: {
    margin: 15
  },
  userCards: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 2,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 6 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    elevation: 2,
    borderBottomColor: 'grey',
    borderBottomWidth: 4
  },
  headerContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: constant.COLORS.THEME_BLUE,
    marginHorizontal: 12,
    paddingLeft: 10,
    height: 40,
    marginTop: 15,
    justifyContent: 'space-between',
    marginVertical: 20
  },
  micButtonView:{
    alignItems:'center',
    justifyContent:'center',
    marginHorizontal:5
  },
  searchButtonView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: constant.COLORS.THEME_BLUE,
    width:40
  },
  findColleagueInput: {
    color: constant.COLORS.THEME_BLUE,
    flex:1
  },
  searchBarIcon: {
    width: 20,
    height: 20
  },
  micIcon:{
    height:30,
    width:30
  },
  searchIcon: {
    width: 20,
    height: 20
  },
  userCardProfileImage: {
    height: 80,
    width: 80,
    marginHorizontal: 5,
    borderRadius: 40
  },
  userCardForwardImage: {
    height: 30,
    width: 15,
    marginHorizontal: 15
  },
  userCardText: {
    fontSize: 14,
    marginVertical: 5
  },
  userCardSocialIcons: {
    height: 30,
    width: 30,
    marginHorizontal: 5
  },
  userListText: {
    fontSize: 18,
    fontWeight: 'bold'
  }
})