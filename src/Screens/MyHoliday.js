import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, FlatList} from 'react-native'
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
import constant from '../Assests/constants/constant'
import PickerComponent from '../Components/PickerComponent'
import CustomHeader from '../Components/CustomHeader'
import HolidayCard from '../Components/HolidayCard'
import * as navigation from '../Utils/Navigation'
import picker from 'react-native-picker'
import {observable} from 'mobx'
import {observer, inject} from 'mobx-react'

const normal_Shiftdata = ['a', 'b', 'c', 'd']
const us_ShiftData = ['a', 'b', 'c', 'd']
const pickeritems = ['2017', '2018', '2019', '2020', '2021', '2022', '2023']

@inject('store')
@observer
export default class MyHoliday extends Component {
  
  @observable selectedTab:Number = 0
  @observable selectedYear:String = '0001'
  @observable tabSwitch:Boolean = true
  @observable isLoading:Boolean = false
  @observable titles:Array = []
  @observable tabsMap:Map = new Map()
  @observable holidayList:Array = []

  async componentWillMount() {
    this.selectedYear = new Date().getFullYear()
    this.onHolidaySearch()
  }

  getTabStyle = (index) => {
   if(index === 0 ) {
     return styles.leftTab
   }
   else if( index === this.titles.length -1 ) {
     return styles.rightTab
   }
   else return {}
  }

  onHolidaySearch = async() => {
    this.selectedTab = 0
    this.isLoading = true
    const store = this.props.store.login
    let response = await store.fetchHolidays(this.selectedYear)
    if(response) {
      this.titles = response.reduce(((acc, obj)=>{
        this.tabsMap.set(obj.title, obj.holidayList)
        acc.push(obj.title)
        return acc
      }), [])
      this.holidayList = this.tabsMap.get(this.titles[0])
    }
    this.isLoading = false
  }

  onTabSwitch = (item, selectedTab) => {
    if(!(this.selectedTab === selectedTab)) {
      this.tabSwitch = !this.tabSwitch
    }
    this.selectedTab = selectedTab
    this.holidayList = this.tabsMap.get(item)
  }

  openPicker = () => {
    const selectedValue = this.selectedYear
    picker.init({
      pickerToolBarBg:[3, 105, 162, 1],
      pickerTitleText:'',
      pickerBg:[255, 255, 255, 1],
      pickerConfirmBtnText: '',
      pickerCancelBtnText: 'Close',
      pickerConfirmBtnColor:[255, 255, 255, 1],
      pickerCancelBtnColor:[255, 255, 255, 1],
      pickerFontSize:20,
      pickerFontFamily: 'aller',
      pickerToolBarFontSize:18,
      pickerData: pickeritems,
      selectedValue: [selectedValue.toString()],
      onPickerCancel: data => {
        picker.hide()
      },
      onPickerSelect: data => {
        this.selectedYear = data[0]
      }
    })
    picker.show()
  }


  renderSearchbar = () => {
    return(
      <View style={styles.searchBar}>
        <View style={{flex:3}}>
          <PickerComponent
            disabled={true}
            onPress={this.openPicker}
            textValue={this.selectedYear}
          />
        </View>
        <View style={{flex: 0.3}}/>
        <TouchableOpacity 
          onPress={this.onHolidaySearch}
          style={{flex:1, backgroundColor: constant.COLORS.THEME_BLUE, paddingVertical: 7}}>
          <Text style={{color: constant.COLORS.WHITE, fontSize: 16, textAlign: 'center'}}>
            Find
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderTopTabs = () => {
    if (this.titles.length === 1) {
      return(
        <View
          style={styles.topTabs}
        >
          <View
            style={[styles.tab,{backgroundColor:constant.COLORS.THEME_BLUE, borderRadius: 7}]}
          >
            <Text
              style={[styles.text, {color:constant.COLORS.WHITE}]}
            >
              {this.titles[0]}
            </Text>
          </View>
        </View>
      )
    }

    else return(
      <View
        style={styles.topTabs}
      >
        {this.titles.map((item, index)=> {
          let tabStyle = this.getTabStyle(index)
          return(
            <TouchableOpacity
              onPress={() => this.onTabSwitch(item, index)}
              style={[styles.tab, {backgroundColor: index === this.selectedTab ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE}, tabStyle]}
            >
              <Text
                style={[styles.text, {color: index === this.selectedTab ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE}]}
              >
                {item}
              </Text>
            </TouchableOpacity>
          )
        })}
      </View>
    )
    // return(
    //   <View
    //     style={styles.topTabs}
    //   >
    //     <TouchableOpacity
    //       onPress={() => this.onTabSwitch(0)}
    //       style={[styles.tab, styles.leftTab, {backgroundColor: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE}]}
    //     >
    //       <Text
    //         style={[styles.text, {color: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE}]}
    //       >
    //         Normal Shift
    //       </Text>
    //     </TouchableOpacity>
          
    //     <TouchableOpacity
    //       onPress={() => this.onTabSwitch(1)}
    //       style={[styles.tab, styles.rightTab, {backgroundColor: tabSwitch ? constant.COLORS.WHITE : constant.COLORS.THEME_BLUE}]}
    //     >
    //       <Text
    //         style={[styles.text, {color: tabSwitch ? constant.COLORS.THEME_BLUE : constant.COLORS.WHITE}]}
    //       >
    //         US Shift
    //       </Text>
    //     </TouchableOpacity>
    //   </View>
    // )
  }

  renderHolidayList = () => {
    // const data = this.selectedTab === 0 ? this.normal_Shift : this.us_Shift
    const data = this.holidayList
    if(data.length !== 0) {
      return(
        <View style={{flex: 1}}>
          <FlatList
            extraData={this}
            data={data}
            style={{marginTop: 10}}
            renderItem={this.renderCard}
          />
      </View>
      )
    }
    else return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>
          Nothing to Show here
        </Text>
      </View>
    )
   }

  renderCard = ({item, index}) => {
    return(
      <HolidayCard
        cardData={item}
        backgroundColor={index % 2 === 0 ? constant.COLORS.LIGHT_BLUE : constant.COLORS.WHITE }
      />
    )
  }

  render() {
    return(
      <View style={{backgroundColor: constant.COLORS.WHITE, flex: 1}}>
       {this.isLoading ? <CustomActivityIndicator/> : null}
        <CustomHeader
          title = "Konnect"
          headerRightImage = {constant.IMAGES.sidebarIcon} 
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
          onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
        />
        <View style={{paddingHorizontal: 12, flex: 1}}>
          {this.renderSearchbar()}
          {this.renderTopTabs()}
          {this.renderHolidayList()}
          {/* { this.selectedTab === 0 ? this.renderNormalShift() : this.renderUSShift()} */}
        </View>
        
      </View>
    )
  }   
}

const styles = StyleSheet.create({
  searchBar: {
    height:40,
    flexDirection:'row',
    marginTop: 20,
    alignItems: 'center'
  },
  topTabs:{
    flexDirection:'row',
    marginTop:20
  },
  tab:{
    flex:1,
    height:40,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:1,
    borderColor:constant.COLORS.THEME_BLUE
  },
  leftTab:{
    borderRightWidth:0,
    borderTopLeftRadius:7,
    borderBottomLeftRadius: 7
  },
  rightTab:{
    borderLeftWidth:0,
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7
  },
  text:{
    alignItems:'center',
    fontSize:14,
  }
})