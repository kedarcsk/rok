import React, { Component } from 'react';
import { View, StyleSheet, Text, Alert, ScrollView, Image, Dimensions, TouchableOpacity, StatusBar, SafeAreaView } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import * as sideMenu from '../Assests/constants/DrawerMenu'
import * as utils from '../Utils/utilFunctions'
import * as navigation from '../Utils/Navigation'
import * as Screen_IDs from '../Utils/Screen_IDs'
import constant from '../Assests/constants/constant'
import { observer, inject } from "mobx-react";
import { observable } from 'mobx';
import { Navigation } from "react-native-navigation"
import CustomHeader from '../Components/CustomHeader';
import { getFromaAsyncStorage } from '../Utils/utilFunctions';


const { height, width } = Dimensions.get('screen')

@inject("store")
@observer
export default class HomeScreen extends Component {

  @observable profile_pic: String = constant.IMAGES.PROFILE_PLACEHOLDER
  @observable empData: String = ''
  @observable token: String = ''

  state = {
    userModel: {},
    token: '',
  }

  constructor(props) {
    super(props);
    this.empData = props.empData ? props.empData : props.store.login.empData
    //this.profile_pic = this.empData.profileImg ? this
    console.log(this.empData)
    // this.props.navigator.setButtons(navigatorButtons(this.props.navigator));
  }

   async componentWillMount() {
    this.props.store.time.initializeTime()
    let token = await getFromaAsyncStorage('@token')
    let parsedToken = JSON.parse(token)
    this.token = parsedToken
  }

  openImagePicker = () => {
    ImagePicker.showImagePicker((response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.profile_pic = source
        this.uploadUserImage(response)
      }
    });
  }

  onPressHeaderRightImage = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }

  uploadUserImage = (userPic) =>{
    const { uri, type, fileName } =  userPic
    let formdata = new FormData();
    formdata.append('file', {uri:uri, name:fileName, type: type})
    formdata.append('token',this.token)  
    console.log('formdata',formdata);
    fetch(constant.BaseUrlStaging+constant.URLs.API_UPLOADIMAGE,{
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    body: formdata
    }).then(response => {
      console.log(response)
    }).catch(err => {
      console.log('error in the house',err)
      alert(err)
    })      
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar
          backgroundColor= {constant.COLORS.THEME_BLUE}
        />
        <CustomHeader 
          title = "Konnect"
          headerRightImage = {constant.IMAGES.sidebarIcon} 
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          // onPressHeaderLeftImage = {this.onPressHeaderLeftImage}
          onPressHeaderRightImage = {this.onPressHeaderRightImage}
        />
        <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false}>
          <View style={{ alignItems: 'center' }}>
            <View backgroundColor='white' alignItems='center' style={{ paddingTop: 20 }}>
              <View>
              <Image source={this.profile_pic}
                style={styles.logo}
              />
              <TouchableOpacity
                onPress={this.openImagePicker}
                style={styles.camera_icon}
              >
                <Image
                  style={{width:30, height:30}}
                  source={constant.IMAGES.CAMERA_ICON}
                />
              </TouchableOpacity>
              
              </View>

              <Text style={styles.empID}>
              {/* 6131 */}
              {this.empData.corpid ? this.empData.corpid : ''}
              </Text>
              <Text style={styles.textLabel}>
              {/* Meghna Malhotra */}
              {this.empData.empname ? this.empData.empname : ''}
              </Text>
              <Text style={styles.textLabel}>
              {/* meghna.malhotra@kelltontech.com */}
              {this.empData.email ? this.empData.email : ''}
              </Text>
              <Text style={styles.textLabel}>
              {/* Software Developer */}
              {this.empData.desg ? this.empData.desg : ''}
              </Text>
            </View>

            <TouchableOpacity
              style={styles.findColleagueButton}
              onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.FIND_COLLEAGUE)}}
            >
              <Image source={constant.IMAGES.SEARCH_ICON} style={styles.search} />
              <Text style={styles.findColleagueText}> Find a Colleague </Text>
            </TouchableOpacity>
            <View style={styles.timeExpenseContainer}>
              <TouchableOpacity
                 onPress={() => { 
                    this.props.store.sideMenu.changeSideBarItems(sideMenu.TIME_MENUITEMS)
                    navigation.push('LoggedIn_Stack', Screen_IDs.MY_TIME)}}
              >
                <Image source={constant.IMAGES.TIME_ICON} style={styles.timeExpenseImage} />
              </TouchableOpacity>

              <TouchableOpacity
                 onPress={() => { navigation.push(this.props.componentId, Screen_IDs.MY_EXPENSE_REPORT)}}
              >
                <Image source={constant.IMAGES.EXPENSES_ICON} style={styles.timeExpenseImage} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.ptoButton}
              onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.MY_PTO)}}
            >
              <Text style={styles.ptoText}> My PTO </Text>
            </TouchableOpacity>
            <TouchableOpacity
                          style={styles.helpButton}
                          onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.RAISE_TICKET)}}
                        >
                          <Text style={styles.ptoText}> HELP DESK </Text>
                        </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={styles.bottomBar}>
          <Text style={styles.bottomBarText}>Copyright </Text>
          <Image source={constant.IMAGES.COPYRIGHT_ICON} style={styles.copyRight} />
          <Text style={styles.bottomBarText}> 2018 </Text>
          <TouchableOpacity
            onPress={this.pushToWebView}
          >
            <Text style={styles.KelltonTech}>KelltonTech</Text>
          </TouchableOpacity>
          <Text style={styles.bottomBarText}>. All rights reserved. </Text>
        </View>
      </SafeAreaView>

    );
  }
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor:constant.COLORS.WHITE
  },
  logo: {
    height: 120,
    width: 120,
    borderRadius: 60
  },
  search: {
    height: 25,
    width: 25,
  },
  copyRight: {
    height: 13,
    width: 13,
  },
  empID: {
    marginVertical: 6,
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold'
  },
  textLabel: {
    marginLeft: 20,
    marginRight: 20,
    marginVertical: 2,
    color: 'black',
    fontSize: 16
  },
  findColleagueButton: {
    marginTop: 15,
    width: width - 40,
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#26ACEC',
    fontSize: 14,
    alignItems: 'center',
    justifyContent: 'center'
  },
  findColleagueText: {
    color: 'white',
    fontSize: 18,
    marginLeft: 18,
  },
  ptoButton: {
    marginVertical: 12,
    width: width - 40,
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#FC660B',
    fontSize: 14,
    alignItems: 'center',
    justifyContent: 'center'
  },
  helpButton: {
      marginVertical: 12,
      width: width - 40,
      height: 40,
      flexDirection: 'row',
      backgroundColor: 'blue',
      fontSize: 14,
      alignItems: 'center',
      justifyContent: 'center'
    },
  KelltonTech: {
    color: constant.COLORS.WHITE,
    fontWeight: 'bold',
    fontSize: width / 26
  },
  ptoText: {
    color: 'white',
    fontSize: 18,
  },
  timeExpenseContainer: {
    marginTop: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // width:50,
    // height:50,
    width: width - 40,
    height: (width - 40 - 10) / 2,
  },
  timeExpenseImage: {
    // height:50,
    // width:50,
    height: (width - 40 - 10) / 2,
    width: (width- 40 - 10) / 2
  },
  bottomBar: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: constant.COLORS.THEME_BLUE,
    //width:50,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0
  },
  bottomBarText: {
    color: constant.COLORS.WHITE,
    fontSize: width / 26
  },
  camera_icon:{
    position:'absolute',
    right:0,
    top:10,
    width:30,
    height:30
  }
});


