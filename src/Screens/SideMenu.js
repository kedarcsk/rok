import React, { Component } from 'react';
import {  ScrollView , StyleSheet , Text, View, Image, FlatList, } from 'react-native';
import Constants from '../Assests/constants/constant'
import { observer, inject } from 'mobx-react'
import { HOME_MENUITEMS, TIME_MENUITEMS } from '../Assests/constants/DrawerMenu';
import { Navigation } from 'react-native-navigation'
import * as ScreenIDs from '../Utils/Screen_IDs'
import * as navigation from '../Utils/Navigation'
import Row from '../Components/Row'

@inject('store')
@observer
export default class SideMenu extends Component {

  componentWillMount = () => {
    const store = this.props.store.sideMenu
    store.setSideMenu(this)
    store.changeSideBarItems(HOME_MENUITEMS);
  }
  toggleDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: false
        }
      }
    });
  }
  pushToScreen = screenName => {
    this.toggleDrawer();
      Navigation.push('LoggedIn_Stack',{
        component:{
          name: screenName,
          passProps: {
            text: 'Pushed screen'
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate:false,
            }
          }
        }
      })
  }
  itemClick = async(item) => {
    console.log(item.screenName);
    switch (item.screenName) {
        case ScreenIDs.HOME_SCREEN:
            // this.props.sideMenuStore.changeSideBarItems({ items: HOME_MENUITEMS });
            break;
        case ScreenIDs.TIME:
            // this.props.sideMenuStore.changeSideBarItems({ items: TIME_MENUITEMS });
            alert('time');
            break;
        // case ScreenIDs.EXPENSE:
        //     // this.props.sideMenuStore.changeSideBarItems({ items: EXPENSE_MENUITEMS });
        //     // { this.pushToScreen(appConstants.ExpenseScreen) }
        //     alert("MY EXPENSE")
        //     break;
        // case ScreenIDs.FIND_A_COLLEAGUE:
        //     alert("FIND_A_COLLEAGUE")
        //     break;
        case ScreenIDs.CHANGE_PASSWORD:
            { this.pushToScreen(ScreenIDs.CHANGE_PASSWORD) }
            break;
        case 'Logout':
            await navigation.LogOut()
            break;
        // case ScreenIDs.MYHOLIDAYS:
        //     alert('MY MYHOLIDAYS');
        //     break;
        // case ScreenIDs.MYPTO:
        //     { this.pushToScreen(appConstants.MyPTO) }
        //     break;
        // case ScreenIDs.MYTIME:
        //     alert('MY TIME');
        //     break;
        // case ScreenIDs.ADDTIME:
        //     alert('ADD TIME');
        //     break;
        // case ScreenIDs.MYEXPENSES:
        //     alert('MY EXPENSE');
        // break;
        // case ScreenIDs.ADDEXPENSE:
        //     { this.pushToScreen('Konnect.AddExpense') }
        //     alert('ADD EXPENSE');
        //     break;
        default:
            navigation.LogOut()
    }
};
  render() {
    const data = this.props.store.sideMenu.sideBarItems
    const store = this.props.store.sideMenu
    return(
      <ScrollView style={styles.contentContainer} showsVerticalScrollIndicator={false}>
        <View style={styles.mainContainer}>
          <View backgroundColor='transparent' alignItems='center' style={{ paddingTop: 20 }}>
            <Image source={Constants.IMAGES.kelltonLogo} style={styles.logo} />
            <Text style={styles.textLabel}>Bhaskar Joshi</Text>
            <Text style={styles.empID}>3682</Text>
            <Text style={styles.separator}></Text>
          </View>
          <View style={{ height: 17 }} />

          <FlatList style={styles.flatList}
            extraData={this.props.store.sideMenu.sideBarItems}
            data={this.props.store.sideMenu.sideBarItems}
            renderItem={({ item, index }) => {
              return (
                <Row
                  item={item}
                  index={index}
                  onItemClick={(item) => {
                    this.toggleDrawer()
                    item.onPress(navigation, store)
                    this.setState({})
                  }}
                />)
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: Constants.COLORS.DRAWER_COLOR
  },
  separator: {
    marginTop: 20,
    backgroundColor:Constants.COLORS.WHITE,
    width: '100%',
    height: 0.3
  },
  mainContainer: {
    flex: 1,
  },
  logo: {
    marginTop: 30,
    height: 100,
    width: 100,
    borderWidth: 3,
    borderColor: Constants.COLORS.WHITE,
    borderRadius: 50
  },
  empID: {
    color:Constants.COLORS.WHITE,
    fontSize: 17,
  },
  textLabel: {
    fontSize: 17,
    color:Constants.COLORS.WHITE,
    marginTop: 10
  },
});