import React, { Component } from 'react';
import {  View , StyleSheet , Image, Text, TouchableOpacity, KeyboardAvoidingView, ScrollView, SafeAreaView} from 'react-native';

import CustomTextInput from '../Components/CustomTextInput';
import CustomButton from '../Components/CustomButton';
import constant from '../Assests/constants/constant'
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import * as Navigation from '../Utils/Navigation'
import { showAlert, renderIf } from '../Utils/utilFunctions'
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
@inject('store')
@observer

export default class Login extends Component {
  @observable empId = ''
  @observable isLoading = false
  componentWillMount(){

  }

  onGeneratePress = async() => {
    const { store } = this.props
    if(this.validationCheck()) {
      this.isLoading = true
      response = await store.login.forgotPassword(this.empId)
      this.isLoading = false
      if(response) {
        showAlert(response.message, () => {Navigation.pop(this.props.componentId)})
      }
    }
  }

  validationCheck = () => {
    if(this.empId.trim() !== '') {
      return true
    }
    else {
      showAlert(constant.TEXTS.empId)
      return false
    }
  }

  validateUserId = (text) => {
    let reg = new RegExp('^[0-9]+$')
    if(!reg.test(text) && text != '' ) {
      showAlert(constant.TEXTS.numeric)
      this.empId = text.substring(0, (text.length - 1))
    }
    else{
      this.empId = text
    }
  }


  render() {
    return(
      <ScrollView scrollEnabled={false} contentContainerStyle = {{flex:1}}>
       {renderIf( this.isLoading, <CustomActivityIndicator />)}
        <SafeAreaView style = {styles.container}  behavior="padding" enabled={true}>
          <View style={{alignItems:'center'}}>    
            <Image  
              resizeMode = {'contain'} 
              style = {styles.image} 
              source = {constant.IMAGES.kelltonLogo} /></View>
          <Text 
            style = {[styles.text, { fontSize:20, marginTop:15 }]} >
            {constant.TEXTS.infinitePoss}
          </Text>
          <Text
            style = {[styles.text, { fontSize:30, marginTop:40}]} >
            {constant.TEXTS.RESET_PASS}
          </Text>
          <View style = {{marginTop:50, marginHorizontal:40}}>
            <CustomTextInput 
              maxLength={8}
              image = {constant.IMAGES.userPic}
              placeholder = "Employee Id"
              value={this.empId}
              onChangeText={(text) => this.validateUserId(text) }
            />
          </View>
          <View style={{marginHorizontal:40, marginTop:20, alignItems:'center'}}>    
            <CustomButton 
              text = "Generate Password"
              onPress = {async()=> await this.onGeneratePress()}
            />
          </View>
          <TouchableOpacity
            onPress={ () => {Navigation.pop( this.props.componentId ) }}>
            <Text
              style = {[styles.text, { fontSize:16, marginTop:15}]} >
            Back to Login</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    backgroundColor:'white',
  },
  image:{
    width:240,
    height:140
  },
  text:{
    color:'#0075B2',
    textAlign:'center',
  }
})