import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, Modal, SafeAreaView, ScrollView, Dimensions, FlatList} from 'react-native'
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
import Calendar from '../Components/Calendar'
import WeekTimeCard from '../Components/WeekTimeCard'
import constant from '../Assests/constants/constant'
import CustomHeader from '../Components/CustomHeader'
import {observer, inject} from 'mobx-react'
import * as utils from '../Utils/utilFunctions'
import * as navigation from '../Utils/Navigation'
import moment from 'moment'
import {observable} from 'mobx'

const {height, width} = Dimensions.get('window')
const projectList = ['Project1', 'Project2', 'Project3', 'Project4', 'Project5']
const timeType = ['type1', 'type2', 'type3', 'type4', 'type5']

@inject('store')
@observer
export default class AddTime extends Component {

  @observable selectedPicker:Array = projectList 
  @observable isLoading: Boolean = false
  @observable selectedValue:String = ''
  @observable selectedDate:String = '01/01/0001'
  @observable modalVisible:Boolean = false
  @observable projectList:Array = []
  @observable existingData:Array = []
  @observable updateCards:Boolean = false
  @observable cardData: Array = [{isNewCard: true}]
  @observable isCardDeleted: Boolean = false

  async componentWillMount() {
    const store = this.props.store.time
    if(this.props.selectedDate){
      this.selectedDate = this.props.selectedDate
    }
    else {
      let currentMonday = utils.getMonday(new Date())
      this.selectedDate = moment(currentMonday).format('DD-MM-YYYY')
    }
    store.setSelectedDate(this.selectedDate)
    this.isLoading = true
    await store.getProjectList(this.selectedDate)
    this.projectList = store.projectList
    store.updateCardStatus(true)
    await this.getExistingTimeData()
    store.updateCardStatus(false)
    this.isLoading = false
  }

  getExistingTimeData = async() => {
    const store = this.props.store.time
    this.cardData = [{isNewCard:true}]
    let existingData = await store.getExistingTime(this.selectedDate)
    if(existingData) {
      this.existingData = existingData
    }
  }

  onOkayPressed = async(submitStatus) => {
    const store = this.props.store.time
    this.isLoading = true
    if(await store.hitAddApi(submitStatus)) {
      store.updateCardStatus(true)
      await this.getExistingTimeData()
      store.updateCardStatus(false)
    }
    this.isLoading = false
  }

  onSubmitPress = async(submitStatus) => {
    utils.showAlert('Are you sure you want to submit the time', async()=>{await this.onConfirmation(submitStatus)}, true)
  }

  onSavePress = async(submitStatus) => {
    if(submitStatus) {
      this.onSubmitPress(submitStatus)
      return
    }
    await this.onConfirmation(submitStatus)
  }

  onConfirmation = async(submitStatus) => {
    const store = this.props.store.time
    if(!store.checkForHours()) {
      utils.showAlert('Total hours entered are less than 40 hours', async()=>{await this.onOkayPressed(submitStatus)}, true)
    }
    else{
      await this.onOkayPressed(submitStatus)
    }
  }

  onAddPress = () => {
    this.isLoading = true
    const store = this.props.store.time
    this.existingData = store.weekCardsData().concat({isNewCard:true})
    console.log('added')
    this.isLoading = false
  }

  onDeletePress = async(index) => {
    this.isLoading = true
    const store = this.props.store.time
    store.updateCardStatus(true)
    this.existingData = await store.deleteCard(index)
    this.isLoading = false
  }

  onDonePress = async() => {
    this.isLoading = true
    this.modalVisible = false
    this.props.store.time.updateCardStatus(true)
    await this.getExistingTimeData()
    this.props.store.time.updateCardStatus(false)
    this.isLoading = false
  }

  onDayPress = async(date) => {
    this.selectedDate = moment(date.dateString).format('DD-MM-YYYY');
  }

  openCalendar = () => {
    this.modalVisible = true
  }

  renderCalendar = () => {
    return(
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.modalVisible}
      >
        <View style={styles.calendarContainer}>
          <Calendar
            selectedDate={this.selectedDate}
            onDonePress={this.onDonePress}
            onDayPress={(date) => {this.onDayPress(date)}}
          />
        </View>
    </Modal>
    )
  }

  renderAddTimeHeader = ()=> {
    const totalTime = this.props.store.time.totalTime
    return(
      <View style={styles.timeHeader}>
        <TouchableOpacity
          onPress={this.openCalendar}
          style={[styles.monthyHeader, {width: ( (3/7) * width )}]}
        >
          <Text
            style={{color:constant.COLORS.THEME_BLUE, fontSize:16, textAlign:'center'}}
          >
            {this.selectedDate}
          </Text>
          <View style={{flex:1, alignItems:'flex-end'}}>
            <Image
              style={{height:25, width:25}}
              source={constant.IMAGES.CALENDAR}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.topHours}>
          <Text style={{color:constant.COLORS.THEME_BLUE, fontSize:20, textAlign:'center'}}>
          {totalTime.toFixed(1)}
          </Text>
        </View>
      </View>
    )
  }

  renderTimeCard = (item ,index) => {
    const store = this.props.store.time
    const isToUpdate = this.updateCards
    return(
      <WeekTimeCard
        onDeletePress={()=> this.onDeletePress(index)}
        item={item}
        index={index}
        updateCard={isToUpdate}
        projectList={store.projectList}
        store={this.props.store.time}
      />
    )
  }

  renderTime = () => {
    const data = this.existingData.length !== 0 ? this.existingData : this.cardData
    return(
      <ScrollView 
         ref='dataList'
         style={{flex:1}}>
        {data.map((item, index)=>{
          return this.renderTimeCard(item, index)
        })}
      </ScrollView>
    )
  }

  renderAddTimeFooter = ()=> {
    if(this.existingData.length !== 0 && !constant.EDITABLE.includes(this.existingData[0].statusId)) {
      return null
    }
    return(
      <View style={[styles.timeHeader]}>
        <TouchableOpacity 
          onPress={this.onAddPress}
          style={styles.bottomTab}
        >
          <Text style={styles.bottomText}>
            Add
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.bottomTab}>
          <Text 
            onPress={async() => this.onSavePress(false)}
            style={styles.bottomText}>
            Save
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.bottomTab}>
          <Text 
            onPress={async()=>this.onSavePress(true)}
            style={styles.bottomText}>
            Submit
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return(
      <SafeAreaView style={styles.container}>
      {utils.renderIf( this.isLoading, <CustomActivityIndicator />)}
        <CustomHeader
          title = "Konnect"
          headerRightImage = {constant.IMAGES.sidebarIcon} 
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
          onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
        />
        {this.renderAddTimeHeader()}
        {this.renderTime()}
        {this.renderAddTimeFooter()}
        {this.renderCalendar()}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor :constant.COLORS.WHITE
  },
  timeHeader:{
    flex:0.125,
    flexDirection:'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginHorizontal:15
  },
  monthyHeader:{
    justifyContent: 'flex-end',
    flexDirection:'row',
    height:35,
    borderBottomWidth:1,
    borderColor:constant.COLORS.THEME_BLUE
  },
  topHours:{
    marginLeft:'auto',
    marginRight:10,
    borderColor:constant.COLORS.THEME_BLUE,
    borderBottomWidth:1,
    height:35
  },
  bottomTab:{
    flex:1,
    backgroundColor:constant.COLORS.THEME_BLUE,
    paddingHorizontal:15,
    paddingVertical:10,
    marginHorizontal:5
  },
  bottomText:{
    fontSize:18,
    color:constant.COLORS.WHITE,
    textAlign:'center'
  },
  calendarContainer:{
    height: ( (2/5) * height ) + 60,
    marginTop:( (3/5) * height) - 60,
  }
})