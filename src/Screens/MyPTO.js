
import React, {Component} from 'react'
import {View, Text, FlatList, StyleSheet} from 'react-native'
import constant from '../Assests/constants/constant'
import CustomHeader from '../Components/CustomHeader'
import CustomActivityIndicator from '../Components/CustomActivityIndicator'
import * as navigation from '../Utils/Navigation'
import {observer, inject} from 'mobx-react'
import {observable} from 'mobx'

@inject('store')
@observer
export default class MyPTO extends Component {

  @observable pto_data:Object = {}
  @observable isLoading:Boolean = false

  async componentWillMount() {
    this.isLoading = true
    let response = await this.props.store.login.fetchPTO()
    if(response) {
      this.pto_data = response.data
    }
    this.isLoading = false
  }

  renderPTO_Card = ({item}) => {
    const color = constant.PTO_COLOR.get(item.title)
    const hrs = item.hrs === '' ? 0 : parseInt(item.hrs) 
    return(
      <View style={{flexDirection: 'row', marginTop: 15}}>
          <View style={[styles.row, styles.leftColumn, {backgroundColor: color}]}>
            <Text style={[styles.text, {textAlign: 'left'}]}>
              {item.title}
            </Text>
          </View>
          <View style={[styles.row, styles.rightColumn, {backgroundColor: color}]}>
            <Text style={styles.text}>
              {hrs.toFixed(1)}
            </Text>
          </View>
      </View>
    )
  }

  render() {
    return(
      <View style={styles.container}>
        {this.isLoading ? <CustomActivityIndicator/> : null}
        <CustomHeader
          title = "Konnect"
          headerRightImage = {constant.IMAGES.sidebarIcon} 
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
          onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
        />
        <Text style={styles.pto_text}>My PTO</Text>
        <FlatList
          style={{paddingHorizontal: 8}}
          extraData={this}
          data={this.pto_data}
          renderItem={this.renderPTO_Card}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: constant.COLORS.WHITE
  },
  text: {
    color: constant.COLORS.WHITE,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '300'
  },
  row: {
    paddingHorizontal:10,
    paddingVertical: 15
  },
  leftColumn: {
    flex: 0.8
  },
  rightColumn: {
    flex:0.2,
    marginLeft: 1
  },
  pto_text: {
    textAlign: 'center',
    fontSize: 25,
    marginTop: 20,
    color: constant.COLORS.THEME_BLUE
  }

})