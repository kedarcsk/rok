import React from 'react';
import {

  Text,
  View,
Alert,

} from 'react-native';
import CardView from 'react-native-cardview';
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage,
} from 'react-native-material-cards';
import { Button } from 'react-native-elements';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { renderIf, getFromaAsyncStorage } from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';
import DisplayModal from './DisplayModal';

 export default class YourTickets extends React.PureComponent {

 constructor(props) {
   super(props);
   this.state = {
     userId:'',
     display:false,
     date:'',
   }
 }




  triggerModal() {
      this.setState(prevState => {
        return {
          display: true
        }
      });
    }


displayJsxMessage() {
        if (true) {
            return <Text> Hello, JSX! </Text>;
        } else {
            return <Text> Goodbye, JSX! </Text>;
        }
}



  render() {

var moment = require('moment-business-days');


    return (
      <View>

                <Card style={{ marginTop: 20 }}>
                  <CardTitle title="Ticket 1" />
                  <CardContent>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginRight: 30 }}>id</Text>
                      <Text style={{ color: 'blue' }}>{this.props.item.id}</Text>
                    </View>
                  </CardContent>
                  <CardContent>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginRight: 30 }}>street</Text>
                      <Text style={{ color: 'blue' }}>{this.props.item.address.street}</Text>
                    </View>
                  </CardContent>

                  <CardContent>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginRight: 30 }}>user longitude location </Text>
                      <Text style={{ color: 'blue' }}>{this.props.item.phone}</Text>
                    </View>
                  </CardContent>
                  <CardContent>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginRight: 30 }}>company catch phrase</Text>
                      <Text style={{ color: 'blue' }}>{this.props.item.company.catchPhrase}</Text>
                    </View>
                  </CardContent>

                  <CardAction separator={true} inColumn={false}>
                    <Button
                 onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.TICKET_VIEW_DETAILS,{'id':this.props.item.id,'name':this.props.item.name,'username':this.props.item.username,'email':this.props.item.email})}}
                      raised
                      icon={{ name: 'lock-open', type: 'font-awesome5' }}
                      style={{ width: 80 }}
                      title="Open"
                    />


<View>
<Text>
{this.displayJsxMessage()}
</Text>r
</View>


                    <View>
                      <Text> </Text>
                      <Text> </Text>
                    </View>
                    <Button
                      raised
                      icon={{ name: 'redo', type: 'font-awesome5' }}
                      title="Re-Open"
                    />
                  </CardAction>
                  <View>
                  <DisplayModal
                                uid = {this.props.item.id}
                                uname = {this.props.item.phone}
                                 display = { this.state.display }
                                  />

                                </View>

                </Card>



                </View>
    );
  }
}




// <View>
//