import React,{Component} from 'react';
import {StyleSheet,SafeAreaView,ScrollView,Text,View,ActivityIndicator,FlatList} from 'react-native';
import YourTickets from './YourTickets'
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { renderIf, getFromaAsyncStorage } from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';

export default class MyTickets extends Component {

  constructor(){
    super();
    this.state = {
      items: [],
    }
  }



componentDidMount(){
  this._get('https://jsonplaceholder.typicode.com/users').then(
    data => {
      this.setState({items: data})
    }
  )
}

  render(){
     if (this.state.items.length==0) {
      return (
        <View style={{ flex: 1, alignItems:'center',justifyContent:'center', }}>
          <ActivityIndicator size = "large"/>
        </View>
      );
    }
    return (
        <View style={{ flex: 1 }}>
         <CustomHeader
                           title = "Konnect"
                           headerRightImage = {constant.IMAGES.sidebarIcon}
                           headerLeftImage = {constant.IMAGES.kelltonLogo}
                           onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
                           onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
                         />
        <ScrollView>


<FlatList
style = {{marginTop:20,backgroundColor:'white',}}
data = {this.state.items}
keyExtractor = {(item,index) => index.toString()}
renderItem = {({item}) => <YourTickets item = {item}/>}
/>



        </ScrollView>

        <SafeAreaView style={{}}>
          <Text
            style={{
              backgroundColor: '#3D6DCC',
              paddingBottom: 25,
              textAlign: 'center',
              color: '#FFF',
              fontSize: 15,
              fontWeight: 'bold',
            }}>
            Copyright 2019 KelltonTech. All rigths reserved.
          </Text>
        </SafeAreaView>
      </View>
    );
  }

  _get = async (endpoint) => {
  const res = await fetch(endpoint);
  const data = await res.json();
  return data;
}
}



