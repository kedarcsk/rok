import React from 'react';
import {
  Modal,
  View,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import CardView from 'react-native-cardview';
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage,
} from 'react-native-material-cards';
import { Button } from 'react-native-elements';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { renderIf, getFromaAsyncStorage } from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';

const { height, width } = Dimensions.get('window');

export default class TicketViewDetails extends React.Component {

constructor(props)
{
        super(props);



}

componentDidMount() {
    return fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            // In this block you can do something with new state.
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

        render(){
        return(
                <SafeAreaView>
                      <CustomHeader
                        title="View Ticket"
                        headerRightImage={constant.IMAGES.sidebarIcon}
                        headerLeftImage={constant.IMAGES.kelltonLogo}
                        onPressHeaderLeftImage={() => {
                          navigation.onHomePress();
                        }}
                      />
                      <ScrollView contentContainerStyle={styles.contentContainer}>
                        <KeyboardAvoidingView
                          style={styles.container}
                          behavior="padding"
                          enabled>
                          <Card style={{ marginTop: 20 }}>
                            <CardTitle title="Ticket 1" />
                            <CardContent>
                              <View style={{ flexDirection: 'row' }}>
                                <Text style={{ marginRight: 30 }}>id</Text>
                                <Text style={{ color: 'blue' }}>{this.props.id}</Text>
                              </View>
                            </CardContent>
                            <CardContent>
                              <View style={{ flexDirection: 'row' }}>
                                <Text style={{ marginRight: 30 }}>street</Text>
                                <Text style={{ color: 'blue' }}>{this.props.name}</Text>
                              </View>
                            </CardContent>

                            <CardContent>
                              <View style={{ flexDirection: 'row' }}>
                                <Text style={{ marginRight: 30 }}>
                                  user longitude location{' '}
                                </Text>
                                <Text style={{ color: 'blue' }}>{this.props.username}</Text>
                              </View>
                            </CardContent>
                            <CardContent>
                              <View style={{ flexDirection: 'row' }}>
                                <Text style={{ marginRight: 30 }}>company catch phrase</Text>
                                <Text style={{ color: 'blue' }}>{this.props.email}</Text>
                              </View>
                            </CardContent>
                          </Card>
 <SafeAreaView style={{ flex: 1, flexDirection: 'row' }}>

                          <Card style={{ marginTop: 20 }}>
                                                      <CardTitle title="Ticket 1" />
                                                      <CardContent>
                                                        <View style={{ flexDirection: 'row' }}>
                                                          <Text style={{ marginRight: 30 }}>id</Text>
                                                          <Text style={{ color: 'blue' }}>{this.props.id}</Text>
                                                        </View>
                                                      </CardContent>
                                                      <CardContent>
                                                        <View style={{ flexDirection: 'row' }}>
                                                          <Text style={{ marginRight: 30 }}>street</Text>
                                                          <Text style={{ color: 'blue' }}>{this.props.name}</Text>
                                                        </View>
                                                      </CardContent>

                                                      <CardContent>
                                                        <View style={{ flexDirection: 'row' }}>
                                                          <Text style={{ marginRight: 30 }}>
                                                            user longitude location{' '}
                                                          </Text>
                                                          <Text style={{ color: 'blue' }}>{this.props.username}</Text>
                                                        </View>
                                                      </CardContent>
                                                      <CardContent>
                                                        <View style={{ flexDirection: 'row' }}>
                                                          <Text style={{ marginRight: 30 }}>company catch phrase</Text>
                                                          <Text style={{ color: 'blue' }}>{this.props.email}</Text>
                                                        </View>
                                                      </CardContent>
                                                    </Card>

                                                    <Card style={{ marginTop: 20 }}>
                                                                                <CardTitle title="Ticket 1" />
                                                                                <CardContent>
                                                                                  <View style={{ flexDirection: 'row' }}>
                                                                                    <Text style={{ marginRight: 30 }}>id</Text>
                                                                                    <Text style={{ color: 'blue' }}>{this.props.id}</Text>
                                                                                  </View>
                                                                                </CardContent>
                                                                                <CardContent>
                                                                                  <View style={{ flexDirection: 'row' }}>
                                                                                    <Text style={{ marginRight: 30 }}>street</Text>
                                                                                    <Text style={{ color: 'blue' }}>{this.props.name}</Text>
                                                                                  </View>
                                                                                </CardContent>

                                                                                <CardContent>
                                                                                  <View style={{ flexDirection: 'row' }}>
                                                                                    <Text style={{ marginRight: 30 }}>
                                                                                      user longitude location{' '}
                                                                                    </Text>
                                                                                    <Text style={{ color: 'blue' }}>{this.props.username}</Text>
                                                                                  </View>
                                                                                </CardContent>
                                                                                <CardContent>
                                                                                  <View style={{ flexDirection: 'row' }}>
                                                                                    <Text style={{ marginRight: 30 }}>company catch phrase</Text>
                                                                                    <Text style={{ color: 'blue' }}>{this.props.email}</Text>
                                                                                  </View>
                                                                                </CardContent>
                                                                              </Card>



                          </SafeAreaView>




                          <View>
                            <TouchableOpacity
                              style={styles.TopViewButtonContainer}
                               onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.MY_TICKETS)}}
                              >
                              <Text style={{ fontSize: 20, color: '#fff' }}>Submit Ticket</Text>
                            </TouchableOpacity>
                          </View>
                        </KeyboardAvoidingView>
                      </ScrollView>
                       <View style={styles.bottomBar}>
                                        <Text style={styles.bottomBarText}>Copyright </Text>
                                        <Image source={constant.IMAGES.COPYRIGHT_ICON} style={styles.copyRight} />
                                        <Text style={styles.bottomBarText}> 2018 </Text>
                                        <TouchableOpacity
                                          onPress={this.pushToWebView}
                                        >
                                          <Text style={styles.KelltonTech}>KelltonTech</Text>
                                        </TouchableOpacity>
                                        <Text style={styles.bottomBarText}>. All rights reserved. </Text>
                                      </View>
                    </SafeAreaView>
        );
        }
}

const styles = StyleSheet.create({

  TopViewButtonContainer: {
      backgroundColor: constant.COLORS.THEME_BLUE,
      alignItems: 'center',
      justifyContent: 'center',
      height: 50,
      width: 150,
    },
    copyRight: {
          height: 13,
          width: 13,
        },
        KelltonTech: {
            color: constant.COLORS.WHITE,
            fontWeight: 'bold',
            fontSize: width / 26
          },
          bottomBar: {
              flexDirection: 'row',
              height: 50,
              backgroundColor: constant.COLORS.THEME_BLUE,
              //width:50,
              width: width,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 0,
              marginTop:103,
            },
            bottomBarText: {
              color: constant.COLORS.WHITE,
              fontSize: width / 26
            },
});

