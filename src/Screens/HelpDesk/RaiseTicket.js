import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
  Button,
  Text,
  Platform,
  ActivityIndicator,
  Alert,
 Dimensions,
 TouchableOpacity,
 Image,
} from 'react-native';
import { Picker } from 'react-native-picker-dropdown';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { Calendar } from 'react-native-calendars';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation'
import * as Screen_IDs from '../../Utils/Screen_IDs'
import { renderIf, getFromaAsyncStorage,openDocumentPicker,  showAlert } from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';
import CustomTextInput from '../../Components/CustomTextInput';

const { height, width } = Dimensions.get('window');

export default class RaiseTicket extends Component {



  constructor(props, context) {
    super(props, context);
    this.state = {
      language: '',
      text: '',
      summary: '',
      isLoading: true,
      raiseTo: '',
      type:'',
      attachment:'',
    };
    // this.onValueChange = this.handleValueChange.bind(this);
  }

  componentDidMount() {
    return fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            // In this block you can do something with new state.
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
 showData() {

showAlert('hiiiiii')



}

  GetPickerSelectedItemValue = () => {
    Alert.alert(this.state.raiseTo);

  };

   GetPickerSelectedItemValu = () => {

    Alert.alert(this.state.type);
  };



uploadAttachments = async () => {
    openDocumentPicker(async (file) => { this.state.attachment = file })
    Alert.alert("fdhght");
  }
  renderBottomView = () => {
    return (
      <View>


        <View style={{ justifyContent: 'center', marginTop: 10, flexDirection:'row', alignItems:'center' }}>
		 			<TouchableOpacity onPress = {()=>{this.uploadAttachments()}}>
              <Image source = {constant.IMAGES.ATTACHMENT_ICON} style={[styles.Icon,{marginRight:5}]}/>
           </TouchableOpacity>
          <Text style={styles.BottomViewText}>File Upload</Text>
        </View>


        <TouchableOpacity
                  style={styles.TopViewButtonContainer}
                onPress={() => { navigation.push('LoggedIn_Stack', Screen_IDs.MY_TICKETS)}}
                >
                  <Text style={{ fontSize: 20, color: '#fff' }}>Submit Ticket</Text>
                </TouchableOpacity>

      </View>
    );
  };
  onPickerValueChange=(value, index)=>{
      this.setState(
        {
          "raiseTo": value
        },
        () => {
          // here is our callback that will be fired after state change.
          Alert.alert("Throttlemode", this.state.raiseTo);
          if(this.state.raiseTo !=''){
           // Alert.alert("Thro", this.state.raiseTo);
           return fetch('https://jsonplaceholder.typicode.com/users')
                 .then(response => response.json())
                 .then(responseJson => {
                   this.setState(
                     {
                       isLoading: false,
                       data: responseJson,
                     },
                     function() {
                       // In this block you can do something with new state.
                     }
                   );
                 })
                 .catch(error => {
                   console.error(error);
                 });

                 <SafeAreaView>
                 <Picker
                         placeholder={'Description of the Ticket '}
                                                 underlineColorAndroid='transparent'
                                                 placeholderTextColor = "blue"
                               selectedValue={this.state.type}
                               //onPress = {() => this.showData()}
                               onValueChange={(itemValue, itemIndex) =>
                                 this.setState({ type: itemValue })
                               }
                               prompt="Choose your favorite language"
                               style={styles.picker}
                               textStyle={styles.pickerText}
                               cancel>
                               {this.state.data.map((item, key) => (
                                 <Picker.Item
                                   label={item.name}
                                   value={item.name}
                                   key={key}
                                 />
                               ))}
                             </Picker>
                 </SafeAreaView>

          }
          else{
          <SafeAreaView>
                           <Picker
                                   placeholder={'Description of the Ticket '}
                                                           underlineColorAndroid='transparent'
                                                           placeholderTextColor = "blue"
                                         selectedValue={this.state.type}
                                         //onPress = {() => this.showData()}
                                         onValueChange={(itemValue, itemIndex) =>
                                           this.setState({ type: itemValue })
                                         }
                                         prompt="Choose your favorite language"
                                         style={styles.picker}
                                         textStyle={styles.pickerText}
                                         cancel>
                                         {this.state.data.map((item, key) => (
                                           <Picker.Item
                                             label={item.email}
                                             value={item.email}
                                             key={key}
                                           />
                                         ))}
                                       </Picker>
                           </SafeAreaView>

          }
        }
      );
    }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }



    return (
 <SafeAreaView>
 <CustomHeader
          title = "Raise Ticket"
          headerRightImage = {constant.IMAGES.sidebarIcon}
          headerLeftImage = {constant.IMAGES.kelltonLogo}
          onPressHeaderLeftImage = {()=>{navigation.onHomePress()}}
          onPressHeaderRightImage = {()=>{navigation.onPressHeaderRightImage(this.props.componentId)}}
        />

                <ScrollView contentContainerStyle={styles.contentContainer}>
                  <KeyboardAvoidingView
                    behavior="padding"
                    enabled>
            <Picker

              selectedValue={this.state.raiseTo}
              onValueChange={this.onPickerValueChange}
               /* onValueChange={(itemValue, itemIndex) =>
                               this.setState({ raiseTo: itemValue })
                             }*/

               style={styles.picker}
              prompt="Raise the issue to"
              textStyle={styles.pickerText}
              cancel>
               {this.state.dataSource.map((item, key) => (
                <Picker.Item
                  label={item.username}
                  value={item.username}
                  key={key}
                />
                 ))}
            </Picker>


            <Picker
                                              placeholder={'Description of the Ticket '}
                                                                      underlineColorAndroid='transparent'
                                                                      placeholderTextColor = "blue"
                                                    selectedValue={this.state.type}
                                                    //onPress = {() => this.showData()}
                                                    onValueChange={(itemValue, itemIndex) =>
                                                      this.setState({ type: itemValue })
                                                    }
                                                    prompt="Choose your favorite language"
                                                    style={styles.picker}
                                                    textStyle={styles.pickerText}
                                                    cancel>
                                                    {this.state.dataSource.map((item, key) => (
                                                      <Picker.Item
                                                        label={item.email}
                                                        value={item.email}
                                                        key={key}
                                                      />
                                                    ))}
                                                  </Picker>


            <SafeAreaView style={{ flex: 1, flexDirection: 'row' }}>
              <Picker
               placeholder={'Description of the Ticket '}
                                              underlineColorAndroid='transparent'
                                              placeholderTextColor = "blue"
                selectedValue={this.state.language}
                onValueChange={this.onValueChange}
                placeholder="Choose your favorite language"
                style={styles.pickerPriority}
                textStyle={styles.pickerText}
                cancel>
                <Picker.Item label="Priority" value="0" />
                <Picker.Item color="cadetBlue" label="JavaScript" value="js" />
                <Picker.Item label="Ruby" value="ruby" />
                <Picker.Item label="Python" value="python" />
                <Picker.Item label="Elm" value="elm" />
              </Picker>

              <View><Text>   </Text><Text>   </Text></View>

              <Picker
               placeholder={'Description of the Ticket '}
                                              underlineColorAndroid='transparent'
                                              placeholderTextColor = "blue"
                selectedValue={this.state.language}
                onValueChange={this.onValueChange}
                prompt="Choose your favorite language"
                style={styles.pickerPriority}
                textStyle={styles.pickerText}
                cancel>
                <Picker.Item label="Location" value="0" />
                <Picker.Item label="JavaScript" value="js" />
                <Picker.Item label="Ruby" value="ruby" />
                <Picker.Item label="Python" value="python" />
                <Picker.Item label="Elm" value="elm" />
              </Picker>
            </SafeAreaView>

            <View
              style={{
                paddingTop: 15,
                backgroundColor: this.state.text,
                borderBottomColor: 'blue',
                borderBottomWidth: 1,
              }}>
              <TextInput
                placeholder={'Description of the Ticket '}
                                underlineColorAndroid='transparent'
                                placeholderTextColor = "blue"
                multiline={true}
                numberOfLines={4}
                onChangeText={text => this.setState({ text })}
                value={this.state.text}
              />
            </View>
            <Text style={{ }}>Descripton of the issue </Text>
             <KeyboardAvoidingView
                                behavior="padding"
                                enabled>
            <View
              style={{
                backgroundColor: this.state.summary,
                borderBottomColor: 'blue',
                borderBottomWidth: 1,
              }}>
              <TextInput
                placeholder={'Summary of the Ticket '}
                  underlineColorAndroid='transparent'
                                 placeholderTextColor = "blue"
                multiline={true}
                numberOfLines={4}
                onChangeText={summary => this.setState({ summary })}
                value={this.state.summary}
              />
            </View>
             </KeyboardAvoidingView>
            <Text style={{ padding: 15 }}>Summary of the issue </Text>








 {this.renderBottomView()}

          </KeyboardAvoidingView>
        </ScrollView>
        <View style={styles.bottomBar}>
                  <Text style={styles.bottomBarText}>Copyright </Text>
                  <Image source={constant.IMAGES.COPYRIGHT_ICON} style={styles.copyRight} />
                  <Text style={styles.bottomBarText}> 2018 </Text>
                  <TouchableOpacity
                    onPress={this.pushToWebView}
                  >
                    <Text style={styles.KelltonTech}>KelltonTech</Text>
                  </TouchableOpacity>
                  <Text style={styles.bottomBarText}>. All rights reserved. </Text>
                </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TopViewButtonContainer: {
      backgroundColor: constant.COLORS.THEME_BLUE,
      alignItems: 'center',
      justifyContent: 'center',
      height: 50,
      width: 150,
      marginTop:20,
      marginLeft:140,
      marginBottom:72,
    },
  picker: {
    alignSelf: 'stretch',
    marginTop: 20,
    borderBottomColor: 'blue',
    borderBottomWidth: 1,
    paddingLeft:20,
    paddingRight:20,
  },
  pickerPriority: {
    width: 150,
    alignSelf: 'stretch',
    marginTop: 20,
    borderBottomColor:'blue' ,
    borderBottomWidth: 1,
  },
  pickerText: {
    color: 'black',
  },
  contentContainer: {
    // paddingVertical: 20
  },
   copyRight: {
      height: 13,
      width: 13,
    },
    KelltonTech: {
        color: constant.COLORS.WHITE,
        fontWeight: 'bold',
        fontSize: width / 26
      },
      bottomBar: {
          flexDirection: 'row',
          height: 50,
          backgroundColor: constant.COLORS.THEME_BLUE,
          //width:50,
          width: width,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 0
        },
        bottomBarText: {
          color: constant.COLORS.WHITE,
          fontSize: width / 26
        },

});
