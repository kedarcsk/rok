import React from 'react';
import {
  Modal,
  View,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { Header } from 'react-native-elements';
import CardView from 'react-native-cardview';
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage,
} from 'react-native-material-cards';
import { Button } from 'react-native-elements';
import { Picker } from 'react-native-picker-dropdown';
import constant from '../../Assests/constants/constant';
import CustomHeader from '../../Components/CustomHeader';
import { Navigation } from 'react-native-navigation';
import { observer, inject } from 'mobx-react';
import { Calendar } from 'react-native-calendars';
import { observable } from 'mobx';
import * as navigation from '../../Utils/Navigation';
import * as Screen_IDs from '../../Utils/Screen_IDs';
import {
  renderIf,
  getFromaAsyncStorage,
  openDocumentPicker,
  showAlert,
} from '../../Utils/utilFunctions';
import CustomButton from '../../Components/CustomButton';
import CustomTextInput from '../../Components/CustomTextInput';

componentwillmount = async ()=>{
 return fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            // In this block you can do something with new state.
          }
        );
      })
      .catch(error => {
        console.error(error);
      });


}


const DisplayModal = props => (

  <Modal
    visible={props.display}
    animationType="slide"
    onRequestClose={() => console.log('closed')}>
    <SafeAreaView>
      <CustomHeader
        title="Raise Ticket"
        headerRightImage={constant.IMAGES.sidebarIcon}
        headerLeftImage={constant.IMAGES.kelltonLogo}
        onPressHeaderLeftImage={() => {
          navigation.onHomePress();
        }}
      />
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
          enabled>
          <Card style={{ marginTop: 20 }}>
            <CardTitle title="Ticket 1" />
            <CardContent>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 30 }}>id</Text>
                <Text style={{ color: 'blue' }}>{props.uid}</Text>
              </View>
            </CardContent>
            <CardContent>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 30 }}>street</Text>
                <Text style={{ color: 'blue' }}>{props.uname}</Text>
              </View>
            </CardContent>

            <CardContent>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 30 }}>
                  user longitude location{' '}
                </Text>
                <Text style={{ color: 'blue' }}>{props.usernameName}</Text>
              </View>
            </CardContent>
            <CardContent>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 30 }}>company catch phrase</Text>
                <Text style={{ color: 'blue' }}>{props.uemail}</Text>
              </View>
            </CardContent>

          </Card>

          <View>

          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </SafeAreaView>
  </Modal>
);

const styles = StyleSheet.create({

  TopViewButtonContainer: {
      backgroundColor: constant.COLORS.THEME_BLUE,
      alignItems: 'center',
      justifyContent: 'center',
      height: 50,
      width: 150,
    },
});


export default DisplayModal;
