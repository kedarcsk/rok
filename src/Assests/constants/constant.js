module.exports = {
  BaseUrl:'https://konnect.kelltontech.net/index.php?r=',
  BaseUrlStaging:'https://konnectuat.kelltontech.net/index.php?r=',
  DEVICE_ID : 'D6B27888-7EB8-4FF2-8CD9-0950E0762B79',

  IMAGES:{
    userPic: require('../images/user.png'),
    kelltonLogo: require('../images/kellton-logo.png'),
    passIcon: require('../images/pw-icon.png'),
    searchIcon:require('../images/menu.png'),
    expenseIcon:require('../images/expense.png'),
    ptoIcon:require('../images/pto.png'),
    calenderIcon:require('../images/calendar.png'),
    changePwdIcon:require('../images/changepw.png'),
    logoutIcon:require('../images/logout.png'),
    homeIcon:require('../images/home.png'),
    holidayIcon:require('../images/holiday.png'),
    sidebarIcon:require('../images/sidebar.png'),
    backIcon:require('../images/back.png'),
    TIME_ICON: require('../images/time_icon_800.jpg'),
    EXPENSES_ICON: require('../images/Expenses_icon_800.jpg'),
    PROFILE_PLACEHOLDER: require('../images/img_placeholder.png'),
    SEARCH_ICON: require('../images/search-75.png'),
    COPYRIGHT_ICON: require('../images/copyright-symbol-3.png'),
    CAMERA_ICON: require('../images/camera_icon.png'),
    DROPDOWN: require('../images/dropdown_icons_75.png'),
    CALENDAR: require('../images/calendar_icon_75.png'),
    TICK: require('../images/Tick_Icon_75.png'),
    CROSS: require('../images/Cross_Icon_75.png'),
    APPROVE: require('../images/approve_icon_75.png'),
    REJECT: require('../images/reject_icon_3x.png'),
    ADD: require('../images/new_icon_3x.png'),
    UPLOAD: require('../images/upload_icon_75.png'),
    DELETE: require('../images/delete-icon_75.png'),
    SAVE: require('../images/saved_icon_3x.png'),
    SUBMITTED: require('../images/submitted_icon_3x.png'),
    CLOSE: require('../images/close_icon_75.png'),
    ATTACHEMENT: require('../images/attachment_icon_75.png'),
    BLUE_BACK_ICON: require('../images/blueBack.png'),
    CALL_ICON: require('../images/call.png'),
    CHAT_ICON:require('../images/chat.png'),
    EMAIL_ICON:require('../images/email.png'),
    SKYPE_ICON:require('../images/skype.png'),
    NEXT_ICON:require('../images/next.png'),
    VOICE_ICON_INACTIVE:require('../images/voiceIconInactive.png'),
    VOICE_ICON_ACTIVE:require('../images/voiceIconActive.png'),
    EXPENSE_BG:require('../images/expenseBG.jpg'),
    ATTACHMENT_ICON:require('../images/attach.png'),
    CLAIM_EXPENSE : require('../images/claimExpense.png'),
    SUBMIT_EXPENSE: require('../images/submit.png'),
    EDIT_ICON: require('../images/edit.png'),
    COPY_ICON: require('../images/copy.png'),
    EYE_ICON: require('../images/eye.png'),
    SMILEY_ICON: require('../images/error_icon_150.png'),
    ERROR_EMOJI: require('../images/error_icon_150.png')
  },

  COLORS:{
    THEME_BLUE: '#0369A2',
    YELLOW: '#F8A925',
    WHITE:'#FFFFFF',
    DRAWER_COLOR:'#0C3758',
    BACK_GREEN: '#049804',
    GREEN: '#459e60',
    LIGHT_BLUE: '#E9FFFF',
    NON_BILLABLE: '#FF7C01',
    TOTAL_HOURS: '#03BC17'
  },


  PTO_COLOR: new Map([
    ['Accrued Hours', '#00C3EC'],
    ['PTO Taken', '#F29E13'],
    ['PTO Available', '#0073B6'],
    ['Approved', '#01A65A'],
    ['Future Requested PTO', '#D94E37'],
    ['Lapsed Hours', '#144683']
  ])
  ,

  TEXTS:{
    infinitePoss :'Infinite Possibilities with Technology',
    RESET_PASS: 'Reset Password',
    wlcm : 'Welcome to Konnect',
    empId: 'Please enter your Employee Id',
    passwd: 'Please enter your password',
    invalidCred: 'Invalid Credentials',
    otpValid: 'OTP has been sent to your email id. Please enter valid OTP.',
    numeric:'Only numeric allowed'
  },

  URLs:{
    login : 'employee/KhEmployeeMaster/MblValidateCredentials',
    otpValidate : 'employee/KhEmployeeMaster/MblValidateUser',
    MONTHLY_TIME : 'attendance/KhEmployeeTimesheets/MblGetTimeForDateRange',
    WEEKLY_TIME : 'attendance/KhEmployeeTimesheets/MblMyTime',
    OTP  : 'employee/KhEmployeeMaster/MblValidateUser',
    API_FORGOTPASSWORD  : 'employee/KhEmployeeMaster/MblForgotPassword',
    API_CHANGEPASSWORD  : 'employee/KhEmployeeMaster/MblChangePassword',
    API_FETCHPROJECTS  : 'pms/KhProjects/MblGetTimeProjectListForWeek',
    API_FETCHLOGGEDINUSERDETAILS  : 'employee/KhEmployeeMaster/MblGetLoggedInUserDetails',
    API_FETCHMYEXPENSEREPORTS  : 'expense/KhEmployeeReimbursements/MblGetMyExpenseReports',
    API_FETCHCLAIMEDEXPENSEHISTORY  : 'expense/KhEmployeeReimbursements/MblClaimedExpenseForReport',
    API_FETCHEXPENSEDETAILS  : 'expense/KhEmployeeReimbursements/MblGetExpenseReportDetails',
    API_FETCHLISTOFCOLLEAGUES  : 'employee/khEmployeeMaster/MblGetSearchOptionsList',
    API_GETBASICSEARCHDETAIL  : 'employee/khEmployeeMaster/MblGetSearchResults',
    API_FETCHSEARCHDETAILSFOREMPLOYEE  : 'employee/khEmployeeMaster/MblGetSearchResultsForEmployee',
    API_FETCHMYPTO  : 'leave/KhEmployeeLeavesMaster/MblGetLeaveOverview',
    API_COPYREPORT  : 'expense/KhEmployeeReimbursements/MblCopyExpenseReport',
    API_DELETEREPORT  : 'expense/KhEmployeeReimbursements/MblDeleteExpenseReport',
    API_COPYCLAIMEDEXPENSE  : 'expense/KhEmployeeReimbursements/MblCopyExpenseDetails',
    API_DELETECLAIMEDEXPENSE : 'expense/KhEmployeeReimbursements/MblDeleteExpenseDetails',
    API_FETCHTIME_TYPES : 'pms/KhProjects/MblGetProjectCodesForProject',
    API_FETCH_EXISTING_TIME : 'attendance/KhEmployeeTimesheets/MblGetWeekTime',
    API_SAVE_TIME : 'attendance/KhEmployeeTimesheets/MblAddWeekTime',
    API_UPLOAD_ATTACHEMENT : 'attendance/KhEmployeeTimesheets/MblUploadWeekAttachment',
    API_FETCH_ATTACHEMENTS : 'attendance/KhEmployeeTimesheets/MblGetWeekAttachments',
    API_DELETE_ATTACHEMENT : 'attendance/KhEmployeeTimesheets/MblDeleteWeekAttachment',
    API_DELETE_WEEK_ATTACHEMENT: 'attendance/KhEmployeeTimesheets/MblDeleteAllWeekAttachment',
    API_DELETE_WEEK_TIME: 'attendance/KhEmployeeTimesheets/MblDeleteAllWeekTime',
    API_UPLOADIMAGE : 'employee/KhEmployeePersonalDetails/MblUploadEmployeeProfileImage',
    API_ADDEXPENSEREPORT : 'expense/KhEmployeeReimbursements/MblAddExpenseReport',
    API_FETCHPROJECTLISTFOREXPENSE: 'pms/KhProjects/MblGetExpenseProjectListByDateRange',
    API_FETCHEXPENSECURRENCY: 'site/MblGetCurrencyList',
    API_EDITEXPENSEREPORT: 'expense/KhEmployeeReimbursements/MblEditExpenseReportDetails',
    API_FETCHCLAIMEDEXPENSETYPE: 'expense/KhEmployeeReimbursements/MblGetExpenseTypes',
    API_CLAIMEXPENSE :'expense/KhEmployeeReimbursements/MblAddClaimExpenseDetails',
    API_UPLOADEXPENSEATTACHMENT: 'expense/KhEmployeeReimbursements/MblUploadExpenseReciept',
    API_FETCH_HOLIDAYS: 'attendance/KhEmployeeHolidays/MblGetHolidayList',
    API_PTO: 'leave/KhEmployeeLeavesMaster/MblGetLeaveOverview'
  },

  MONTHS_ALPHA:new Map([
    [0,'January'],
    [1,'February'],
    [2,'March'],
    [3,'April'],
    [4,'May'],
    [5,'June'],
    [6,'July'],
    [7,'August'],
    [8,'September'],
    [9,'October'],
    [10,'November'],
    [11,'December'],
  ]),

  MONTHS_NUMERIC:new Map([
    ['January', {number:1, endDate:'31'}],
    ['February', {number:2, endDate:'28'}],
    ['March', {number:3, endDate:'31'}],
    ['April', {number:4, endDate:'30'}],
    ['May', {number:5, endDate:'31'}],
    ['June', {number:6, endDate:'30'}],
    ['July', {number:7, endDate:'31'}],
    ['August', {number:8, endDate:'31'}],
    ['September', {number:9, endDate:'30'}],
    ['October', {number:10, endDate:'31'}],
    ['November', {number:11, endDate:'30'}],
    ['December', {number:12, endDate:'31'}],
  ]),

  DAYS_ALPHA:new Map([
    [0, 'Sun'],
    [1, 'Mon'],
    [2, 'Tue'],
    [3, 'Wed'],
    [4, 'Thu'],
    [5, 'Fri'],
    [6, 'Sat']
  ]),

  TIME_STATUS:{
    NEW: '1',
    SAVE: '2',
    SUBMITTED: '3',
    APPROVED: '6',
    REJECTED: '7'
  },

  HOLIDAY_TYPE: new Map([
    ['1', {text:'M', color: '#049804'}],
    ['2', {text:'O', color: '#F8A925'}]
  ]),

  TIME_STATUS_ICON : new Map([
    ['1', require('../images/new_icon_3x.png')],
    ['2', require('../images/saved_icon_3x.png')],
    ['3', require('../images/submitted_icon_3x.png')],
    ['6', require('../images/approve_icon_75.png')],
    ['7', require('../images/reject_icon_3x.png')]

  ])
  ,

  EDITABLE:['1', '2', '7'],
  MANDATORY_DESC : ['2', '6', '8', '11', '4'],
  WEEK_DAYS: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  
  SOCIAL_PLATFORM: {
    FACEBOOK: 'facebook',
    TWITTER: 'twitter',
    LINKED_IN: 'linkedIN',
    SKYPE: 'skype'
  },
}