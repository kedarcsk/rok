import Constants from '../constants/constant'
import * as ScreenIDs from '../../Utils/Screen_IDs';

export const HOME_MENUITEMS = [
  {
    'screenName':ScreenIDs.HOME_SCREEN,
    'screenIcon':Constants.IMAGES.homeIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.MY_TIME,
    'screenIcon':Constants.IMAGES.calenderIcon,
    'onPress': (navigation, store) => {
      store.changeSideBarItems(TIME_MENUITEMS)
      navigation.push('LoggedIn_Stack', ScreenIDs.MY_TIME)
    }
  },
  {
    'screenName':ScreenIDs.HOME_SCREEN,
    'screenIcon':Constants.IMAGES.expenseIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.FIND_COLLEAGUE,
    'screenIcon':Constants.IMAGES.searchIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.CHANGE_PASSWORD,
    'screenIcon':Constants.IMAGES.changePwdIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':'Logout',
    'screenIcon':Constants.IMAGES.logoutIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  }
];

export const TIME_MENUITEMS = [
  {
    'screenName':ScreenIDs.HOME_SCREEN,
    'screenIcon':Constants.IMAGES.homeIcon,
    'onPress': (navigation, store) => {
      store.changeSideBarItems(HOME_MENUITEMS)
      navigation.push('LoggedIn_Stack', ScreenIDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.MY_TIME,
    'screenIcon':Constants.IMAGES.calenderIcon,
    'onPress': (navigation, store) => {
      // store.changeSideBarItems(HOME_MENUITEMS)
      // navigation.push('LoggedIn_Stack', Screen_IDs.MY_TIME)
    }
  },
  {
    'screenName':ScreenIDs.ADD_TIME,
    'screenIcon':Constants.IMAGES.calenderIcon,
    'onPress': (navigation, store) => {
      navigation.push('LoggedIn_Stack', ScreenIDs.ADD_TIME)
    }
  },
  {
    'screenName':ScreenIDs.MY_HOLIDAY,
    'screenIcon':Constants.IMAGES.holidayIcon,
    'onPress': (navigation, store) => {
      navigation.push('LoggedIn_Stack', ScreenIDs.MY_HOLIDAY)
    }
  },
  {
    'screenName':ScreenIDs.MY_PTO,
    'screenIcon':Constants.IMAGES.ptoIcon,
    'onPress': (navigation, store) => {
      navigation.push('LoggedIn_Stack', ScreenIDs.MY_PTO)
    }
  },
  {
    'screenName':ScreenIDs.FIND_COLLEAGUE,
    'screenIcon':Constants.IMAGES.searchIcon,
    'onPress': (navigation, store) => {
      // store.changeSideBarItems(HOME_MENUITEMS)
      // navigation.push('LoggedIn_Stack', Screen_IDs.MY_TIME)
    }
  },
  {
    'screenName':ScreenIDs.CHANGE_PASSWORD,
    'screenIcon':Constants.IMAGES.changePwdIcon,
    'onPress': (navigation, store) => {
      // store.changeSideBarItems(HOME_MENUITEMS)
      // navigation.push('LoggedIn_Stack', Screen_IDs.MY_TIME)
    }
  },
  {
    'screenName':'Logout',
    'screenIcon':Constants.IMAGES.logoutIcon,
    'onPress': (navigation, store) => {
      store.changeSideBarItems(HOME_MENUITEMS)
      navigation.LogOut()
    }
  }
];

export const EXPENSE_MENUITEMS = [
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.homeIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.expenseIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.expenseIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.searchIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.changePwdIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  },
  {
    'screenName':ScreenIDs.LOGIN,
    'screenIcon':Constants.IMAGES.logoutIcon,
    'onPress': (navigation, store) => {
      // navigation.push('LoggedIn_Stack', Screen_IDs.HOME_SCREEN)
    }
  }
];

export const HELPDESK = [

{
    'screenName':ScreenIDs.RAISE_TICKET,
    'onPress': (navigation, store) => {
       navigation.push('LoggedIn_Stack',  ScreenIDs.RAISE_TICKET)
    }
  },
  {
      'screenName':ScreenIDs.MY_TICKETS,
      'onPress': (navigation, store) => {
         navigation.push('LoggedIn_Stack',  ScreenIDs.MY_TICKETS)
      }
    },

]






