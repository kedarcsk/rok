import React, { Component } from 'react';
import {  TouchableOpacity , StyleSheet , Text } from 'react-native';
import Constants from '../Assests/constants/constant'
import { PropTypes } from 'prop-types';

export default class CustomButton extends Component {

  static propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string
  }

  static defaultProps = {
    onPress: () => {},
    text: ''
  }
  
  render() {
    const {text, onPress} = this.props
    return(
      <TouchableOpacity 
        onPress={() => onPress()}
        style = {styles.container}
      >
        <Text style = {styles.text}>{text}</Text>
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: Constants.COLORS.THEME_BLUE,
    alignItems:'center',
    justifyContent:'center',
    height:44,
    width:'100%'
  },
  text:{
    color:'white',
    fontSize:24
  }
});