
// @flow

import React, { Component } from 'react';
import { MaterialIndicator } from 'react-native-indicators';
import { View, StyleSheet } from 'react-native';
import constant from '../Assests/constants/constant'

export default class CustomActivityIndicator extends Component {
  render() {
    return <View style={style.indicator}>
      <View style={style.indicatorContainer}>
        <MaterialIndicator color={constant.COLORS.WHITE} size={40} />
      </View>
    </View>
  }
}

const style = StyleSheet.create({
  indicator: {
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    position: 'absolute',
    marginTop: 20,
    opacity: 0,
    zIndex: 100,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 1
  },
  indicatorContainer: {
    
    backgroundColor: 'black',
    opacity: 0.4,
    height: '100%',
    width:'100%',
  },
})