import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import constant from '../Assests/constants/constant'

export default class HolidayCard extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    const {cardData, backgroundColor} = this.props
    return(
      <View style={[styles.container, {backgroundColor: backgroundColor}]}>
        <View style={styles.dateDesc}>
          <View style={{borderRightWidth: 1,borderColor: constant.COLORS.THEME_BLUE, paddingHorizontal: 8}}>
            <View style={[styles.date]}>
              <Text style={styles.text}>
                {cardData.day}
              </Text>  
            </View>
            <View style={{borderTopWidth: 1, borderColor: constant.COLORS.THEME_BLUE}}/>
            <View style={styles.date}>
              <Text style={styles.text}>
                {cardData.dateText}
              </Text>  
            </View>
          </View>

        </View>
        <View style={{flex:0.75, flexDirection: 'row',  paddingHorizontal: 5,}}>
          <View style={styles.holidayText}>
            <Text numberOfLines={1} style={{textAlign: 'left', color: 'black'}}>
              {cardData.holiday}
            </Text>
          </View>
          <View style={[styles.holidayType, {backgroundColor: cardData.holidayColor}]}>
            <Text style={{textAlign: 'center', fontSize:22, color: constant.COLORS.WHITE}}>
              {cardData.holidayType}
            </Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 3,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.6,
    shadowRadius: 7,
    elevation: 5,
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
  dateDesc: {
    paddingVertical: 5,
    flex:0.25,
    justifyContent:'center'
  },
  text: {
    textAlign: 'center',
    color: 'black'
  },
  date: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  holidayText: {
    flex: 0.8,
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  holidayType:{
    flex: 0.2,
    borderRadius: 2,
    justifyContent:'center'
  }
})