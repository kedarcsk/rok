import React, { Component } from 'react';
import {  View , StyleSheet , Text , Image, TouchableOpacity} from 'react-native';
import Constants from '../Assests/constants/constant'
import { renderIf } from '../Utils/utilFunctions';

export default class CustomHeader extends Component {
  headerLeft = (headerLeftImage, onPressHeaderLeftImage) => {
    return(
      <View>
        {renderIf(headerLeftImage,
          <TouchableOpacity onPress= {onPressHeaderLeftImage}>
            <Image 
              source={headerLeftImage} 
              style={styles.headerImage}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }
  headerMiddle = (title) => {
    return(
      <View>
        <Text style = {styles.title}>{title}</Text>
      </View>
    )
  }
  headerRight = (headerRightImage, onPressHeaderRightImage) => {
    return(
      <View>
        {renderIf(headerRightImage,
          <TouchableOpacity onPress = {onPressHeaderRightImage}>
            <Image 
              source={headerRightImage} 
              style = {styles.headerImage}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }
  render() {
    const {
      title,
      headerRightImage,
      headerLeftImage,
      onPressHeaderLeftImage = () => {},
      onPressHeaderRightImage = () => {}
    } = this.props
    return(
      <View style = {styles.container}>
        {this.headerLeft(headerLeftImage, onPressHeaderLeftImage)}
        {this.headerMiddle(title)}
        {this.headerRight(headerRightImage, onPressHeaderRightImage)}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: Constants.COLORS.THEME_BLUE,
    height:44,
    justifyContent:'space-between',
    flexDirection:'row'
  },
  title:{
    color:Constants.COLORS.WHITE,
    fontSize:26,
    textAlign:'center'
  },
  headerImage:{
    height:30,
    width:30,
    marginHorizontal:10
  }
});