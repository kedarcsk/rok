import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, Modal, Dimensions, FlatList} from 'react-native'
import constant from '../Assests/constants/constant'
import {alpha_numeric_date, showFile} from '../Utils/utilFunctions'

const {height, width} = Dimensions.get('screen')

type Props = {
  visible:Boolean,
  store:Object,
  date:Array,
  index:Number,
  onClosePress:Function,
  ondeleteAttachement:Function
}

export default class AttachementsListing extends Component<Props> {
  constructor(props){
    super(props)
  }

  renderAttachementCard = ({item, index}) => {
    return(
      <View style={styles.card}>
        <View>
          <Text style={styles.text}>
            {item.projName}
          </Text>
        </View>
        <View>
          <Text style={styles.text}>
            {item.timeTypeTitle}
          </Text>
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity
            onPress={()=>{showFile(item.path)}} 
            style={styles.button}
          >
            <Text style={[styles.text, {color: constant.COLORS.THEME_BLUE}]}>
              View
            </Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.button}
            onPress={()=> this.props.ondeleteAttachement(item.docId, index)}
          >
            <Text style={[styles.text, {color: constant.COLORS.THEME_BLUE}]}>
              Delete
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const {visible, data, onClosePress, store} = this.props
    return(
      <Modal
        visible={visible}
        animationType={'fade'}
        transparent={true}
      >
        <View style={{flex:1, backgroundColor:'rgba(52, 52, 52, 0.8)'}}>
          <View style={styles.modalContainer}>
            <View style={styles.contentHeader}>
              <View style={styles.headerText}>
                <Text style={{fontSize:25, color: constant.COLORS.THEME_BLUE, textAlign: 'left'}}>
                  Time Attachments for {alpha_numeric_date(store.selectedDate)}
                </Text>
              </View>
              <TouchableOpacity
                onPress={onClosePress}
                style={{alignItems: 'center', justifyContent: 'center', flex: 0.1}}
              >
                <Image
                  source={constant.IMAGES.CLOSE}
                  style={{height:30, width:30}}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              style={{padding: 10, marginBottom: 5}}
              extraData={data}
              data={data}
              renderItem={this.renderAttachementCard}
            />
          </View>
         
        </View>

      </Modal>
    )
  }

}

const styles = StyleSheet.create({
  modalContainer: {
    marginTop: 100,
    backgroundColor: 'white',
    height: ((2/3)* height),
    marginHorizontal: 15,
    borderWidth: 1,
    borderColor: constant.COLORS.THEME_BLUE,
  },
  contentHeader: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 15 
  },
  card: {
    marginTop:10,
    marginBottom: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: constant.COLORS.THEME_BLUE
  },
  text: {
    fontSize:16,
    color:'black'
  },
  buttonView:{
    marginTop:10,
    flexDirection: 'row',
    paddingHorizontal: 60,
    paddingVertical: 5,
    justifyContent: 'space-between',
  },
  button: {
    borderWidth: 1,
    borderColor: constant.COLORS.THEME_BLUE,
    paddingHorizontal:25,
    paddingVertical:5
  },
  headerText: {
    flex: 0.9,
    flexWrap: 'wrap',
  }
})