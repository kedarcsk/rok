import React, { Component } from 'react';
import { TextInput, View, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import { renderIf } from '../Utils/utilFunctions'
import Constants from '../Assests/constants/constant'
import { PropTypes } from 'prop-types';

export default class CustomTextInput extends Component{
  
    static propTypes = {
      image: PropTypes.any,
      placeholder: PropTypes.string,
      imageRight: PropTypes.any
    }
  
    static defaultProps = {
      image: '',
      placeholder: '',
      imageRight:''
    }

    constructor(props) {
      super(props)
      this.props=props
    }
    renderImage = (image,onPress) => {
      return(
        <TouchableWithoutFeedback onPress = {onPress}>
          <Image  style ={ styles.image } source = { image }
            resizeMode = {'contain'}/>
        </TouchableWithoutFeedback>
      )
    }
    render() {
      const { image , placeholder, onChangeText, type, value, maxLength, imageRight, onPressImageLeft = () =>{} , onPressImageRight = () => {} } = this.props
      return(
        <TouchableWithoutFeedback onPress = {()=>{this.refs.textInput.focus()}}>
          <View style = {styles.container}>
            {renderIf( image ,this.renderImage(image, onPressImageLeft))
            }
            <TextInput
              style={styles.textInput}
              secureTextEntry={type}
              placeholder = { placeholder }
              onChangeText = {onChangeText}
              maxLength={maxLength}
              value={value}
              ref = 'textInput'
            />
            {renderIf( imageRight ,this.renderImage(imageRight, onPressImageRight))
            }
          </View>
        </TouchableWithoutFeedback>
      )
    }
}
const styles = StyleSheet.create({
  container: {
    flexDirection :'row',
    backgroundColor : Constants.COLORS.WHITE,
    borderBottomWidth : 1,
    borderBottomColor: Constants.COLORS.THEME_BLUE,
    alignItems:'center',
    marginBottom:20,
    paddingVertical:5,
    justifyContent:'space-between'
  },
  textInput:{
    padding:5,
    color:Constants.COLORS.THEME_BLUE,
    alignItems:'center',
    fontSize:18,
    flex:1
  },
  image:{
    width:35,
    height:35,
  }
});