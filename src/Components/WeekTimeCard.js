import React,{Component} from 'react'
import {View, Text, Image, TouchableOpacity, StyleSheet, ScrollView, FlatList} from 'react-native'
import AttachementsListing from '../Components/AttachementsListing'
import PickerComponent from '../Components/PickerComponent'
import TimeEntryCard from '../Components/TimeEntryCard'
import constant from '../Assests/constants/constant'
import * as utils from '../Utils/utilFunctions'
import picker from 'react-native-picker';
import {observer} from 'mobx-react'
import { observable } from 'mobx';

type Props = {
  index:Number,
  updateCard:Boolean,
  projectList:Array,
  isNewCard:Boolean,
  store:Object,
  existingData:Object,
}

@observer
export default class WeekTimeCard extends Component<Props> {

  @observable dayMap:Map = new Map()
  @observable selectedProject:String = ''
  @observable selectedType:String = ''
  @observable timeType:Array = []
  @observable timeDetails:Array = []
  @observable isEditable:Boolean = true
  @observable totalWeekTime:Number = 0
  @observable attachement:Object = undefined
  @observable attachementVisible: Boolean = false
  @observable attachementArray: Array = []

  constructor(props) {
    super(props)
    props.store.createWeekCard(this)
    // this.state = {}
  }

  onFileSelection = async() => {
    const {store} = this.props
    this.attachement && await store.uploadWeekAttachements(this.attachement, this.selectedProject, this.selectedType)
  }

  async componentWillMount() {
    await this.initComponent(this.props)
    console.log("Component mounted")
  }

  async shouldComponentUpdate(nextProps) {
    console.log("status<<<<"+nextProps !== this.props)
    if(nextProps.store.cardUpdate) {
      await this.initComponent(nextProps)
      nextProps.store.isCardDeleted && nextProps.store.updateCardStatus(false, nextProps.index)
      return true
    }
    return true
  }

  componentWillUnmount() {
    if(!this.props.store.isCardDeleted){
      this.props.store.removeCard()   
    }
  }

  initComponent = async(props) => {
    const { item, store} = props
    this.totalWeekTime = 0
    if(item.isNewCard) {
      this.timeDetails = await store.createNewWeekCard()
      this.isEditable = true
      this.timeType = []
      this.selectedProject = ''
      this.selectedType = ''
    }
    else {
      this.timeDetails = item.timeDetails
      this.isEditable = constant.EDITABLE.includes(item.statusId)
      this.selectedType = item.timeTypeTitle
      this.selectedProject = item.projName
      await this.getTypes(this.selectedProject)
      this.getWeekAttach(item, store)
      this.totalWeekTime = 0
      this.timeDetails.forEach((timeEntry)=>{
        let tempTime = timeEntry.hrs ? parseInt(timeEntry.hrs) : 0
        this.totalWeekTime = this.totalWeekTime + tempTime
      })
      // store.updateTotalTime()
    }
  }

  getWeekAttach = (item, store) => {
    if(item.attachement) {
      this.attachement = item.attachement
    }
    else if(item.attachementArray) {
      this.attachementArray = item.attachementArray
    }
    else{
      let attach = store.attachementMap.get(`${item.projId}${item.timeTypeId}`) 
      this.attachementArray = attach ? attach : []
      this.setState({})
    }
  }

  onDeletePress = async() => {
    const {store, onDeletePress, item} = this.props
    // if(this.attachementArray.length !== 0) {
    //   utils.showAlert('Attachements of this time will also be deleted', async()=> {
    //     await store.deleteWeekAttachements(item.projId, item.timeTypeId)
    //     onDeletePress()
    //   }, true)
    //   return
    // }
    await onDeletePress()
  }

  ondeleteAttachement = async(docId, index) => {
    const {store, item} = this.props
    if(index === 0){
      utils.showAlert('Are you sure you want to delete this last attachement', async()=>{
        await store.deleteAttachement(docId, index, `${item.projId}${item.timeTypeId}`) && this.attachementArray.splice(index, 1)
        this.attachementVisible = false
        this.setState({})
      }, true)
      return
    }
    await store.deleteAttachement(docId, index, `${item.projId}${item.timeTypeId}`) && this.attachementArray.splice(index, 1)
    this.setState({})
  }

  onHourChange = () => {
    const {store} = this.props
    this.totalWeekTime = 0
    constant.WEEK_DAYS.forEach((entry)=>{
      let entryTime = this[entry].hours === '' ? 0 : parseInt(this[entry].hours)
      this.totalWeekTime = this.totalWeekTime + entryTime
    })
    // Array.from(this.dayMap.values()).forEach((entry) => { 
    //   let entryTime = entry.hours === '' ? 0 : parseInt(entry.hours)
    //   this.totalWeekTime = this.totalWeekTime + entryTime
    // })
    store.updateTotalTime()
  }

  getTypes = async(projNm) => {
    const {store} = this.props
    let response = await store.getTimeTypes(projNm)
    if(response) {
      this.timeType = response
      console.log(this.timeType)
    }
  }

  getCurrentStateWeekData = () => {
    const {store, item} = this.props
    return {
      projId: store.projectMap.get(this.selectedProject),
      projName: this.selectedProject,
      timeTypeId: store.typeMap.get(this.selectedType),
      timeTypeTitle: this.selectedType,
      statusId: item.statusId ? item.statusId : '1',
      timeDetails: this._processEntries(),
      attachementArray: this.attachementArray,
      attachement : this.attachement
    }
  }

  getWeekData = () => {
    const {store} = this.props
    return {
      projId: store.projectMap.get(this.selectedProject),
      projName: this.selectedProject,
      timeTypeId: store.typeMap.get(this.selectedType),
      timeTypeTitle: this.selectedType,
      status: '1',
      timeDetails: this.processEntries()
    }
  }

  _processEntries = () => {
    return constant.WEEK_DAYS.map((entry) => { 
      return this[entry].getExistingEntry()
    })
  }

  processEntries = () => {
    return constant.WEEK_DAYS.filter((entry) => { 
      return this[entry].hours !== ''
    }).map((entry) => { 
      return this[entry].getEntry()
    })
  }

  setRef = (ref) => {
    this[ref.date.day] = ref 
    this.dayMap.set(ref.date.day, ref)
  }

  openPicker = async(isProject) => {
    if(!isProject && this.timeType.length === 0) {
      alert("select a project first")
      return
    }
    const selectedValue = isProject ? this.selectedProject : this.selectedType
    picker.init({
      pickerToolBarBg:[3, 105, 162, 1],
      pickerTitleText:'',
      pickerBg:[255, 255, 255, 1],
      pickerConfirmBtnColor:[255, 255, 255, 1],
      pickerCancelBtnColor:[255, 255, 255, 1],
      pickerFontSize:20,
      pickerFontFamily: 'aller',
      pickerToolBarFontSize:18,
      pickerData: isProject ? this.props.projectList : this.timeType,
      selectedValue: [selectedValue.toString()],
      onPickerConfirm: async(data) => {
        if(isProject) {
          console.log(this.props.store)
          await this.getTypes(this.selectedProject)
          //console.log("<<<>>>"+this.projectMap.get(data))  
        }
        else {

        }
      },
      onPickerCancel: data => {
        picker.hide()
        // console.log(data);
      },
      onPickerSelect: data => {
        if(isProject) {
          this.selectedProject = data[0]
          console.log("<<<<"+data)  
        }
        else {
          this.selectedType = data[0]
        }
      }
    })
    picker.show()
  }

  validateEntries = () => {
    const {item} = this.props
    let result = true
    Array.from(this.dayMap.values()).filter((entry) => { 
      return entry.hours !== ''
    }).forEach((entry)=>{
      if(constant.MANDATORY_DESC.includes(item.timeTypeId) && entry.description === '') {
        result = false
        alert("Description is mandatory for entry with date"+ entry.date.dateString)
        return
      }
    })
    return result
  }

  renderCard = ({item}) => {
    return(
      <TimeEntryCard
        store={this.props.store}
        isEditable={this.isEditable} 
        entry={item} 
        onHourChange={this.onHourChange}
        onRef = {(ref) => {this.setRef(ref)}}/>
    )
  }

  renderListFooter = () => {
    const {item} = this.props
    const source = item.statusId ? constant.TIME_STATUS_ICON.get(item.statusId) : constant.TIME_STATUS_ICON.get('1')
    return(
      <View 
        style={styles.listFooter}>
        <TouchableOpacity
          disabled={!this.isEditable}
          onPress = {this.processEntries}
        >
          <Image
            style={{height:30, width:30}}
            source={source}
          />
        </TouchableOpacity>
        {this.renderAttachement()}
        {this.renderUploadIcon()}
        {this.renderDeleteIcon()}
        <View style={{marginHorizontal:10}}>
          <Text style={{textAlign:'center', fontSize:16 }}>{this.totalWeekTime.toFixed(1)}</Text>
        </View>
      </View>
    )
  }

  renderUploadIcon = () => {
    if(this.isEditable) {
      return(
        <TouchableOpacity 
          disabled={!this.isEditable}
          onPress={()=>{utils.chooseFile((file)=>this.attachement = file)}}
          style={{marginLeft:10}}>
          <Image
            style={{height:30, width:30}}
            source={constant.IMAGES.UPLOAD}
          />
      </TouchableOpacity>
      )
    }
    else return null
  }

  renderDeleteIcon = () => {
    const {index, onDeletePress, item} = this.props
    if(item.isNewCard && index === 0 || index === 0 && item.statusId && item.statusId === '1' ) {
      return null
    }
    else if(this.isEditable) {
      return(
        <TouchableOpacity 
          onPress={this.onDeletePress}
          style={{marginLeft:10}}>
        <Image
          style={{height:30, width:30}}
          source={constant.IMAGES.DELETE}
        />
      </TouchableOpacity>
      )
    }
  }

  renderAttachement = () => {
    if(this.attachementArray.length !== 0 && this.isEditable) {
      return(
        <TouchableOpacity 
          onPress={()=>{this.attachementVisible = true}}
          style={{marginLeft:10}}>
        <Image
          style={{height:30, width:30}}
          source={constant.IMAGES.ATTACHEMENT}
        />
      </TouchableOpacity>
      )
    }
    return null
  }

  render() {
    return(
      <View style={styles.mainContainer}>
        <View style={styles.cardHeader}>
          <View style={{flex:0.6, justifyContent:'center'}}>
            <PickerComponent
              disabled={this.isEditable}
              placeholder={'Project Name'}
              onPress={async()=>{ await this.openPicker(true)}}
              textValue={this.selectedProject}
            />  
          </View>
          <View style={{flex:0.4, marginLeft:20, justifyContent:'center'}}>
            <PickerComponent
              disabled={this.isEditable}
              placeholder={'Time type'}
              onPress={async()=>{ await this.openPicker(false)}}
              textValue={this.selectedType}
            />  
          </View>
        </View>
        <View style={styles.cardEntries}>
          <FlatList
            extraData={this.totalWeekTime}
            data={this.timeDetails}
            renderItem={this.renderCard}
            ListFooterComponent={()=>{return this.renderListFooter()}}
          />
        </View>
        <AttachementsListing
          visible={this.attachementVisible}
          data={this.attachementArray}
          store={this.props.store}
          onClosePress= {()=> this.attachementVisible = false}
          ondeleteAttachement={this.ondeleteAttachement}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer:{
    flex:0.8,
    marginTop:20,
    marginHorizontal:15,
    borderColor:constant.COLORS.THEME_BLUE,
    borderWidth:1
  },
  cardHeader:{
    flex:0.125,
    flexDirection: 'row',
    justifyContent:'center',
    paddingVertical:13,
    borderColor:constant.COLORS.THEME_BLUE,
    //borderBottomWidth: 1,
    paddingHorizontal:8,
    // borderWidth:1
  },
  cardEntries:{
    flex:1
    //flex:0.75
  },
  listFooter:{
    justifyContent:'flex-end',
    alignItems: 'center',
    flexDirection:'row',
    borderTopWidth:1,
    borderColor:constant.COLORS.THEME_BLUE,
    padding:15
  }
})