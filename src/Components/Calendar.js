import React,{Component} from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import { Calendar } from 'react-native-calendars'
import constant from '../Assests/constants/constant'
import * as utils from '../Utils/utilFunctions'
import {styles} from '../Screens/MyTime'
import moment from 'moment'

type Props={
  selectedDate:String,
  onDonePress:Function,
  onDayPress:Function
}

export default class CalendarComponent extends Component<Props> {
  constructor(props) {
    super(props)
  }

  componentWillReceiveProps (nextProps) {
    console.log(nextProps)
  }

  renderHeader = () => {
    return(
      <View
        style={styles.pickerHeader}
      >
        <TouchableOpacity
          onPress={this.props.onDonePress}
          style={styles.headerItem}
        >
          <Text
            style={styles.headerText}
          >
            Done
          </Text>
        </TouchableOpacity>
        <View
          style={styles.headerItem}
        />
      </View>
    )
  }

  renderDayComponent = (date, state) => {
    const {selectedDate} = this.props
    let style = {height:20, width:20}
    let textStyle = {}
    let status = utils.checkForMonday(date.dateString)
    let currentDate = moment(date.dateString).format('DD-MM-YYYY')
    let currentMonday = moment(utils.getMonday(new Date())).format('DD-MM-YYYY')
    if(currentDate == selectedDate) {
      style = [styles.selectedDate, {backgroundColor: constant.COLORS.GREEN}]
    }
    else if(currentMonday == currentDate) {
      style = styles.selectedDate
    }
    if(currentMonday == currentDate || currentDate == selectedDate) {
      textStyle = {textAlign:'center', color:constant.COLORS.WHITE}
    }
    else {
      textStyle = {textAlign: 'center', color:  !status || state === 'disabled' ? 'gray' : 'black'}
    }
    return (
      <View 
        style={style}
      >
        <TouchableOpacity
          onPress={() => this.props.onDayPress(date)}
          disabled={!status || state === 'disabled'}
        >
          <Text style={textStyle}>
            {date.day}
          </Text>
        </TouchableOpacity>
      </View>);
  }

  render() {
    return(
      <View
        style={{flex:1}}
      >
        {this.renderHeader()}
        <View
          style={styles.picker}
        >
          <Calendar
            markedDates={
              {'2018-02-25': {textColor: 'green'},
                '2018-02-18': {startingDay: true, color: 'green'},
                '2018-02-11': {selected: true, endingDay: true, color: 'green', textColor: 'gray'},
                '2018-02-04': {disabled: true, startingDay: true, color: 'green', endingDay: true}
              }}
            dayComponent={({date, state}) => { return this.renderDayComponent(date, state)}}
          />
        </View>
      </View>
    )
  }
}
