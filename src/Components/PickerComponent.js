import React, {Component} from 'react'
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native'
import constant from '../Assests/constants/constant'

type Props = {
    placeholder?:String,
    onPress:Function,
    textValue:String
}

export default class PickerComponent extends Component<Props> {
  render() {
    const {placeholder, onPress, textValue, disabled} = this.props
    const isPlaceholder = placeholder && textValue === ''
    return(
      <TouchableOpacity
        disabled={!disabled}
        onPress={onPress}
        style={styles.container}
      >
        <Text
          numberOfLines={1}
          style={[styles.text, {color : isPlaceholder ? 'grey' : constant.COLORS.THEME_BLUE}]}
        >
          { isPlaceholder ? placeholder : textValue}
        </Text>
        <View style={{flex:1, alignItems:'flex-end'}}>
          <Image
            style={styles.icon}
            source={constant.IMAGES.DROPDOWN}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flexDirection:'row',
    backgroundColor:'white',
    height:35,
    borderBottomWidth:1,
    borderColor:constant.COLORS.THEME_BLUE
  },
  text:{
    fontSize:16,
    fontWeight:'500'
  },
  icon:{
    height:20,
    width:20
  }
})