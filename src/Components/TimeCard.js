import React,{Component} from 'react'
import constant from '../Assests/constants/constant'
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native'
import moment from 'moment';

type Props = {
  item?:Object
}

export default class TimeCard extends Component<Props> {

  getDay = () => {
    let date = new Date(this.props.item.timeDt)
    return constant.DAYS_ALPHA.get(date.getDay())
  }

  render() {
    const {item} = this.props
    const source = constant.TIME_STATUS_ICON.get(item.statusId)
    return(
      <View style={styles.timeContainer}>
        <View style={styles.timeDetail}>
          <Text style={styles.timeText}>
            {item.hours}
          </Text>
          <Text style={styles.timeText}>
            {this.getDay()}
          </Text>
          <Text style={styles.timeText}>
            {moment(item.timeDt).format('DD/MM')}
          </Text>
        </View>
        <View style={styles.timeEntryContainer}>
          <View style={styles.timeEntryHeader}> 
            <View style={{flex:0.6, alignItems:'flex-start'}}>
              <Text numberOfLines={1} style={styles.timeEntryText}>
                {item.projNm}
              </Text>
            </View>
            <View style={{flex:0.4, alignItems:'flex-end'}}>
              <Text numberOfLines={1} style={styles.timeEntryText}>
                {item.timeCode}
              </Text>
            </View>
          </View>
          <View style={styles.timeEntryHeader}>
            <View style={{flex:0.8, justifyContent:'center'}}>
              <Text numberOfLines={1} style={styles.timeEntryText}>
                {item.summary}
              </Text> 
            </View>
            <View style={{flex:0.2, alignItems:'flex-end'}}> 
              <Image
                style={{height:25, width:25}}
                source={source}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  timeContainer:{
    flexDirection:'row',
    borderColor:constant.COLORS.THEME_BLUE,
    marginHorizontal:10,
    borderWidth:1,
    marginTop:10
  },
  timeDetail:{
    flex:0.2,
    backgroundColor:constant.COLORS.THEME_BLUE,
  },
  timeText:{
    fontSize:16,
    fontWeight:'500',
    color:constant.COLORS.WHITE,
    textAlign:'center'
  },
  timeEntryContainer:{
    flex:0.8,
    paddingVertical:5,
    paddingHorizontal:8
  },
  timeEntryHeader:{
    flex:1,
    flexDirection:'row'
  },
  timeEntryText:{
    fontSize:16,
    fontWeight:'300'
  }
})