import React, { Component } from 'react';
import { TextInput , View , Image , StyleSheet} from 'react-native';
import { renderIf } from '../Utils/utilFunctions'
export default class Textinput extends Component {
  constructor(props) {
    super(props)
    this.props=props
  }
  render() {
    const { image , placeholder, design } = this.props
    const style = design !== undefined ? [design,styles.container] : styles.container
    return(
      <View style = {style}>
        {renderIf( image ,
          <Image  style ={ styles.image } source = { image }
            resizeMode = {'contain'}/>
        )
        }
        <TextInput
          style={styles.textInput}
          placeholder = { placeholder }
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    
    flexDirection :'row',
    backgroundColor : 'white',
    borderBottomWidth : 2,
    borderBottomColor:'#0075B2',
    marginHorizontal:40,
    alignItems:'center'
    
  },
  image:{
    width:35,
    height:35,
  },
  textInput:{
    fontSize:18,
    paddingLeft:20
  }
    
});