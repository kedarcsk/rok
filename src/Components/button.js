import React, { Component } from 'react';
import {  TouchableOpacity , StyleSheet , Text} from 'react-native';
import constant from '../Assests/constants/constant'
export default class Button extends Component {
  
  render() {
    return(
      <TouchableOpacity style = {styles.container}>
        <Text style = { styles.text }>{ this.props.text }</Text>
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor:'#0075B2',
    paddingVertical:'3%',
    paddingHorizontal:'35%'
  },
  text:{
    color:'white',
    fontSize:20
  }
    
});