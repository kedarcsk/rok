import React,{Component} from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
import constant from '../Assests/constants/constant'
import {observable} from 'mobx'
import {observer} from 'mobx-react'
import {showAlert} from '../Utils/utilFunctions'

type Props = {
  store:Object,
  isEditable:Boolean,
  entry:Object,
  onRef:Function,
  onHourChange:Function
}

@observer
export default class TimeEntryCard extends Component<Props> {

  @observable description:String = ''
  @observable hours:Number = 0
  @observable date:Object = {day:'Mon', dateString:'07/08'}
  
  constructor(props) {
    super(props)
    this.initCard(props)
    props.onRef(this)
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.store.cardUpdate) {
      this.initCard(nextProps)
    }
  }

  // componentWillMount() {
  //   this.props.onRef(this)
  //   this.initCard(this.props)
  // }

  initCard = (props) => {
    const {entry, onHourChange} = props
    this.date = {day: entry.day, dateString: entry.date, api_date: entry.dateString}
    this.hours = entry.hrs ? entry.hrs : ''
    this.description = entry.descp ? entry.descp : ''
    //onHourChange()
  }

  getEntry = () => {
    return{
      hrs:this.hours,
      descp:this.description,
      dt:this.date.api_date
    }
  }

  getExistingEntry = () => {
    const {entry} = this.props
    return{...entry, hrs:this.hours, descp: this.description}
  }

  validateHour = (text) => {
    let reg = new RegExp('^[0-9]+$')
    if(!reg.test(text) && text != '' ) {
      showAlert(constant.TEXTS.numeric)
      this.hours = text.substring(0, (text.length - 1))
    }
    else if(parseInt(text) > 24) {
      alert("hours can't exceed over 24")
      this.hours = text.substring(0, (text.length - 1))
    }
    else {
      this.hours = text
    }
  }

  onHoursChange = (text) => {
    const {onHourChange} = this.props
    this.validateHour(text)
    //this.hours = text
    onHourChange()
  }

  render() {
    const {isEditable} = this.props
    return(
      <View style={styles.container}>
        <View style={styles.date_hour}>
          <Text style={[styles.text, {color:constant.COLORS.THEME_BLUE}]}>
            {this.date.day}
          </Text>
          <Text style={[styles.text, {color:constant.COLORS.THEME_BLUE}]}>
            {this.date.dateString}
          </Text>
        </View>
        <View style={styles.desc}>
          <TextInput 
            editable={isEditable}
            style={styles.text}
            value={this.description}
            placeholder={'Description'}
            onChangeText={(text)=>{this.description = text}}
          />
        </View>
        <View style={styles.date_hour}>
          <TextInput
            keyboardType={'numberic'}
            editable={isEditable}
            style={[styles.text, {textAlign: "center"}]}
            placeholder={'Hrs'}
            value={this.hours}
            onChangeText={(text)=>{this.onHoursChange(text)}}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection: 'row',
    borderColor:constant.COLORS.THEME_BLUE,
    borderTopWidth:1,
    padding:7
  },
  text:{
    fontSize:14,
    color: constant.COLORS.THEME_BLUE
  },
  desc:{
    flex:0.7,
    justifyContent:'center',
    paddingHorizontal:8
  },
  date_hour:{
    flex:0.15,
    justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal:3,
    paddingVertical:5,
    borderColor:constant.COLORS.THEME_BLUE,
    borderWidth:1,
    borderRadius:10
  }
})