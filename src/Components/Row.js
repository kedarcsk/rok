import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Platform } from 'react-native';

export default class Row extends Component {
  render() {
    const { platform } = this.props;
    if (platform && platform !== Platform.OS) {
      return <View />;
    }
    return (
      
      <TouchableOpacity onPress={() =>
        this.props.onItemClick(this.props.item)
      }>      
        <View style={styles.rowButton}>
          <Image source={this.props.item.screenIcon} style={styles.icon} />
          <Text style={styles.rowButtonText}> {this.props.item.screenName} </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({

  rowButton: {
    marginVertical: 30,
    height: 25,
    flexDirection: 'row',
    backgroundColor: '#26ACEC',
    fontSize: 14,
    marginLeft: 18,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  rowButtonText: {
    color: '#fff',
    fontSize: 24,
    marginLeft: 15
  },
  icon: {
    height: 25,
    width: 25,
  }
});
