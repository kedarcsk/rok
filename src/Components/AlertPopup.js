import React, { Component } from 'react';
import { TextInput , View , Text, TouchableOpacity, Image , StyleSheet , Dimensions, Modal} from 'react-native';
import { renderIf } from '../Utils/utilFunctions'
import constant from '../Assests/constants/constant'
import { PropTypes } from 'prop-types';
import { observable } from 'mobx';
import { observer } from 'mobx-react/native';

@observer
export default class AlertPopup extends Component{
  // @observable InputText: String = ''
  @observable otp: String = ''
  @observable Alert_Visibility: Boolean = false
  @observable inValidOTPVisible: Boolean = false
  @observable matchedString: String = ''

  static propTypes = {
    otp:PropTypes.string,
    Alert_Visibility:PropTypes.boolean,
    okPress:PropTypes.func,
    cancelPress:PropTypes.func
  }

  static defaultProps = {
    otp:'',
    Alert_Visibility:false,
    okPress: () => {},
    cancelPress: () => {}
  }

  constructor(props){
    super(props)
  }

  componentWillReceiveProps(nextProps){
    this.otp = nextProps.otp
  }

  handleOTP = (text) => {
    this.matchedString = text
    let res = this.otp.substring(0, text.length)
    if (text === res){
      this.inValidOTPVisible = false
      return
    }
    this.inValidOTPVisible = true

  }

  render(){
    const { okPress, cancelPress, Alert_Visibility } = this.props
    return(
      <Modal visible={Alert_Visibility}
      transparent = {true}
      avoidKeyboard={true}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:'rgba(52, 52, 52, 0.8)' }}>
        <View style={styles.Alert_Main_View}>
          <Text style={styles.Alert_Title}>Konnect {this.otp}</Text>
          <Text style={styles.Alert_Message}>{constant.TEXTS.otpValid}</Text>
          <View style={{ width: '90%', height: 30, borderColor: 'rgba(128,128,128,.3)', borderWidth: 1, marginTop: 18 }}>
            <TextInput
              placeholder="Enter OTP"
              autoFocus={true}
              editable={!(this.otp === this.matchedString)}
              style={styles.Alert_TextInput}
              onChangeText={this.handleOTP} />
          </View>
          {
            // Pass any View or Component inside the curly bracket.
            // Here the ? Question Mark represent the ternary operator.
            this.inValidOTPVisible ? <Text style={styles.TextInvalidOTP}> Invalid OTP</Text> : null
          }
          <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(128,128,128,.3)', marginTop: 40 }} />
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={styles.buttonOKStyle}
              onPress={cancelPress}
              activeOpacity={0.7}
            >
              <Text style={styles.TextStyle}> Cancel </Text>
            </TouchableOpacity>

            <View style={{ width: 1, backgroundColor: 'rgba(128,128,128,.3)' }} />
            <TouchableOpacity
              style={styles.buttonOKStyle}
              onPress={okPress}
              disabled={!(this.otp === this.matchedString)}
              activeOpacity={0.7}
            >
              <Text style={((this.otp === this.matchedString)) ? styles.TextOKStyle : styles.TextOKStyleDisabled}> OK </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 80,
    flex: 1,
    alignItems: 'center'
  },
  logo: {
    height: 120,
    width: 120
  },
  textLabel: {
    marginVertical: 8,
    color: '#0369A2',
    fontSize: 17
  },
  textKonnect: {
    marginVertical: 33,
    color: '#0369A2',
    fontSize: 22
  },
  inputMainContainer: {
    height: 41,
    marginTop: 15,
    width: window.width - 50,
    marginHorizontal: 50,
    flexDirection: 'column'
  },
  textInputView: {
    height: 40,
    flexDirection: 'row',
  },
  textInputField: {
    width: window.width - 90,
    fontSize: 15,
    color: '#0369A2'
  },
  textInputIcon: {
    height: 22,
    width: 22,
    margin: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#0369A2',
    marginLeft: 10,
    marginRight: 10
  },
  buttonStyle: {
    backgroundColor: "#0369A2",
    marginTop: 30,
    width: window.width - 70,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: "white",
    fontSize: 17
  },
  textForgotPwd: {
    marginVertical: 15,
    color: '#0369A2',
    fontSize: 15
  },
  foreground: {
    backgroundColor: 'red',
  },
  background: {
    backgroundColor: '#000000',
    opacity: .4
  },
  MainContainer: {

    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //marginTop: (Platform.OS == 'ios') ? 20 : 0,
    //backgroundColor:'#00ffff'
  },

  Alert_Main_View: {

    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,.9)',
    width: '90%',
    borderWidth: 1,
    borderColor: '#E0E0E0',
    borderRadius: 14,

  },

  Alert_Title: {

    fontSize: 18,
    color: "black",
    textAlign: 'center',
    paddingTop: 20,
    fontWeight: 'bold'
  },
  Alert_TextInput: {
    padding: 4,
    fontSize: 17,
    color: "#0369A2",
    backgroundColor: 'white'
  },

  Alert_Message: {
    fontSize: 14,
    color: "black",
    textAlign: 'center',
    marginTop: 5,
    padding: 5
  },

  buttonOKStyle: {

    width: '50%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold'

  },

  TextStyle: {
    color: '#007AFF',
    textAlign: 'center',
    fontSize: 18,
  },
  TextOKStyle: {
    textAlign: 'center',
    fontSize: 18,
    color: '#007AFF',
  },
  TextOKStyleDisabled: {
    textAlign: 'center',
    fontSize: 18,
    color: 'grey',
  },
  TextInvalidOTP: {
    color: 'red',
    fontSize: 15,
    width: '90%'
  }


});