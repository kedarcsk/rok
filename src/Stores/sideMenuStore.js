import { observable, action } from "mobx";

class SideMenuStore {

  @observable sideBarItems = [];
  @observable sideMenu = {}
  
  @action setSideMenu(sideMenu: Object) {
    this.sideMenu = sideMenu
  }

  @action changeSideBarItems(data: Object) {
    if (data) {
      this.sideBarItems = data; 
    }
    this.sideMenu.setState({})
  }
}

export default new SideMenuStore();