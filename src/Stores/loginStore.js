import { fetchPost } from '../Utils/NetworkOps'
import constant from '../Assests/constants/constant'
import { observable, action } from 'mobx'
import {getToken, setInAsyncStorage, getFromaAsyncStorage, holidayDate} from '../Utils/utilFunctions'
class LoginStore {

  constructor(){
    this.isAlreadyLoggedIn()
  }

  @observable empid:String = ''
  @observable token:String = ''
  @observable empData:Object = {}

  @action async updateToken(token:String, isLogin?:Boolean ){
    this.token = getToken(token)
    await setInAsyncStorage('@token', JSON.stringify(this.token))
  }

  async isAlreadyLoggedIn(){
    let empData = await getFromaAsyncStorage('@empData')
    if(empData) {
      let token = await getFromaAsyncStorage('@token')
      this.empData = JSON.parse(empData)
      this.token = JSON.parse(token)
      return true
    }
    else return false
  }

  async updateStorage(responseData){
    await setInAsyncStorage('@empData',JSON.stringify(responseData.data));
    this.empData = responseData.data
    await this.updateToken(responseData.token)
  }

  async loginApi(empid:String, passwd:String){
    let params = {'empid':empid,'pwd' :passwd}
    let url = `${constant.BaseUrlStaging}${constant.URLs.login}`
    let response = await fetchPost(url, params)
    response && await this.updateToken(response.token, true)
    return response
  }

  async otpVaild(empid:String, passwd:String, otp:String ){
    let params = {'empid':empid,'pwd' :passwd , 'otp':otp, 'token':this.token}
    let url = `${constant.BaseUrlStaging}${constant.URLs.otpValidate}`
    let response = await fetchPost(url, params)
    response && await this.updateStorage(response)
    return response
  }

  callWebServiceToChangePassword = async(token, id, currentPwd, newPwd) => {
    let params = {
      token: token,
      empid: id,
      pwd:   currentPwd,
      npwd:  newPwd
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_CHANGEPASSWORD}`
    response = await fetchPost (url, params)
    return response;
  }

  async forgotPassword(empid){
    let params = {'corpid':empid}
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FORGOTPASSWORD}`
    let response = await fetchPost(url, params)
    console.log('>>>>'+response)
    return response
  }

  async fetchHolidays(year:String) {
    let params ={
      token: this.token,
      year: year 
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCH_HOLIDAYS}`
    let response = await fetchPost(url, params)
    if(response) {
      return response.data.map((shift)=>{
        return {
          title: shift.title,
          holidayList: shift.holidayList.map((holidayItem)=>{
            return {
              dateText: holidayDate(holidayItem.dt),
              day: holidayItem.day,
              holiday: holidayItem.holiday,
              holidayType: constant.HOLIDAY_TYPE.get(holidayItem.typeId).text,
              holidayColor: constant.HOLIDAY_TYPE.get(holidayItem.typeId).color
            }
          })
        }
      })
    }
  }

  async fetchPTO() {
    let params = {
      token: this.token
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_PTO}`
    let response = await fetchPost(url, params)
    if(response) {
      return response
    }
  }

}

export default new LoginStore()