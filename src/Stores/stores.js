import login from './loginStore'
import sideMenu from './sideMenuStore';
import time from './time'
import fac from '../Stores/findACollegueStore'
import expense from '../Stores/expenseStore'

export default{
  login,
  sideMenu,
  time,
  fac, 
  expense
}