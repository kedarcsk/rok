import { observable, action } from "mobx";
import constant from '../Assests/constants/constant'
import { fetchPost } from '../Utils/NetworkOps'

class FindAColleagueStore {
  async getUserList(searchedText:String, token:String){
    const params = {
      token: token,
      searchString: searchedText
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHLISTOFCOLLEAGUES}`
    let response = await fetchPost(url, params)
    return response
  }
  async getUserDetails(searchedText:String, token:String){
    const params = {
      token: token,
      searchString: searchedText
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_GETBASICSEARCHDETAIL}`
    let response = await fetchPost(url, params)
    return response
  }
  async getSearchResultForEmployee(id:String, token:String){
    const params = {
      token: token,
      empId: id
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHSEARCHDETAILSFOREMPLOYEE}`
    console.log(url)
    let response = await fetchPost(url, params)
    return response
  }
}

export default new FindAColleagueStore();