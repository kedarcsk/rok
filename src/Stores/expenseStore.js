import constant from '../Assests/constants/constant'
import { fetchPost, uploadForm } from '../Utils/NetworkOps'
import * as utils from '../Utils/utilFunctions'
import { observable, action } from 'mobx'

class ExpenseStore {
  @observable token:String = ''
  @observable reportId:String = ''
  @observable stDt:String = ''
  @observable endDt:String = ''
  @observable currId:String = ''
  @observable projId:String = ''
  @observable expenseData: Array = []

  async addExpenseReport(stDt:String, endDt:String, projId:String, title:String, curr:String, token:String){
    const params = {
      token: token,
      stDt:stDt,
      endDt:endDt,
      projId:projId,
      title:title,
      curr:curr
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_ADDEXPENSEREPORT}`
    let response = await fetchPost(url, params)
    return response
  }
  async getProjectList(stDt:String, endDt:String, token:String){
    const params = {
      token: token,
      stDt: stDt,
      endDt: endDt,
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHPROJECTLISTFOREXPENSE}`
    let response = await fetchPost(url, params)
    return response
  }
  async getCurrencyList(token:String){
    const params = {
      token: token
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHEXPENSECURRENCY}`
    let response = await fetchPost(url, params)
    return response
  }
  async getExpenseDetails(token:String, reportId:String){
    const params = {
      token: token,
      reportId:reportId
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHEXPENSEDETAILS}`
    let response = await fetchPost(url, params)
    return response
  }
  async getExpenseReportList(token:String){
    const params = {
      token: token
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHMYEXPENSEREPORTS}`
    let response = await fetchPost(url, params)
    return response
  }
  async copyExpenseReport(token:String, reportId:String){
    const params = {
      token: token,
      reportId: reportId
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_COPYREPORT}`
    let response = await fetchPost(url, params)
    return response
  }
  async deleteExpenseReport(token:String, reportId:String){
    const params = {
      token: token,
      reportId: reportId
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_DELETEREPORT}`
    let response = await fetchPost(url, params)
    return response
  }
  async editExpenseReport(token:String, reportId:String, reportTitle:String, startDate:String, endDate:String, projId:String){
    const params = {
      token: token,
      reportId: reportId,
      endDate: endDate,
      startDate: startDate,
      reportTitle: reportTitle,
      projId: projId
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_EDITEXPENSEREPORT}`
    let response = await fetchPost(url, params)
    return response
  }
  async getClaimedExpenseType(token:String){
    const params = {
      token: token
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHCLAIMEDEXPENSETYPE}`
    let response = await fetchPost(url, params)
    return response
  }
  async claimExpense(token:String, exDt:String, type:String, bill:String, summary:String, location:String, merchantNm:String, reimburse:String, billAmount:String){
    const params = {
      token: token,
      reportId: this.reportId,
      stDt: this.stDt,
      endDt: this.endDt,
      projId: this.projId,
      exDt: exDt,
      type: type,
      bill: bill,
      curr: this.currId,
      summary: summary,
      location: location,
      merchantNm: merchantNm,
      reimburse: reimburse,
      billAmount: billAmount
    };
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_CLAIMEXPENSE}`
    let response = await fetchPost(url, params)
    return response
  }
  uploadExpenseAttachment = async(file:Object, expenseId:String, reportId:String, token:String) => {
    let formData = new FormData()
    formData.append('token', token)
    formData.append('expenseId', expenseId)
    formData.append('reportId', reportId)
    formData.append('file', {
      name: file.fileName,
      uri: file.uri,
      type: file.type
    });
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_UPLOADEXPENSEATTACHMENT}`
    let response = await uploadForm(url, formData)
    return response
  }
}

export default new ExpenseStore();