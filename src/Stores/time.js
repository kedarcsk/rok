import { fetchPost, uploadForm } from '../Utils/NetworkOps'
import constant from '../Assests/constants/constant'
import { observable, action, computed } from 'mobx'
import moment from 'moment'
import * as utils from '../Utils/utilFunctions'
import login from './loginStore'
class Time {

  @observable token:String = ''
  @observable projectList=[]
  @observable projectMap:Map = new Map()
  @observable typeMap:Map = new Map()
  @observable weekData:Array = []
  @observable selectedDate:String = ''
  @observable weekCards:Array = []
  @observable isCardDeleted: Boolean = false
  @observable cardUpdate:Boolean = false
  @observable totalTime:Number = 0
  @observable timeStatus:String = '1'
  @observable attachementMap:Map = new Map()

  async getWeeklyTimeData() {
    //await 
  }

  initializeTime() {
    this.token = login.token
    console.log(this.token)
  }

  setSelectedDate(date:String) {
    this.selectedDate = date
  }

  uploadWeekAttachements = async(file:Object, projNm:String, timeType:String) => {
    let formData = new FormData()
    formData.append('token', this.token)
    formData.append('weekStDt', utils.formatAPI_DATE(this.selectedDate))
    formData.append('projId', this.projectMap.get(projNm))
    formData.append('projNm', projNm)
    formData.append('timeTypeId', this.typeMap.get(timeType))
    formData.append('file', {
      name: file.fileName,
      uri: file.uri,
      type: file.type
    });
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_UPLOAD_ATTACHEMENT}`
    let response = await uploadForm(url, formData)
    console.log(response)
    console.log(formData)
  }

  @action
  async getWeekAttachements() {
    let params={
      token: this.token,
      'startDt': utils.formatAPI_DATE(this.selectedDate)
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCH_ATTACHEMENTS}`
    let response = await fetchPost(url, params)
    if(response) {
      response.data.forEach((attachement)=>{
        let existing_attach = this.attachementMap.get(`${attachement.projId}${attachement.timeTypeId}`)
        if(existing_attach) {
          this.attachementMap.set(`${attachement.projId}${attachement.timeTypeId}`, existing_attach.concat(attachement) )
        }
        else {
          this.attachementMap.set(`${attachement.projId}${attachement.timeTypeId}`, [attachement])
        }
      })
    }
    console.log(response)
  }

  @action 
  async deleteAttachement(docId, index, key) {
    let params = {
      'token': this.token,
      'docId': docId
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_DELETE_ATTACHEMENT}`
    let response = await fetchPost(url, params)
    if(response) {
      let attach = this.attachementMap.get(key)
      attach.splice(index, 1)
      this.attachementMap.set(key, attach)
      return true
    }
  }

  @action 
  async deleteWeekAttachements(projId, timeTypeId) {
    let params = {
      'token': this.token,
      'weekStDt': utils.formatAPI_DATE(this.selectedDate),
      'projId': projId,
      'timeTypeId': timeTypeId
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_DELETE_WEEK_ATTACHEMENT}`
    let response = await fetchPost(url, params)
    if(response) {
      this.attachementMap.delete(`${projId}${timeTypeId}`)
      return true
    }
  }

  @action
  updateTotalTime() {
    this.totalTime = 0
    this.weekCards.forEach((week)=> {
      this.totalTime = this.totalTime + week.totalWeekTime
    })
    console.log(this.totalTime)
  }

  @action
  updateDeleteStatus(status:Boolean) {
    this.isCardDeleted = status
  }

  @action
  updateCardStatus(status:Boolean, index?:Number) {
    if ( index === (this.weekCards.length -1) ){
      this.cardUpdate = false
    }
    else {
      this.cardUpdate = status
    }
  }

  @action
  weekCardsData() {
    this.totalTime = 0
    this.isCardDeleted = false
    return this.weekCards.map((week)=> {
      this.totalTime = this.totalTime + week.totalWeekTime
      return week.getCurrentStateWeekData()
    })
  }

  @action 
  async deleteCard(index:Number) {
    this.totalTime = 0
    this.isCardDeleted = true
    let tempData = this.weekCards.filter((item, ind)=> ind !== index).map((week)=> {
      this.totalTime = this.totalTime + week.totalWeekTime
      return week.getCurrentStateWeekData()
    })
    this.weekCards.pop()
    if(tempData.length === 0) {
      await this.deleteWeekTime()
      tempData.push({isNewCard:true})
    }
    return tempData   
  }

  async deleteWeekTime() {
    let params = {
      'token': this.token,
      'weekStDt': utils.formatAPI_DATE(this.selectedDate)
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_DELETE_WEEK_TIME}`
    await fetchPost(url, params)
  }

  checkForHours() {
    if(this.totalTime < 40) {
      return false
    }
    return true
  }

  validateCards() {
    let result = true
    this.weekCards.forEach((week)=>{
      if(week.selectedProject === '') {
        result = false
        alert('select a project')
        return
      }
      else if(week.selectedType === '') {
        result = false
        alert('select a time type')
        return
      }
      else {
        result = week.validateEntries()
      }
    })
    return result
  }

  async hitAddApi(submitStatus:Boolean) {
    if(this.validateCards()) {
      let param = {
        token: this.token,
        weekStDt: utils.formatAPI_DATE(this.selectedDate),
        data: this.weekCards.map((week)=>{return { ...week.getWeekData(), status: submitStatus ? constant.TIME_STATUS.SUBMITTED : constant.TIME_STATUS.SAVE}})
      }
      let url = `${constant.BaseUrlStaging}${constant.URLs.API_SAVE_TIME}`
      let response = await fetchPost(url, param)
      if(response) {
        //alert('time added')
        this.weekCards.forEach(async(week)=>{
          await week.onFileSelection()
        })
        return true
      }   
    }
  }

  @action
  createWeekCard(weektimeCard) {
    try{
      this.weekCards.push(weektimeCard)
      console.log('jjjj')
    }
    catch(error) {
      console.log('***'+error)
    }
   
  }

  @action
  removeCard() {
    this.weekCards.pop()
    console.log("removed")
  }

  mapEntry(date:String, entryMap:Map) {
    let entry = entryMap.get(date)
    let entryObject = {
      day: utils.getAlphaDay(date),
      date: utils.formatWeekDate(date),
      dateString: utils.formatAPI_DATE(date)
    }
    if(entry) {
      let temptime = entry.hrs === '' ? 0 : parseInt(entry.hrs)
      this.totalTime = this.totalTime + temptime
      return{...entryObject, hrs:entry.hrs, descp:entry.descp}
    }
    else {
      return entryObject
    } 
  }

  createNewWeekCard() {
    return this.processTimeEntries([], this.selectedDate)
  }

  processTimeEntries(timeDetails:Array, date:String) {
    let entryMap = new Map()
    let tempdtString = date.split('-')
    timeDetails.forEach((timeEntry)=>{
      // alert(utils.correctDateFormat(timeEntry.dt))
      entryMap.set(utils.correctDateFormat(timeEntry.dt), timeEntry)
    })
    let day = 0
    let weekDateArray = []
    for(day ; day<7 ; day++) {
      let weekDate = utils.formatEntryDate(date, day)
      weekDateArray.push((this.mapEntry(weekDate, entryMap)))
    }
    return weekDateArray
  }

  async getMonthlyTimeData(startDate:String, endDate:String) {
    let params = {
      token: this.token,
      stDt: startDate,
      endDt: endDate
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.MONTHLY_TIME}`
    let response = await fetchPost(url, params)
    console.log(response)
    return response
  }

  async getWeeklyTimeData(date:String) {
    let params = {
      date:date,
      token:this.token
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.WEEKLY_TIME}`
    let response = await fetchPost(url, params)
    console.log(response)
    return response
  }

  @action
  async getProjectList(date:String) {
    let tempdtString = date.split('-')
    let params = {
      weekStdt: date,
      weekEnddt: `${parseInt(tempdtString[0])+7}-${tempdtString[1]}-${tempdtString[2]}`,
      token: this.token
    }
    //console.log(params)
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHPROJECTS}`
    let response = await fetchPost(url, params)
    //console.log(response)
    if(response) {
      this.projectList = response.data.reduce( (acc, obj) => { 
        this.projectMap.set(obj.projNm, obj.projId)
        return acc.concat(obj.projNm)
      } , [])
    }
    console.log(response)
  }

  async getTimeTypes(projNm:String) {
    let params = {
      projId: this.projectMap.get(projNm),
      token:this.token
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCHTIME_TYPES}`
    let response = await fetchPost(url, params)
    console.log(response)
    if(response) {
      return response.data.reduce( (acc, obj) => { 
        this.typeMap.set(obj.typeNm, obj.typeId)
        return acc.concat(obj.typeNm)
      } , []) 
    }
  }

  @action
  async getExistingTime(date:String) {
    this.attachementMap.clear()
    this.totalTime = 0
    this.isCardDeleted = false
    if(this.selectedDate !== date) {
      this.selectedDate = date
      await this.getProjectList(date)
    }
    let params = {
      startDt: date,
      token:this.token
    }
    let url = `${constant.BaseUrlStaging}${constant.URLs.API_FETCH_EXISTING_TIME}`
    let response = await fetchPost(url, params)
    console.log(response)
    if(response) {
      await this.getWeekAttachements()
      this.weekData = response.data
      return response.data.map((projectEntry)=>{
        return {...projectEntry, timeDetails: this.processTimeEntries(projectEntry.timeDetails, date)}
      })
    }
    //return response
  }
} 

export default new Time()